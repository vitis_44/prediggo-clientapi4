# Prediggo ClienApi4

Ce repo permet d'utiliser la bibliothèque ClientApi4 de prediggo avec Composer dans nos projets PHP.

Source : https://wiki4.prediggo.net/api/lib_php

## Pré-requis

- Avoir une clé SSH lié à son compte Gitlab.

## Installation

```json
/* composer.json */
"repositories": [
    {
        "type": "git",
        "url": "git@gitlab.com:vitis_44/prediggo-clientapi4.git"
    }
],
```

```console
composer install vitis44/prediggo-clientapi4
```