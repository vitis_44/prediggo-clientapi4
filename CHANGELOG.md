==== Release notes ====

=== 4.2.1 ===

  * Add ''pictogramId'' field to ''PictogramResult'' object.

=== 4.2.0 ===

  * Add ''url'' field to ''AdvancedParam'' object, ''ignoredSkus'' field to ''SearchStatistics'' object and ''nbSlotsX'' and ''nbSlotsY'' field to ''SearchStatistics && RecoStatistics'', add ''pictograms'' field to ''ItemInfo'' object.

=== 4.1.1 ===

  * Add selected in SortingOption.

=== 4.0.1 ===

  * Added new check for redirection to be null if categoryRedirection is true

=== 4.0.0 ===

  * Update supported PHP version to version 8.1
  * Modified the default behavior to exclusively include the API key within the headers. Additionally, introduced the ability to override this behavior and also include the API key in the URL as an alternative (not recommended).

=== 3.5.0 ===

  * Add support for SEARCH_AISLE block type

=== 3.4.0 ===

  * Add campaignName and campaignLabel to pageContent result's stats block
  * Add blocksContent endpoint

=== 3.3.2 ===

  * Updated to latest changes

=== 3.3.1 ===

  * Added the management of the Redirection URL in the autocomplete call.

=== 3.3.0 ===

  * Corrects updateUsers call

=== 3.2.1 ===

  * Adds blockFilters

=== 3.2.0 ===

  * Adds updateItems version 3.0

=== 3.1.3 ===

  * Adds new FrontResult for the search/reco block result.

=== 3.1.0 ===

  * Adds an optional ''$routingId'' argument in the constructor of PrediggoService to specify the routing header in case of distributed installations.

=== 3.0.0 ===

  * Introduces support region, stores and offers 
  * Deprecates zones and language codes

=== 1.8.0 ===

  * Adds itemsSet call

=== 1.7.0 ===

  * Adds SimplePageContent "mode" property that can be set to 'FACET_ONLY' to return only facets
  * Facet option "unit" is deprecated, as it was unused and always null
  * Adds "absoluteMin" and "absoluteMax" properties to FacetGroup to make price slider implementation easier

=== 1.6.0 ===

  * New calls ViewUser and DeleteUser are supported  (GDPR compliance)

=== 1.4.0 ===

  * New calls SimplePageContents and CachingStats are supported
  * PHP 5.4 compatibility is improved by testing existence of //json_last_error_msg()// method
  * Search : New properties searchId, ruleTemplate, query and updatedQuery are added to search Statistics
  * Search : SortingOrders constants are deprecated, customers should rely on dynamic sorting codes and labels returned by our system instead
  * Fixes a severe pageContent bug where not setting any advanced parameter would serialize into an empty json array "[]" rather than an empty object "{}" causing the server to return a HTTP 400