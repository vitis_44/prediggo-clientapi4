<?php


namespace Prediggo\ClientApi4\Types\PageContent;


class BannerComponent {

    private $pictureUrl;
    private $clickUrl;
    private $textToUser;

    /**
     * @return string
     */
    public function getPictureUrl() {
        return $this->pictureUrl;
    }

    /**
     * @param string $pictureUrl
     */
    public function setPictureUrl($pictureUrl) {
        $this->pictureUrl = $pictureUrl;
    }

    /**
     * @return string
     */
    public function getClickUrl() {
        return $this->clickUrl;
    }

    /**
     * @param string $clickUrl
     */
    public function setClickUrl($clickUrl) {
        $this->clickUrl = $clickUrl;
    }

    /**
     * @return string
     */
    public function getTextToUser() {
        return $this->textToUser;
    }

    /**
     * @param string $textToUser
     */
    public function setTextToUser($textToUser) {
        $this->textToUser = $textToUser;
    }

}