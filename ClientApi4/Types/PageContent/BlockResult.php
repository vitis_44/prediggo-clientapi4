<?php

namespace Prediggo\ClientApi4\Types\PageContent;

abstract class BlockResult {

    private $blockId;
    private $blockName;
    private $blockType;

    /**
     * @return mixed
     */
    public function getBlockId() {
        return $this->blockId;
    }

    /**
     * @param mixed $blockId
     */
    public function setBlockId($blockId) {
        $this->blockId = $blockId;
    }

    /**
     * @return mixed
     */
    public function getBlockName() {
        return $this->blockName;
    }

    /**
     * @param mixed $blockName
     */
    public function setBlockName($blockName) {
        $this->blockName = $blockName;
    }

    /**
     * @return mixed
     */
    public function getBlockType() {
        return $this->blockType;
    }

    /**
     * @param mixed $blockType
     */
    public function setBlockType($blockType) {
        $this->blockType = $blockType;
    }
}