<?php

namespace Prediggo\ClientApi4\Types\PageContent;

class SearchSettings {

    private $page = null;
    private $resultsPerPage = null;
    private $sortingCode = null;

    /**
     * An optional parameter used to give more or less importance to the semantic search.
     * The value should be between 0 and 1.
     * The closer the value is to 0, the less important the semantic search is.
     * The closer the value is to 1, the more important the semantic search is.
     * Not setting this value results in using the system default value.
     *
     * @var float
     */
    private $semanticSearchWeight = null;

    public function getPage() {
        return $this->page;
    }

    public function setPage($page) {
        $this->page = $page;
    }

    public function getResultsPerPage() {
        return $this->resultsPerPage;
    }

    public function setResultsPerPage($resultsPerPage) {
        $this->resultsPerPage = $resultsPerPage;
    }

    public function getSortingCode() {
        return $this->sortingCode;
    }

    public function setSortingCode($sortingCode) {
        $this->sortingCode = $sortingCode;
    }

    public function getSemanticSearchWeight(): ?float
    {
        return $this->semanticSearchWeight;
    }

    public function setSemanticSearchWeight(?float $semanticSearchWeight): void
    {
        $this->semanticSearchWeight = $semanticSearchWeight;
    }
}