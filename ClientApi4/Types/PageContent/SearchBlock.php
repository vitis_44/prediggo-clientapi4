<?php

namespace Prediggo\ClientApi4\Types\PageContent;

use Prediggo\ClientApi4\Types\Filter\Filterable;

class SearchBlock implements Filterable {

    private $blockId;
    private $query;
    private $refiningId;
    private $filters = array();

    private $settings;

    public function __construct() {
        $this->settings = new SearchSettings();
    }

    public function getFilters() {
        return $this->filters;
    }

    public function setFilters($filterSupplier) {
        $this->filters = $filterSupplier->filters();
    }

    /**
     * @return SearchSettings
     */
    public function getSettings() {
        return $this->settings;
    }

    /**
     * @return int
     */
    public function getBlockId() {
        return $this->blockId;
    }

    public function setBlockId($blockId) {
        $this->blockId = $blockId;
    }

    /**
     * @return string
     */
    public function getQuery() {
        return $this->query;
    }

    public function setQuery($query) {
        $this->query = $query;
    }

    /**
     * @return string
     */
    public function getRefiningId() {
        return $this->refiningId;
    }

    public function setRefiningId($refiningId) {
        $this->refiningId = $refiningId;
    }

}