<?php

namespace Prediggo\ClientApi4\Types\PageContent;

class SearchBlockResult extends BlockResult {

    private $blockFilters;
    private $didYouMean = array();
    private $stats;

    private $backtracks = array();
    private $facets = array();
    private $pages = array();
    private $sortings = array();
    private $slots = array();

    private $banner;
    private $atmosphere;
    private $redirection;

    private $front;

    public function __construct() {
        $this->stats = new SearchStatistics();
    }

    /**
     * Tells if this block has an available "did you mean" suggestion
     *
     * @return bool
     */
    public function hasDidYouMean() {
        return (count($this->didYouMean) > 0);
    }

    /**
     * Returns the current did you mean expression or an empty string if not available
     *
     * @return string
     */
    public function didYouMeanExpression() {

        if ($this->hasDidYouMean()) {
            return join(' ', $this->didYouMean);
        }

        return "";
    }

    /**
     * @return mixed
     */
    public function getBlockFilters()
    {
        return $this->blockFilters;
    }

    /**
     * @param mixed $blockFilters
     */
    public function setBlockFilters($blockFilters)
    {
        $this->blockFilters = $blockFilters;
    }

    /**
     * @return SearchStatistics
     */
    public function getStats() {
        return $this->stats;
    }

    /**
     * @param SearchStatistics $stats
     */
    public function setStats($stats) {
        $this->stats = $stats;
    }

    /**
     * @return FacetGroup[]
     */
    public function getFacets() {
        return $this->facets;
    }

    /**
     * @param FacetGroup[] $facets
     */
    public function setFacets($facets) {
        $this->facets = $facets;
    }

    /**
     * @return PageOption[]
     */
    public function getPages() {
        return $this->pages;
    }

    /**
     * @param PageOption[] $pages
     */
    public function setPages($pages) {
        $this->pages = $pages;
    }

    /**
     * @return SortingOption[]
     */
    public function getSortings() {
        return $this->sortings;
    }

    /**
     * @param SortingOption[] $sortings
     */
    public function setSortings($sortings) {
        $this->sortings = $sortings;
    }

    /**
     * @return SearchSlot[]
     */
    public function getSlots() {
        return $this->slots;
    }

    /**
     * @param SearchSlot[] $slots
     */
    public function setSlots($slots) {
        $this->slots = $slots;
    }

    /**
     * @return FacetGroup[]
     */
    public function getBacktracks() {
        return $this->backtracks;
    }

    /**
     * @param FacetGroup[] $backtracks
     */
    public function setBacktracks($backtracks) {
        $this->backtracks = $backtracks;
    }

    /**
     * @return string[]
     */
    public function getDidYouMean() {
        return $this->didYouMean;
    }

    /**
     * @param string[] $didYouMean
     */
    public function setDidYouMean($didYouMean) {
        $this->didYouMean = $didYouMean;
    }

    /**
     * @return BannerComponent
     */
    public function getBanner() {
        return $this->banner;
    }

    /**
     * @param BannerComponent $banner
     */
    public function setBanner($banner) {
        $this->banner = $banner;
    }

    /**
     * @return AtmosphereComponent
     */
    public function getAtmosphere() {
        return $this->atmosphere;
    }

    /**
     * @param AtmosphereComponent $atmosphere
     */
    public function setAtmosphere($atmosphere) {
        $this->atmosphere = $atmosphere;
    }

    /**
     * @return RedirectionComponent
     */
    public function getRedirection() {
        return $this->redirection;
    }

    /**
     * @param RedirectionComponent $redirection
     */
    public function setRedirection($redirection) {
        $this->redirection = $redirection;
    }

    /**
     * @return FrontResult
     */
    public function getFront()
    {
        return $this->front;
    }

    /**
     * @param FrontResult $front
     */
    public function setFront($front)
    {
        $this->front = $front;
    }
}