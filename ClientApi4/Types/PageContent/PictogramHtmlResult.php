<?php

namespace Prediggo\ClientApi4\Types\PageContent;

class PictogramHtmlResult extends PictogramResult
{
    /**
     * @var String
     */
    private $html;

    /**
     * @return String
     */
    public function getHtml()
    {
        return $this->html;
    }

    /**
     * @param String $html
     */
    public function setHtml($html)
    {
        $this->html = $html;
    }
}
