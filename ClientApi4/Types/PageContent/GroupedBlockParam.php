<?php

namespace Prediggo\ClientApi4\Types\PageContent;

use Prediggo\ClientApi4\Types\Filter\Filterable;
use Prediggo\ClientApi4\Types\Filter\FilterSupplier;

class GroupedBlockParam implements Filterable {

    private $query;
    private $refiningId;
    private $filters = array();
    private $skus = array();

    private $page = null;
    private $resultsPerPage = null;
    private $sortingCode = null;
    private $mode = 'ALL';

    /**
     * An optional parameter used to give more or less importance to the semantic search.
     * The value should be between 0 and 1.
     * The closer the value is to 0, the less important the semantic search is.
     * The closer the value is to 1, the more important the semantic search is.
     * Not setting this value results in using the system default value.
     *
     * @var float
     */
    private $semanticSearchWeight = null;

    public function getPage() {
        return $this->page;
    }

    public function setPage($page) {
        $this->page = $page;
    }

    public function getResultsPerPage() {
        return $this->resultsPerPage;
    }

    public function setResultsPerPage($resultsPerPage) {
        $this->resultsPerPage = $resultsPerPage;
    }

    public function getSortingCode() {
        return $this->sortingCode;
    }

    public function setSortingCode($sortingCode) {
        $this->sortingCode = $sortingCode;
    }

    public function getFilters() {
        return $this->filters;
    }

    /**
     * @param $filterSupplier FilterSupplier
     */
    public function setFilters($filterSupplier) {
        $this->filters = $filterSupplier->filters();
    }

    /**
     * @return array[string]
     */
    public function getSkus() {
        return $this->skus;
    }

    /**
     * @param array[string] $skus
     */
    public function setSkus($skus) {
        $this->skus = $skus;
    }


    /**
     * @return string
     */
    public function getQuery() {
        return $this->query;
    }

    public function setQuery($query) {
        $this->query = $query;
    }

    /**
     * @return string
     */
    public function getRefiningId() {
        return $this->refiningId;
    }

    public function setRefiningId($refiningId) {
        $this->refiningId = $refiningId;
    }

    /**
     * @return string
     */
    public function getMode() {
        return $this->mode;
    }

    /**
     * @param string $mode Can be one of 'ALL' or 'FACET_ONLY'
     */
    public function setMode($mode) {
        $this->mode = $mode;
    }

    public function getSemanticSearchWeight(): ?float
    {
        return $this->semanticSearchWeight;
    }

    public function setSemanticSearchWeight(?float $semanticSearchWeight): void
    {
        $this->semanticSearchWeight = $semanticSearchWeight;
    }
}