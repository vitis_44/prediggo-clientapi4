<?php

namespace Prediggo\ClientApi4\Types\PageContent;

use Prediggo\ClientApi4\Types\Filter\Filterable;

class AdBlockParam implements Filterable {

    private $blockId;
    private $filters = array();

    public function getBlockId() {
        return $this->blockId;
    }

    public function setBlockId($blockId) {
        $this->blockId = $blockId;
    }

    public function getFilters() {
        return $this->filters;
    }

    public function setFilters($filterSupplier) {
        $this->filters = $filterSupplier->filters();
    }
}