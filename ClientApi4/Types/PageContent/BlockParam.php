<?php

namespace Prediggo\ClientApi4\Types\PageContent;

use Prediggo\ClientApi4\Types\Filter\Filters;

class BlockParam {

    private $recommendations = array();
    private $searches = array();
    private $ads = array();

    public function addAdvertisement($blockId, $filterSupplier) {

        $block = new AdBlockParam();
        $block->setBlockId($blockId);

        if (isset($filterSupplier)) {
            $block->setFilters($filterSupplier);
        }

        $this->ads[] = $block;
        return $block;
    }

    public function addReco($blockId, $skus, $filterSupplier) {

        $block = new RecoBlockParam();
        $block->setBlockId($blockId);

        if ($skus != null) {

            if (is_array($skus)) {
                $block->setSkus($skus);
            } else {
                $block->setSkus(array($skus));
            }
        }

        if ($filterSupplier == null) {
            $block->setFilters(Filters::blank());
        } else {
            $block->setFilters($filterSupplier);
        }

        $this->recommendations[] = $block;
        return $block;
    }

    public function addNewSearch($blockId, $userQuery, $filterSupplier) {

        $block = new SearchBlock();
        $block->setBlockId($blockId);

        if (isset($userQuery)) {
            $block->setQuery($userQuery);
        }

        if (isset($filterSupplier)) {
            $block->setFilters($filterSupplier);
        }

        $this->searches[] = $block;

        return $block;
    }

    public function addRefinedSearch($blockId, $refiningId) {

        $block = new SearchBlock();
        $block->setBlockId($blockId);

        if ($refiningId instanceof Refinable) {
            $block->setRefiningId($refiningId->getRefiningId());
        } else {
            $block->setRefiningId($refiningId);
        }

        $block->setFilters(Filters::blank());
        $this->searches[] = $block;

        return $block;
    }

    /**
     * @return RecoBlockParam[]
     */
    public function getRecommendations() {
        return $this->recommendations;
    }

    /**
     * @return SearchBlock[]
     */
    public function getSearches() {
        return $this->searches;
    }

    /**
     * @return AdBlockParam[]
     */
    public function getAds() {
        return $this->ads;
    }

}