<?php

namespace Prediggo\ClientApi4\Types\PageContent;

class SortingOption implements Refinable {

    private $sortingCode;
    private $refiningId;
    private $noFollow;
    private $selected;
    private $label;

    /**
     * @return string
     */
    public function getSortingCode() {
        return $this->sortingCode;
    }

    /**
     * @param string
     */
    public function setSortingCode($sortingCode) {
        $this->sortingCode = $sortingCode;
    }

    /**
     * @return string
     */
    public function getRefiningId() {
        return $this->refiningId;
    }

    /**
     * @param string
     */
    public function setRefiningId($refiningId) {
        $this->refiningId = $refiningId;
    }

    /**
     * @return boolean
     */
    public function isNoFollow() {
        return $this->noFollow;
    }

    /**
     * @param boolean
     */
    public function setNoFollow($noFollow) {
        $this->noFollow = $noFollow;
    }

    /**
     * @return boolean
     */
    public function isSelected()
    {
        return $this->selected;
    }

    /**
     * @param boolean $selected
     */
    public function setSelected($selected): void
    {
        $this->selected = $selected;
    }

    /**
     * @return string
     */
    public function getLabel() {
        return $this->label;
    }

    /**
     * @param string
     */
    public function setLabel($label) {
        $this->label = $label;
    }

}
