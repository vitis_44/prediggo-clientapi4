<?php

namespace Prediggo\ClientApi4\Types\PageContent;

abstract class PictogramResult
{
    /**
     * @var int
     */
    private $pictogramId;
    /**
     * @var PictogramType
     */
    private $type;

    /**
     * @var String
     */
    private $position;

    /**
     * @var String
     */
    private $redirectionUrl;

    /**
     * @var String
     */
    private $altValue;

    public function getPictogramId()
    {
        return $this->pictogramId;
    }

    /**
     * @return int
     */
    public function setPictogramId($pictogramId)
    {
        $this->pictogramId = $pictogramId;
    }

    /**
     * @return PictogramType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param PictogramType $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return String
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param String $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return String
     */
    public function getRedirectionUrl()
    {
        return $this->redirectionUrl;
    }

    /**
     * @param String $redirectionUrl
     */
    public function setRedirectionUrl($redirectionUrl)
    {
        $this->redirectionUrl = $redirectionUrl;
    }

    /**
     * @return String
     */
    public function getAltValue()
    {
        return $this->altValue;
    }

    /**
     * @param String $altValue
     */
    public function setAltValue($altValue)
    {
        $this->altValue = $altValue;
    }
}
