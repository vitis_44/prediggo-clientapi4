<?php

namespace Prediggo\ClientApi4\Types\PageContent;

class AdBlockResult extends BlockResult {

    private $blockFilters;
    private $slots = array();
    private $stats;

    /**
     * @return mixed
     */
    public function getBlockFilters()
    {
        return $this->blockFilters;
    }

    /**
     * @param mixed $blockFilters
     */
    public function setBlockFilters($blockFilters)
    {
        $this->blockFilters = $blockFilters;
    }

    /**
     * @return AdSlot[]
     */
    public function getSlots() {
        return $this->slots;
    }

    /**
     * @param AdSlot[] $slots
     */
    public function setSlots($slots) {
        $this->slots = $slots;
    }

    /**
     * @return AdStatistics
     */
    public function getStats() {
        return $this->stats;
    }

    /**
     * @param AdStatistics $stats
     */
    public function setStats($stats) {
        $this->stats = $stats;
    }

}