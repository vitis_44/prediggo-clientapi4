<?php

namespace Prediggo\ClientApi4\Types\PageContent;

class BlockContainer {

    public $recommendations = array();
    public $searches = array();
    public $ads = array();

    /**
     * @return RecoBlockResult[]
     */
    public function getRecommendations() {
        return $this->recommendations;
    }

    /**
     * @param RecoBlockResult[] $recommendations
     */
    public function setRecommendations($recommendations) {
        $this->recommendations = $recommendations;
    }

    /**
     * @return SearchBlockResult[]
     */
    public function getSearches() {
        return $this->searches;
    }

    /**
     * @param SearchBlockResult[] $searches
     */
    public function setSearches($searches) {
        $this->searches = $searches;
    }

    /**
     * @return AdBlockResult[]
     */
    public function getAds() {
        return $this->ads;
    }

    /**
     * @param AdBlockResult[] $ads
     */
    public function setAds($ads) {
        $this->ads = $ads;
    }


    /**
     * @param int $blockId id of the block to search for
     * @return RecoBlockResult The block with specified ID, or null if not found
     */
    public function findRecommendationBlockById($blockId) {

        foreach ($this->recommendations as $block) {
            if ($blockId == $block->getBlockId()) {
                return $block;
            }
        }

        return null;
    }

    /**
     * @param int $blockId id of the block to search for
     * @return RecoBlockResult The block with specified ID, or null if not found
     */
    public function findAdBlockById($blockId) {

        foreach ($this->ads as $block) {
            if ($blockId == $block->getBlockId()) {
                return $block;
            }
        }

        return null;
    }


    /**
     * @param int $blockId id of the block to search for
     * @return SearchBlockResult The block with specified ID, or null if not found
     */
    public function findSearchBlockById($blockId) {

        foreach ($this->searches as $block) {
            if ($blockId == $block->getBlockId()) {
                return $block;
            }
        }

        return null;
    }
}