<?php

namespace Prediggo\ClientApi4\Types\PageContent;

class PictogramUrlResult extends PictogramResult
{
    /**
     * @var String
     */
    private $url;

    /**
     * @return String
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param String $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }
}
