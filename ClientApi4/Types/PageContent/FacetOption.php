<?php

namespace Prediggo\ClientApi4\Types\PageContent;

class FacetOption implements Refinable {

    private $value;
    private $label;
    private $unit;
    private $nbResults;
    private $debugText;
    private $selected;
    private $refiningId;
    private $min;
    private $max;
    private $noFollow;
    private $properties;

    public function getValue() {
        return $this->value;
    }

    public function setValue($value) {
        $this->value = $value;
    }

    public function getLabel() {
        return $this->label;
    }

    public function setLabel($label) {
        $this->label = $label;
    }

    /**
     * @deprecated DO NOT USE, this method always returns NULL
     * @return mixed
     */
    public function getUnit() {
        return $this->unit;
    }

    public function setUnit($unit) {
        $this->unit = $unit;
    }

    public function getNbResults() {
        return $this->nbResults;
    }

    public function setNbResults($nbResults) {
        $this->nbResults = $nbResults;
    }

    public function getDebugText() {
        return $this->debugText;
    }

    public function setDebugText($debugText) {
        $this->debugText = $debugText;
    }

    public function isSelected() {
        return $this->selected;
    }

    public function setSelected($selected) {
        $this->selected = $selected;
    }

    public function getRefiningId() {
        return $this->refiningId;
    }

    public function setRefiningId($refiningId) {
        $this->refiningId = $refiningId;
    }

    public function getMin() {
        return $this->min;
    }

    public function setMin($min) {
        $this->min = $min;
    }

    public function getMax() {
        return $this->max;
    }

    public function setMax($max) {
        $this->max = $max;
    }

    public function isNoFollow() {
        return $this->noFollow;
    }

    public function setNoFollow($noFollow) {
        $this->noFollow = $noFollow;
    }

    public function getProperties() {
        return $this->properties;
    }

    public function setProperties($properties) {
        $this->properties = $properties;
    }
}