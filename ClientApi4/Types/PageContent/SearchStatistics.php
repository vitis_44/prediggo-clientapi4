<?php

namespace Prediggo\ClientApi4\Types\PageContent;

class SearchStatistics {

    /**
     * @var float
     */
    private $computationInMs;

    /**
     * @var int
     */
    private $totalResults;

    /**
     * @var string
     */
    private $userLabel;

    /**
     * @var int
     */
    private $currentPage;

    /**
     * @var int
     */
    private $totalPages;

    /**
     * @var int
     */
    private $resultsInPage;

    /**
     * @var int
     */
    private $resultsPerPage;

    /**
     * @var string
     */
    private $galaxyId;

    /**
     * @var string
     */
    private $sortingCode;

    /**
     * @var float
     */
    private $searchId;

    /**
     * @var string
     */
    private $ruleTemplate;

    /**
     * @var string
     */
    private $query;

    /**
     * @var string
     */
    private $updatedQuery;

    /**
     * @var string
     */
    private $scenarioTemplate;

    /**
     * @var ItemTypesDistribution
     */
    private $itemTypesDistribution;

    /**
     * @var string
     */
    private $campaignName;

    /**
     * @var string
     */
    private $campaignLabel;

    /**
     * @var int
     */
    private $nbSlotsX;

    /**
     * @var int
     */
    private $nbSlotsY;

    /**
     * @var array
     */
    private $ignoredSkus;

    /**
     * @return float
     */
    public function getComputationInMs()
    {
        return $this->computationInMs;
    }

    /**
     * @param float $computationInMs
     */
    public function setComputationInMs($computationInMs)
    {
        $this->computationInMs = $computationInMs;
    }

    /**
     * @return int
     */
    public function getTotalResults()
    {
        return $this->totalResults;
    }

    /**
     * @param int $totalResults
     */
    public function setTotalResults($totalResults)
    {
        $this->totalResults = $totalResults;
    }

    /**
     * @return string
     */
    public function getUserLabel()
    {
        return $this->userLabel;
    }

    /**
     * @param string $userLabel
     */
    public function setUserLabel($userLabel)
    {
        $this->userLabel = $userLabel;
    }

    /**
     * @return int
     */
    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    /**
     * @param int $currentPage
     */
    public function setCurrentPage($currentPage)
    {
        $this->currentPage = $currentPage;
    }

    /**
     * @return int
     */
    public function getTotalPages()
    {
        return $this->totalPages;
    }

    /**
     * @param int $totalPages
     */
    public function setTotalPages($totalPages)
    {
        $this->totalPages = $totalPages;
    }

    /**
     * @return int
     */
    public function getResultsInPage()
    {
        return $this->resultsInPage;
    }

    /**
     * @param int $resultsInPage
     */
    public function setResultsInPage($resultsInPage)
    {
        $this->resultsInPage = $resultsInPage;
    }

    /**
     * @return int
     */
    public function getResultsPerPage()
    {
        return $this->resultsPerPage;
    }

    /**
     * @param int $resultsPerPage
     */
    public function setResultsPerPage($resultsPerPage)
    {
        $this->resultsPerPage = $resultsPerPage;
    }

    /**
     * @return string
     */
    public function getGalaxyId()
    {
        return $this->galaxyId;
    }

    /**
     * @param string $galaxyId
     */
    public function setGalaxyId($galaxyId)
    {
        $this->galaxyId = $galaxyId;
    }

    /**
     * @return string
     */
    public function getSortingCode()
    {
        return $this->sortingCode;
    }

    /**
     * @param string $sortingCode
     */
    public function setSortingCode($sortingCode)
    {
        $this->sortingCode = $sortingCode;
    }

    /**
     * @return float
     */
    public function getSearchId()
    {
        return $this->searchId;
    }

    /**
     * @param float $searchId
     */
    public function setSearchId($searchId)
    {
        $this->searchId = $searchId;
    }

    /**
     * @return string
     */
    public function getRuleTemplate()
    {
        return $this->ruleTemplate;
    }

    /**
     * @param string $ruleTemplate
     */
    public function setRuleTemplate($ruleTemplate)
    {
        $this->ruleTemplate = $ruleTemplate;
    }

    /**
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param string $query
     */
    public function setQuery($query)
    {
        $this->query = $query;
    }

    /**
     * @return string
     */
    public function getUpdatedQuery()
    {
        return $this->updatedQuery;
    }

    /**
     * @param string $updatedQuery
     */
    public function setUpdatedQuery($updatedQuery)
    {
        $this->updatedQuery = $updatedQuery;
    }

    /**
     * @return string
     */
    public function getScenarioTemplate()
    {
        return $this->scenarioTemplate;
    }

    /**
     * @param string $scenarioTemplate
     */
    public function setScenarioTemplate($scenarioTemplate)
    {
        $this->scenarioTemplate = $scenarioTemplate;
    }

    /**
     * @return ItemTypesDistribution
     */
    public function getItemTypesDistribution()
    {
        return $this->itemTypesDistribution;
    }

    /**
     * @param ItemTypesDistribution $itemTypesDistribution
     */
    public function setItemTypesDistribution($itemTypesDistribution)
    {
        $this->itemTypesDistribution = $itemTypesDistribution;
    }

    /**
     * @return string
     */
    public function getCampaignName()
    {
        return $this->campaignName;
    }

    /**
     * @param string $campaignName
     */
    public function setCampaignName($campaignName)
    {
        $this->campaignName = $campaignName;
    }

    /**
     * @return string
     */
    public function getCampaignLabel()
    {
        return $this->campaignLabel;
    }

    /**
     * @param string $campaignLabel
     */
    public function setCampaignLabel($campaignLabel)
    {
        $this->campaignLabel = $campaignLabel;
    }

    /**
     * @return int
     */
    public function getNbSlotsX()
    {
        return $this->nbSlotsX;
    }

    /**
     * @param int $nbSlotsX
     */
    public function setNbSlotsX($nbSlotsX)
    {
        $this->nbSlotsX = $nbSlotsX;
    }

    /**
     * @return int
     */
    public function getNbSlotsY()
    {
        return $this->nbSlotsY;
    }

    /**
     * @param int $nbSlotsY
     */
    public function setNbSlotsY($nbSlotsY)
    {
        $this->nbSlotsY = $nbSlotsY;
    }

    /**
     * @return array
     */
    public function getIgnoredSkus()
    {
        return $this->ignoredSkus;
    }

    /**
     * @param array $ignoredSkus
     */
    public function setIgnoredSkus($ignoredSkus)
    {
        $this->ignoredSkus = $ignoredSkus;
    }
}