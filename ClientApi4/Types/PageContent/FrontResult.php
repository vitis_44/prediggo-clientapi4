<?php

namespace Prediggo\ClientApi4\Types\PageContent;

class FrontResult {

    /**
     * @var string
     */
    private $label;

    /**
     * @var array
     */
    private $extraLabels = array();

    /**
     * @return string|null
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return array
     */
    public function getExtraLabels()
    {
        return $this->extraLabels;
    }

    /**
     * @param array $extraLabels
     */
    public function setExtraLabels($extraLabels)
    {
        $this->extraLabels = $extraLabels;
    }
}