<?php

namespace Prediggo\ClientApi4\Types\PageContent;

class RecoBlockResult extends BlockResult {

    private $blockFilters;
    private $slots = array();
    private $stats;
    private $front;

    /**
     * @return mixed
     */
    public function getBlockFilters()
    {
        return $this->blockFilters;
    }

    /**
     * @param mixed $blockFilters
     */
    public function setBlockFilters($blockFilters)
    {
        $this->blockFilters = $blockFilters;
    }

    /**
     * @return RecoSlot[]
     */
    public function getSlots() {
        return $this->slots;
    }

    /**
     * @param RecoSlot[] $slots
     */
    public function setSlots($slots) {
        $this->slots = $slots;
    }

    /**
     * @return RecoStatistics
     */
    public function getStats() {
        return $this->stats;
    }

    /**
     * @param RecoStatistics $stats
     */
    public function setStats($stats) {
        $this->stats = $stats;
    }

    /**
     * @return FrontResult
     */
    public function getFront()
    {
        return $this->front;
    }

    /**
     * @param FrontResult $front
     */
    public function setFront($front)
    {
        $this->front = $front;
    }
}