<?php

namespace Prediggo\ClientApi4\Types\PageContent;

/**
 * A list of possible sortings
 *
 * @deprecated Since 4.1, many custom sortings have been added so the list is considered incomplete.
 */
class SortingOrders {

    const  NONE = "NONE";
    const  RELEVANCE = 'RELEVANCE';
    const  BROWSING_PROFILE = 'BROWSING_PROFILE';
    const  DECREASING_POPULARITY = 'DECREASING_POPULARITY';
    const  DECREASING_PRICE = 'DECREASING_PRICE';
    const  INCREASING_PRICE = 'INCREASING_PRICE';
    const  POPULARITY_AND_RELEVANCE = 'POPULARITY_AND_RELEVANCE';
    const  SCORE_AND_RELEVANCE = 'SCORE_AND_RELEVANCE';
    const  DECREASING_ITEM_DATE = 'DECREASING_ITEM_DATE';
    const  INCREASING_ITEM_DATE = 'INCREASING_ITEM_DATE';
    const  DECREASING_ALPHABETICAL_SKU = 'DECREASING_ALPHABETICAL_SKU';
    const  INCREASING_ALPHABETICAL_SKU = 'INCREASING_ALPHABETICAL_SKU';
    const  DECREASING_SALES = 'DECREASING_SALES';
    const  DECREASING_RATINGS = 'DECREASING_RATINGS';
    const  DECREASING_SCORES = 'DECREASING_SCORES';
    const  DECREASING_NAMES = 'DECREASING_NAMES';
    const  INCREASING_NAMES = 'INCREASING_NAMES';

    //Values below are reserved codes that can be used in case of special customer request

    const  CUSTOM_1 = 'CUSTOM_1';
    const  CUSTOM_2 = 'CUSTOM_2';
    const  CUSTOM_3 = 'CUSTOM_3';
    const  CUSTOM_4 = 'CUSTOM_4';
    const  CUSTOM_5 = 'CUSTOM_5';
    const  CUSTOM_6 = 'CUSTOM_6';
    const  CUSTOM_7 = 'CUSTOM_7';
    const  CUSTOM_8 = 'CUSTOM_8';
    const  CUSTOM_9 = 'CUSTOM_9';
    const  CUSTOM_10 = 'CUSTOM_10';
}