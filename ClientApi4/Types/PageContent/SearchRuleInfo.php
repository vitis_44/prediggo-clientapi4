<?php

namespace Prediggo\ClientApi4\Types\PageContent;

class SearchRuleInfo {

    /**
     * @var string
     */
    private $ruleTemplateName;

    /**
     * @var string
     */
    private $ruleTemplateLabel;

    /**
     * @var string
     */
    private $ruleType;

    /**
     * @var mixed
     */
    private $startDate;

    /**
     * @var mixed
     */
    private $endDate;

    /**
     * @var int
     */
    private $initialPosition;

    /**
     * @var string
     */
    private $itemLabel;

    /**
     * @var string
     */
    private $userLabel;

    /**
     * @var string
     */
    private $stylesheetUrl;

    /**
     * @var float
     */
    private $computationTimeInMs;

    /**
     * @var FrontResult
     */
    private $front;

    /**
     * @return string
     */
    public function getRuleTemplateName()
    {
        return $this->ruleTemplateName;
    }

    /**
     * @param string $ruleTemplateName
     */
    public function setRuleTemplateName($ruleTemplateName)
    {
        $this->ruleTemplateName = $ruleTemplateName;
    }

    /**
     * @return string
     */
    public function getRuleTemplateLabel()
    {
        return $this->ruleTemplateLabel;
    }

    /**
     * @param string $ruleTemplateLabel
     */
    public function setRuleTemplateLabel($ruleTemplateLabel)
    {
        $this->ruleTemplateLabel = $ruleTemplateLabel;
    }

    /**
     * @return string
     */
    public function getRuleType()
    {
        return $this->ruleType;
    }

    /**
     * @param string $ruleType
     */
    public function setRuleType($ruleType)
    {
        $this->ruleType = $ruleType;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param mixed $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param mixed $endDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * @return int
     */
    public function getInitialPosition()
    {
        return $this->initialPosition;
    }

    /**
     * @param int $initialPosition
     */
    public function setInitialPosition($initialPosition)
    {
        $this->initialPosition = $initialPosition;
    }

    /**
     * @return string
     */
    public function getItemLabel()
    {
        return $this->itemLabel;
    }

    /**
     * @param string $itemLabel
     */
    public function setItemLabel($itemLabel)
    {
        $this->itemLabel = $itemLabel;
    }

    /**
     * @return string
     */
    public function getUserLabel()
    {
        return $this->userLabel;
    }

    /**
     * @param string $userLabel
     */
    public function setUserLabel($userLabel)
    {
        $this->userLabel = $userLabel;
    }

    /**
     * @return string
     */
    public function getStylesheetUrl()
    {
        return $this->stylesheetUrl;
    }

    /**
     * @param string $stylesheetUrl
     */
    public function setStylesheetUrl($stylesheetUrl)
    {
        $this->stylesheetUrl = $stylesheetUrl;
    }

    /**
     * @return float
     */
    public function getComputationTimeInMs()
    {
        return $this->computationTimeInMs;
    }

    /**
     * @param float $computationTimeInMs
     */
    public function setComputationTimeInMs($computationTimeInMs)
    {
        $this->computationTimeInMs = $computationTimeInMs;
    }

    /**
     * @return FrontResult
     */
    public function getFront()
    {
        return $this->front;
    }

    /**
     * @param FrontResult $front
     */
    public function setFront($front)
    {
        $this->front = $front;
    }
}