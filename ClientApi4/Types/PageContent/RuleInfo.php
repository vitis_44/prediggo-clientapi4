<?php

namespace Prediggo\ClientApi4\Types\PageContent;

class RuleInfo {

    /**
     * @var int
     */
    private $ruleId;

    /**
     * @var string
     */
    private $ruleName;

    /**
     * @var string
     */
    private $ruleType;

    /**
     * @var mixed
     */
    private $startDate;

    /**
     * @var mixed
     */
    private $endDate;

    /**
     * @var string
     */
    private $sourceItemId;

    /**
     * @var int
     */
    private $ruleTemplateId;

    /**
     * @var string
     */
    private $ruleTemplateName;

    /**
     * @var string
     */
    private $ruleTemplateLabel;

    /**
     * @var int
     */
    private $initialPosition;

    /**
     * @var string
     */
    private $itemLabel;

    /**
     * @var string
     */
    private $userLabel;

    /**
     * @var string
     */
    private $scenarioName;

    /**
     * @var string
     */
    private $stylesheetUrl;

    /**
     * @var float
     */
    private $computationTimeInMs;

    /**
     * @var FrontResult
     */
    private $front;

    /**
     * @return int
     */
    public function getRuleId()
    {
        return $this->ruleId;
    }

    /**
     * @param int $ruleId
     */
    public function setRuleId($ruleId)
    {
        $this->ruleId = $ruleId;
    }

    /**
     * @return string
     */
    public function getRuleName()
    {
        return $this->ruleName;
    }

    /**
     * @param string $ruleName
     */
    public function setRuleName($ruleName)
    {
        $this->ruleName = $ruleName;
    }

    /**
     * @return string
     */
    public function getRuleType()
    {
        return $this->ruleType;
    }

    /**
     * @param string $ruleType
     */
    public function setRuleType($ruleType)
    {
        $this->ruleType = $ruleType;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param mixed $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param mixed $endDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * @return string
     */
    public function getSourceItemId()
    {
        return $this->sourceItemId;
    }

    /**
     * @param string $sourceItemId
     */
    public function setSourceItemId($sourceItemId)
    {
        $this->sourceItemId = $sourceItemId;
    }

    /**
     * @return int
     */
    public function getRuleTemplateId()
    {
        return $this->ruleTemplateId;
    }

    /**
     * @param int $ruleTemplateId
     */
    public function setRuleTemplateId($ruleTemplateId)
    {
        $this->ruleTemplateId = $ruleTemplateId;
    }

    /**
     * @return string
     */
    public function getRuleTemplateName()
    {
        return $this->ruleTemplateName;
    }

    /**
     * @param string $ruleTemplateName
     */
    public function setRuleTemplateName($ruleTemplateName)
    {
        $this->ruleTemplateName = $ruleTemplateName;
    }

    /**
     * @return string
     */
    public function getRuleTemplateLabel()
    {
        return $this->ruleTemplateLabel;
    }

    /**
     * @param string $ruleTemplateLabel
     */
    public function setRuleTemplateLabel($ruleTemplateLabel)
    {
        $this->ruleTemplateLabel = $ruleTemplateLabel;
    }

    /**
     * @return int
     */
    public function getInitialPosition()
    {
        return $this->initialPosition;
    }

    /**
     * @param int $initialPosition
     */
    public function setInitialPosition($initialPosition)
    {
        $this->initialPosition = $initialPosition;
    }

    /**
     * @return string
     */
    public function getItemLabel()
    {
        return $this->itemLabel;
    }

    /**
     * @param string $itemLabel
     */
    public function setItemLabel($itemLabel)
    {
        $this->itemLabel = $itemLabel;
    }

    /**
     * @return string
     */
    public function getUserLabel()
    {
        return $this->userLabel;
    }

    /**
     * @param string $userLabel
     */
    public function setUserLabel($userLabel)
    {
        $this->userLabel = $userLabel;
    }

    /**
     * @return string
     */
    public function getScenarioName()
    {
        return $this->scenarioName;
    }

    /**
     * @param string $scenarioName
     */
    public function setScenarioName($scenarioName)
    {
        $this->scenarioName = $scenarioName;
    }

    /**
     * @return string
     */
    public function getStylesheetUrl()
    {
        return $this->stylesheetUrl;
    }

    /**
     * @param string $stylesheetUrl
     */
    public function setStylesheetUrl($stylesheetUrl)
    {
        $this->stylesheetUrl = $stylesheetUrl;
    }

    /**
     * @return float
     */
    public function getComputationTimeInMs()
    {
        return $this->computationTimeInMs;
    }

    /**
     * @param float $computationTimeInMs
     */
    public function setComputationTimeInMs($computationTimeInMs)
    {
        $this->computationTimeInMs = $computationTimeInMs;
    }

    /**
     * @return FrontResult
     */
    public function getFront()
    {
        return $this->front;
    }

    /**
     * @param FrontResult $front
     */
    public function setFront($front)
    {
        $this->front = $front;
    }
}