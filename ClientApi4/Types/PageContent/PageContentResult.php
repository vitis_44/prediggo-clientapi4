<?php

namespace Prediggo\ClientApi4\Types\PageContent;


use Prediggo\ClientApi4\Types\Item\ItemInfo;

/**
 * Result object for PageContent queries
 */
class PageContentResult {

    private $sessionId;

    /**
     * @var string region
     */
    private $region;

    /**
     * @var string
     * @deprecated Languages and zones will be be removed.
     */
    private $languageCode;

    /**
     * @var string
     * @deprecated Languages and zones will be be removed.
     */
    private $zone;

    private $pageId;
    private $pageName;
    private $timeInMs;
    private $serverNodeId;
    private $cachingStatsKey;
    private $blocks;

    /**
     * @var AdvancedResult
     */
    private $advanced;

    /**
     * @var ItemInfo[]
     */
    private $items = array();

    public function __construct() {
        $this->blocks = new BlockContainer();
        $this->advanced = new AdvancedResult();
    }

    /**
     * @return string
     */
    public function getSessionId() {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     */
    public function setSessionId($sessionId) {
        $this->sessionId = $sessionId;
    }

    /**
     * @return string
     * @deprecated Languages and zones will be be removed.
     */
    public function getLanguageCode() {
        return $this->languageCode;
    }

    /**
     * @param string $languageCode
     * @deprecated Languages and zones will be be removed.
     */
    public function setLanguageCode($languageCode) {
        $this->languageCode = $languageCode;
    }


    /**
     * @return string
     * @deprecated Languages and zones will be be removed.
     */
    public function getZone() {
        return $this->zone;
    }

    /**
     * @param string $zone
     * @deprecated Languages and zones will be be removed.
     */
    public function setZone($zone) {
        $this->zone = $zone;
    }


    /**
     * @return string
     */
    public function getRegion() {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion($region) {
        $this->region = $region;
    }

    /**
     * @return int
     */
    public function getPageId() {
        return $this->pageId;
    }

    /**
     * @param int $pageId
     */
    public function setPageId($pageId) {
        $this->pageId = $pageId;
    }

    /**
     * @return string
     */
    public function getPageName() {
        return $this->pageName;
    }

    /**
     * @param string $pageName
     */
    public function setPageName($pageName) {
        $this->pageName = $pageName;
    }

    /**
     * @return float
     */
    public function getTimeInMs() {
        return $this->timeInMs;
    }

    /**
     * @param float $timeInMs
     */
    public function setTimeInMs($timeInMs) {
        $this->timeInMs = $timeInMs;
    }

    /**
     * @return BlockContainer
     */
    public function getBlocks() {
        return $this->blocks;
    }

    /**
     * @param BlockContainer $blocks
     */
    public function setBlocks($blocks) {
        $this->blocks = $blocks;
    }

    /**
     * @return string
     */
    public function getServerNodeId() {
        return $this->serverNodeId;
    }

    /**
     * @param string $serverNodeId
     */
    public function setServerNodeId($serverNodeId) {
        $this->serverNodeId = $serverNodeId;
    }

    /**
     * @return string
     */
    public function getCachingStatsKey() {
        return $this->cachingStatsKey;
    }

    /**
     * @param string $cachingStatsKey
     */
    public function setCachingStatsKey($cachingStatsKey) {
        $this->cachingStatsKey = $cachingStatsKey;
    }

    /**
     * @return ItemInfo[]
     */
    public function getItems() {
        return $this->items;
    }

    /**
     * @param ItemInfo[] $items
     */
    public function setItems($items) {
        $this->items = $items;
    }

    /**
     * @param string $sku
     * @param ItemInfo $itemInfo
     */
    public function addItem($sku, $itemInfo) {
        $this->items[$sku] = $itemInfo;
    }

    /**
     * @return AdvancedResult
     */
    public function getAdvanced() {
        return $this->advanced;
    }

    /**
     * @param AdvancedResult $advanced
     */
    public function setAdvanced($advanced) {
        $this->advanced = $advanced;
    }
}
