<?php

namespace Prediggo\ClientApi4\Types\PageContent;

class PictogramType
{
    public const URL = 'URL';
    public const HTML = 'HTML';
}