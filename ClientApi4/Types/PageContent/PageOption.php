<?php

namespace Prediggo\ClientApi4\Types\PageContent;

class PageOption implements Refinable {

    private $pageNumber;
    private $label;
    private $refiningId;
    private $noFollow;

    public function getPageNumber() {
        return $this->pageNumber;
    }

    public function setPageNumber($pageNumber) {
        $this->pageNumber = $pageNumber;
    }

    public function getLabel() {
        return $this->label;
    }

    public function setLabel($label) {
        $this->label = $label;
    }

    public function getRefiningId() {
        return $this->refiningId;
    }

    public function setRefiningId($refiningId) {
        $this->refiningId = $refiningId;
    }

    public function isNoFollow() {
        return $this->noFollow;
    }

    public function setNoFollow($noFollow) {
        $this->noFollow = $noFollow;
    }

}