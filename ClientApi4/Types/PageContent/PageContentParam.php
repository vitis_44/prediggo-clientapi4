<?php

namespace Prediggo\ClientApi4\Types\PageContent;

use Prediggo\ClientApi4\ModuleInfo;
use Prediggo\ClientApi4\Types\ApiVersioned;
use Prediggo\ClientApi4\Types\ContentQueryParameter;

/**
 * Parameter object for PageContent queries
 */
class PageContentParam implements ApiVersioned, ContentQueryParameter {

    private $sessionId = "";

    /**
     * @var string
     * @deprecated Languages and zones will be be removed.
     */
    private $languageCode = "";

    private $pageId = 0;
    private $blocks;

    private $advanced;

    /**
     * @var string
     */
    private $moduleVersion = ModuleInfo::MODULE_VERSION;

    /**
     * @var string region
     */
    private $region = "";

    public function __construct() {
        $this->blocks = new BlockParam();
        $this->advanced = new AdvancedParam();
    }

    public function getSessionId() {
        return $this->sessionId;
    }

    public function setSessionId($sessionId) {
        $this->sessionId = $sessionId;
    }

    /**
     * @return string
     * @deprecated Languages and zones will be be removed.
     */
    public function getLanguageCode() {
        return $this->languageCode;
    }

    /**
     * @deprecated Languages and zones will be be removed.
     * @param string $languageCode
     */
    public function setLanguageCode($languageCode) {
        $this->languageCode = $languageCode;
    }

    /**
     * @return string
     */
    public function getRegion() {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion($region) {
        $this->region = $region;
    }

    public function getPageId() {
        return $this->pageId;
    }

    public function setPageId($pageId) {
        $this->pageId = $pageId;
    }

    public function getBlocks() {
        return $this->blocks;
    }

    /**
     * @return AdvancedParam
     */
    public function getAdvanced() {
        return $this->advanced;
    }

    /**
     * @return string
     * @deprecated Languages and zones will be be removed.
     */
    function getZone() {
        return $this->getAdvanced()->getZone();
    }

    /**
     * @param string $zone
     * @deprecated Languages and zones will be be removed.
     */
    function setZone($zone) {
        $this->getAdvanced()->setZone($zone);
    }

    /**
     * @return string the version of the module
     */
    function getModuleVersion() {
        return $this->moduleVersion;
    }
}


