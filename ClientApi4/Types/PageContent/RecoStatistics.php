<?php

namespace Prediggo\ClientApi4\Types\PageContent;

class RecoStatistics {

    /**
     * @var int
     */
    private $totalResults;

    /**
     * @var float
     */
    private $computationInMs;

    /**
     * @var string
     */
    private $userLabel;

    /**
     * @var string
     */
    private $scenarioName;

    /**
     * @var string
     */
    private $scenarioTemplate;

    /**
     * @var ItemTypesDistribution
     */
    private $itemTypesDistribution;

    /**
     * @var string
     */
    private $campaignName;

    /**
     * @var string
     */
    private $campaignLabel;

    /**
     * @var int
     */
    private $nbSlotsX;

    /**
     * @var int
     */
    private $nbSlotsY;

    /**
     * @return int
     */
    public function getTotalResults()
    {
        return $this->totalResults;
    }

    /**
     * @param int $totalResults
     */
    public function setTotalResults($totalResults)
    {
        $this->totalResults = $totalResults;
    }

    /**
     * @return float
     */
    public function getComputationInMs()
    {
        return $this->computationInMs;
    }

    /**
     * @param float $computationInMs
     */
    public function setComputationInMs($computationInMs)
    {
        $this->computationInMs = $computationInMs;
    }

    /**
     * @return string
     */
    public function getUserLabel()
    {
        return $this->userLabel;
    }

    /**
     * @param string $userLabel
     */
    public function setUserLabel($userLabel)
    {
        $this->userLabel = $userLabel;
    }

    /**
     * @return string
     */
    public function getScenarioName()
    {
        return $this->scenarioName;
    }

    /**
     * @param string $scenarioName
     */
    public function setScenarioName($scenarioName)
    {
        $this->scenarioName = $scenarioName;
    }

    /**
     * @return string
     */
    public function getScenarioTemplate()
    {
        return $this->scenarioTemplate;
    }

    /**
     * @param string $scenarioTemplate
     */
    public function setScenarioTemplate($scenarioTemplate)
    {
        $this->scenarioTemplate = $scenarioTemplate;
    }

    /**
     * @return ItemTypesDistribution
     */
    public function getItemTypesDistribution()
    {
        return $this->itemTypesDistribution;
    }

    /**
     * @param ItemTypesDistribution $itemTypesDistribution
     */
    public function setItemTypesDistribution($itemTypesDistribution)
    {
        $this->itemTypesDistribution = $itemTypesDistribution;
    }

    /**
     * @return string
     */
    public function getCampaignName()
    {
        return $this->campaignName;
    }

    /**
     * @param string $campaignName
     */
    public function setCampaignName($campaignName)
    {
        $this->campaignName = $campaignName;
    }

    /**
     * @return string
     */
    public function getCampaignLabel()
    {
        return $this->campaignLabel;
    }

    /**
     * @param string $campaignLabel
     */
    public function setCampaignLabel($campaignLabel)
    {
        $this->campaignLabel = $campaignLabel;
    }

    /**
     * @return int
     */
    public function getNbSlotsX()
    {
        return $this->nbSlotsX;
    }

    /**
     * @param int $nbSlotsX
     */
    public function setNbSlotsX($nbSlotsX)
    {
        $this->nbSlotsX = $nbSlotsX;
    }

    /**
     * @return int
     */
    public function getNbSlotsY()
    {
        return $this->nbSlotsY;
    }

    /**
     * @param int $nbSlotsY
     */
    public function setNbSlotsY($nbSlotsY)
    {
        $this->nbSlotsY = $nbSlotsY;
    }
}