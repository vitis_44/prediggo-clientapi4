<?php

namespace Prediggo\ClientApi4\Types\PageContent;

use Prediggo\ClientApi4\Types\Item\ItemInfo;

class RecoSlot {

    private $position;
    private $item;
    private $rule;

    /**
     * @return int
     */
    public function getPosition() {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition($position) {
        $this->position = $position;
    }

    /**
     * @return ItemInfo
     */
    public function getItem() {
        return $this->item;
    }

    /**
     * @param ItemInfo $item
     */
    public function setItem($item) {
        $this->item = $item;
    }

    /**
     * @return RuleInfo
     */
    public function getRule() {
        return $this->rule;
    }

    /**
     * @param RuleInfo $rule
     */
    public function setRule($rule) {
        $this->rule = $rule;
    }


}