<?php

namespace Prediggo\ClientApi4\Types\PageContent;


class AdStatistics {

    /**
     * @var int
     */
    private $totalResults;

    /**
     * @var float
     */
    private $computationInMs;

    /**
     * @var string
     */
    private $userLabel;

    /**
     * @var ItemTypesDistribution
     */
    private $itemTypesDistribution;

    /**
     * @return int
     */
    public function getTotalResults()
    {
        return $this->totalResults;
    }

    /**
     * @param int $totalResults
     */
    public function setTotalResults($totalResults)
    {
        $this->totalResults = $totalResults;
    }

    /**
     * @return float
     */
    public function getComputationInMs()
    {
        return $this->computationInMs;
    }

    /**
     * @param float $computationInMs
     */
    public function setComputationInMs($computationInMs)
    {
        $this->computationInMs = $computationInMs;
    }

    /**
     * @return string
     */
    public function getUserLabel()
    {
        return $this->userLabel;
    }

    /**
     * @param string $userLabel
     */
    public function setUserLabel($userLabel)
    {
        $this->userLabel = $userLabel;
    }

    /**
     * @return ItemTypesDistribution
     */
    public function getItemTypesDistribution()
    {
        return $this->itemTypesDistribution;
    }

    /**
     * @param ItemTypesDistribution $itemTypesDistribution
     */
    public function setItemTypesDistribution($itemTypesDistribution)
    {
        $this->itemTypesDistribution = $itemTypesDistribution;
    }
}