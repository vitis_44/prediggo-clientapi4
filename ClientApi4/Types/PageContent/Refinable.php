<?php

namespace Prediggo\ClientApi4\Types\PageContent;


/**
 * Interface for result objects that can be used for refining queries
 */
interface Refinable {

    function getRefiningId();
}
