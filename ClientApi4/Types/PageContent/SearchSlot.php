<?php

namespace Prediggo\ClientApi4\Types\PageContent;

use Prediggo\ClientApi4\Types\Item\ItemInfo;

class SearchSlot {

    private $position;
    private $item;
    private $rule;

    public function getPosition() {
        return $this->position;
    }

    public function setPosition($position) {
        $this->position = $position;
    }

    /**
     * @return ItemInfo
     */
    public function getItem() {
        return $this->item;
    }

    /**
     * @param ItemInfo $item
     */
    public function setItem($item) {
        $this->item = $item;
    }

    /**
     * @return SearchRuleInfo
     */
    public function getRule() {
        return $this->rule;
    }

    /**
     * @param SearchRuleInfo $rule
     */
    public function setRule($rule) {
        $this->rule = $rule;
    }


}