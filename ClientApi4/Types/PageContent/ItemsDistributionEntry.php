<?php

namespace Prediggo\ClientApi4\Types\PageContent;

class ItemsDistributionEntry {

    /**
     * @var double
     */
    private $min;

    /**
     * @var double
     */
    private $max;

    /**
     * @var int
     */
    private $count;

    /**
     * @return float
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * @param float $min
     */
    public function setMin($min)
    {
        $this->min = $min;
    }

    /**
     * @return float
     */
    public function getMax()
    {
        return $this->max;
    }

    /**
     * @param float $max
     */
    public function setMax($max)
    {
        $this->max = $max;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }
}