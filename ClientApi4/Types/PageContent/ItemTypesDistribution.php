<?php

namespace Prediggo\ClientApi4\Types\PageContent;

/**
 * Object for item types distribution result
 */
class ItemTypesDistribution {

    /**
     * @var array
     */
    private $totalResults;

    /**
     * @var array
     */
    private $resultsInPage;

    /**
     * @return array
     */
    public function getTotalResults()
    {
        return $this->totalResults;
    }

    /**
     * @param array $totalResults
     */
    public function setTotalResults($totalResults)
    {
        $this->totalResults = $totalResults;
    }

    /**
     * @return array
     */
    public function getResultsInPage()
    {
        return $this->resultsInPage;
    }

    /**
     * @param array $resultsInPage
     */
    public function setResultsInPage($resultsInPage)
    {
        $this->resultsInPage = $resultsInPage;
    }
}