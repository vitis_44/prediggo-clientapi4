<?php

namespace Prediggo\ClientApi4\Types\PageContent;

class FacetGroup {

    /**
     * @var string
     */
    private $attributeName;

    /**
     * @var string
     */
    private $attributeLabel;

    /**
     * @var string
     */
    private $attributeType;

    /**
     * @var string
     */
    private $attributeUnit;

    /**
     * @var bool
     */
    private $multiSelect;

    /**
     * @var int
     */
    private $position;

    /**
     * @var string|null
     */
    private $widgetType;

    /**
     * @var string|null
     */
    private $widgetPosition;

    /**
     * @var string|null
     */
    private $widgetVisibility;

    /**
     * @var string
     */
    private $robotMetadata;

    /**
     * @var float|null
     */
    private $absoluteMin;

    /**
     * @var float|null
     */
    private $absoluteMax;

    /**
     * @var ItemsDistributionEntry[]
     */
    private $itemsDistribution;

    /**
     * @var FacetOption[]
     */
    private $options = array();

    /**
     * @return string
     */
    public function getAttributeName()
    {
        return $this->attributeName;
    }

    /**
     * @param string $attributeName
     */
    public function setAttributeName($attributeName)
    {
        $this->attributeName = $attributeName;
    }

    /**
     * @return string
     */
    public function getAttributeLabel()
    {
        return $this->attributeLabel;
    }

    /**
     * @param string $attributeLabel
     */
    public function setAttributeLabel($attributeLabel)
    {
        $this->attributeLabel = $attributeLabel;
    }

    /**
     * @return string
     */
    public function getAttributeType()
    {
        return $this->attributeType;
    }

    /**
     * @param string $attributeType
     */
    public function setAttributeType($attributeType)
    {
        $this->attributeType = $attributeType;
    }

    /**
     * @return string
     */
    public function getAttributeUnit()
    {
        return $this->attributeUnit;
    }

    /**
     * @param string $attributeUnit
     */
    public function setAttributeUnit($attributeUnit)
    {
        $this->attributeUnit = $attributeUnit;
    }

    /**
     * @return bool
     */
    public function isMultiSelect()
    {
        return $this->multiSelect;
    }

    /**
     * @param bool $multiSelect
     */
    public function setMultiSelect($multiSelect)
    {
        $this->multiSelect = $multiSelect;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return string|null
     */
    public function getWidgetType()
    {
        return $this->widgetType;
    }

    /**
     * @param string|null $widgetType
     */
    public function setWidgetType($widgetType)
    {
        $this->widgetType = $widgetType;
    }

    /**
     * @return string|null
     */
    public function getWidgetPosition()
    {
        return $this->widgetPosition;
    }

    /**
     * @param string|null $widgetPosition
     */
    public function setWidgetPosition($widgetPosition)
    {
        $this->widgetPosition = $widgetPosition;
    }

    /**
     * @return string|null
     */
    public function getWidgetVisibility()
    {
        return $this->widgetVisibility;
    }

    /**
     * @param string|null $widgetVisibility
     */
    public function setWidgetVisibility($widgetVisibility)
    {
        $this->widgetVisibility = $widgetVisibility;
    }

    /**
     * @return string
     */
    public function getRobotMetadata()
    {
        return $this->robotMetadata;
    }

    /**
     * @param string $robotMetadata
     */
    public function setRobotMetadata($robotMetadata)
    {
        $this->robotMetadata = $robotMetadata;
    }

    /**
     * @return float|null
     */
    public function getAbsoluteMin()
    {
        return $this->absoluteMin;
    }

    /**
     * @param float|null $absoluteMin
     */
    public function setAbsoluteMin($absoluteMin)
    {
        $this->absoluteMin = $absoluteMin;
    }

    /**
     * @return float|null
     */
    public function getAbsoluteMax()
    {
        return $this->absoluteMax;
    }

    /**
     * @param float|null $absoluteMax
     */
    public function setAbsoluteMax($absoluteMax)
    {
        $this->absoluteMax = $absoluteMax;
    }

    /**
     * @return ItemsDistributionEntry[]
     */
    public function getItemsDistribution()
    {
        return $this->itemsDistribution;
    }

    /**
     * @param ItemsDistributionEntry[] $itemsDistribution
     */
    public function setItemsDistribution($itemsDistribution)
    {
        $this->itemsDistribution = $itemsDistribution;
    }

    /**
     * @return FacetOption[]
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param FacetOption[] $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }
}