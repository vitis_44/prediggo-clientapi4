<?php

namespace Prediggo\ClientApi4\Types\PageContent;

class AtmosphereComponent {

    private $cssTemplate;
    private $blockTemplate;
    private $optionalText;

    /**
     * @return string
     */
    public function getCssTemplate() {
        return $this->cssTemplate;
    }

    /**
     * @param string $cssTemplate
     */
    public function setCssTemplate($cssTemplate) {
        $this->cssTemplate = $cssTemplate;
    }

    /**
     * @return string
     */
    public function getBlockTemplate() {
        return $this->blockTemplate;
    }

    /**
     * @param string $blockTemplate
     */
    public function setBlockTemplate($blockTemplate) {
        $this->blockTemplate = $blockTemplate;
    }

    /**
     * @return string
     */
    public function getOptionalText() {
        return $this->optionalText;
    }

    /**
     * @param string $optionalText
     */
    public function setOptionalText($optionalText) {
        $this->optionalText = $optionalText;
    }

}