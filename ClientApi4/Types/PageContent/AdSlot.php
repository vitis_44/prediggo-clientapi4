<?php

namespace Prediggo\ClientApi4\Types\PageContent;

use Prediggo\ClientApi4\Types\Ad\AdInfo;

class AdSlot {

    private $position;
    private $ad;
    private $debugInfo;

    /**
     * @return int
     */
    public function getPosition() {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition($position) {
        $this->position = $position;
    }

    /**
     * @return AdInfo
     */
    public function getAd() {
        return $this->ad;
    }

    /**
     * @param AdInfo $ad
     */
    public function setBanner($ad) {
        $this->ad = $ad;
    }

    /**
     * @return array
     */
    public function getDebugInfo() {
        return $this->debugInfo;
    }

    /**
     * @param array $debugInfo
     */
    public function setDebugInfo($debugInfo) {
        $this->debugInfo = $debugInfo;
    }


}