<?php

namespace Prediggo\ClientApi4\Types\SubCategoryCount;

use Prediggo\ClientApi4\Types\Debug\DebugResult;
use Prediggo\ClientApi4\Types\PageContent\AdvancedResult;

/**
 * SubCategoryCount result class
 */
class SubCategoryCountResult {

    /**
     * @var string
     */
    private $status;

    /**
     * @var float
     */
    private $timeInMs;

    /**
     * @var DebugResult
     */
    private $debug;

    /**
     * @var string
     */
    private $sessionId;

    /**
     * @var string
     */
    private $region = "";

    /**
     * @var AdvancedResult
     */
    private $advanced;

    /**
     * @var string
     */
    private $attributeName;

    /**
     * @var string
     */
    private $attributeValue;

    /**
     * @var CategoryNode
     */
    private $node;

    /**
     * @var string
     */
    private $includeParents;

    /**
     * @var SubCategoryCountSubNode[]
     */
    private $subNodes = array();

    /**
     * @var SubCategoryCountSubNode[]
     */
    private $parentNodes = array();

    public function __construct()
    {
        $this->advanced = new AdvancedResult();
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return float
     */
    public function getTimeInMs()
    {
        return $this->timeInMs;
    }

    /**
     * @param float $timeInMs
     */
    public function setTimeInMs($timeInMs)
    {
        $this->timeInMs = $timeInMs;
    }

    /**
     * @return DebugResult
     */
    public function getDebug()
    {
        return $this->debug;
    }

    /**
     * @param DebugResult $debug
     */
    public function setDebug($debug)
    {
        $this->debug = $debug;
    }

    /**
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return AdvancedResult
     */
    public function getAdvanced()
    {
        return $this->advanced;
    }

    /**
     * @param AdvancedResult $advanced
     */
    public function setAdvanced($advanced)
    {
        $this->advanced = $advanced;
    }

    /**
     * @return string
     */
    public function getAttributeName()
    {
        return $this->attributeName;
    }

    /**
     * @param string $attributeName
     */
    public function setAttributeName($attributeName)
    {
        $this->attributeName = $attributeName;
    }

    /**
     * @return string
     */
    public function getAttributeValue()
    {
        return $this->attributeValue;
    }

    /**
     * @param string $attributeValue
     */
    public function setAttributeValue($attributeValue)
    {
        $this->attributeValue = $attributeValue;
    }

    /**
     * @return CategoryNode
     */
    public function getNode()
    {
        return $this->node;
    }

    /**
     * @param CategoryNode $node
     */
    public function setNode($node)
    {
        $this->node = $node;
    }

    /**
     * @return string
     */
    public function getIncludeParents()
    {
        return $this->includeParents;
    }

    /**
     * @param string $includeParents
     */
    public function setIncludeParents($includeParents)
    {
        $this->includeParents = $includeParents;
    }

    /**
     * @return SubCategoryCountSubNode[]
     */
    public function getSubNodes()
    {
        return $this->subNodes;
    }

    /**
     * @param SubCategoryCountSubNode[] $subNodes
     */
    public function setSubNodes($subNodes)
    {
        $this->subNodes = $subNodes;
    }

    /**
     * @return SubCategoryCountSubNode[]
     */
    public function getParentNodes()
    {
        return $this->parentNodes;
    }

    /**
     * @param SubCategoryCountSubNode[] $parentNodes
     */
    public function setParentNodes($parentNodes)
    {
        $this->parentNodes = $parentNodes;
    }
}
