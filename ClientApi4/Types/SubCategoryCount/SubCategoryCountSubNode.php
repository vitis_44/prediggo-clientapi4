<?php

namespace Prediggo\ClientApi4\Types\SubCategoryCount;

class SubCategoryCountSubNode extends CategoryNode
{
    /**
     * @var int
     */
    private $searchableProducts;

    /**
     * @return int
     */
    public function getSearchableProducts()
    {
        return $this->searchableProducts;
    }

    /**
     * @param int $searchableProducts
     */
    public function setSearchableProducts($searchableProducts)
    {
        $this->searchableProducts = $searchableProducts;
    }
}