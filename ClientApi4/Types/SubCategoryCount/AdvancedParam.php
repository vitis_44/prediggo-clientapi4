<?php

namespace Prediggo\ClientApi4\Types\SubCategoryCount;


class AdvancedParam {

    private $customerId;
    private $device;
    private $referrer;
    private $referenceDate;

    /**
     * @var string
     */
    private $offer;

    /**
     * @var string
     */
    private $store;

    /**
     * @var string
     */
    private $url;

    /**
     * @return string
     */
    public function getCustomerId() {
        return $this->customerId;
    }

    /**
     * @param string $customerId
     */
    public function setCustomerId($customerId) {
        $this->customerId = $customerId;
    }

    /**
     * @return string
     */
    public function getDevice() {
        return $this->device;
    }

    /**
     * @param string $device
     */
    public function setDevice($device) {
        $this->device = $device;
    }

    /**
     * @return string
     */
    public function getReferrer() {
        return $this->referrer;
    }

    /**
     * @param string $referrer
     */
    public function setReferrer($referrer) {
        $this->referrer = $referrer;
    }

    /**
     * @return string
     */
    public function getOffer() {
        return $this->offer;
    }

    /**
     * @param string $offer
     */
    public function setOffer($offer) {
        $this->offer = $offer;
    }

    /**
     * @return string
     */
    public function getStore() {
        return $this->store;
    }

    /**
     * @param string $store
     */
    public function setStore($store) {
        $this->store = $store;
    }

    /**
     * @return mixed
     */
    public function getReferenceDate() {
        return $this->referenceDate;
    }

    /**
     * @param mixed $referenceDate
     */
    public function setReferenceDate($referenceDate) {
        $this->referenceDate = $referenceDate;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }
}