<?php

namespace Prediggo\ClientApi4\Types\SubCategoryCount;

class CategoryNode
{
    /**
     * @var string
     */
    private $attributeName;

    /**
     * @var string
     */
    private $attributeType;

    /**
     * @var string
     */
    private $attributeLabel;

    /**
     * @var string
     */
    private $attributeValue;

    /**
     * @var string
     */
    private $attributeValueLabel;

    /**
     * @var array
     */
    private $attributeProperties = array();

    /**
     * @return string
     */
    public function getAttributeName()
    {
        return $this->attributeName;
    }

    /**
     * @param string $attributeName
     */
    public function setAttributeName($attributeName)
    {
        $this->attributeName = $attributeName;
    }

    /**
     * @return string
     */
    public function getAttributeType()
    {
        return $this->attributeType;
    }

    /**
     * @param string $attributeType
     */
    public function setAttributeType($attributeType)
    {
        $this->attributeType = $attributeType;
    }

    /**
     * @return string
     */
    public function getAttributeLabel()
    {
        return $this->attributeLabel;
    }

    /**
     * @param string $attributeLabel
     */
    public function setAttributeLabel($attributeLabel)
    {
        $this->attributeLabel = $attributeLabel;
    }

    /**
     * @return string
     */
    public function getAttributeValue()
    {
        return $this->attributeValue;
    }

    /**
     * @param string $attributeValue
     */
    public function setAttributeValue($attributeValue)
    {
        $this->attributeValue = $attributeValue;
    }

    /**
     * @return string
     */
    public function getAttributeValueLabel()
    {
        return $this->attributeValueLabel;
    }

    /**
     * @param string $attributeValueLabel
     */
    public function setAttributeValueLabel($attributeValueLabel)
    {
        $this->attributeValueLabel = $attributeValueLabel;
    }

    /**
     * @return array
     */
    public function getAttributeProperties()
    {
        return $this->attributeProperties;
    }

    /**
     * @param array $attributeProperties
     */
    public function setAttributeProperties($attributeProperties)
    {
        $this->attributeProperties = $attributeProperties;
    }
}