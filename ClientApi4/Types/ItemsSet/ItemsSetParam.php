<?php

namespace Prediggo\ClientApi4\Types\ItemsSet;

use Prediggo\ClientApi4\ModuleInfo;
use Prediggo\ClientApi4\Types\ApiVersioned;
use Prediggo\ClientApi4\Types\SessionSensitive;

/**
 * ItemsSet parameter class
 */
class ItemsSetParam implements ApiVersioned, SessionSensitive {

    /**
     * @var string
     */
    private $sessionId;

    /**
     * @var string
     */
    private $itemsSetType;
    private $advanced;

    /**
     * @var string
     */
    private $region = "";

    /**
     * @var string
     */
    private $moduleVersion = ModuleInfo::MODULE_VERSION;

    public function __construct() {
        $this->advanced = new AdvancedParam();
    }


    public function getSessionId() {
        return $this->sessionId;
    }

    public function setSessionId($sessionId) {
        $this->sessionId = $sessionId;
    }

    /**
     * @return string
     */
    public function getItemsSetType() {
        return $this->itemsSetType;
    }

    /**
     * @param string $itemsSetType
     */
    public function setItemsSetType($itemsSetType) {
        $this->itemsSetType = $itemsSetType;
    }

    /**
     * @return string
     */
    public function getRegion() {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion($region) {
        $this->region = $region;
    }

    /**
     * @return AdvancedParam
     */
    public function getAdvanced() {
        return $this->advanced;
    }

    /**
     * @return string the version of the module
     */
    function getModuleVersion() {
        return $this->moduleVersion;
    }
}
