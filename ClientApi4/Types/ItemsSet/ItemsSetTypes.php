<?php

namespace Prediggo\ClientApi4\Types\ItemsSet;

/**
 * Possible event constants for ItemsSet calls
 */
class ItemsSetTypes {

    const BASKETED_ITEMS = "BASKETED_ITEMS";
    const VIEWED_ITEMS = "VIEWED_ITEMS";
    const PURCHASED_ITEMS = "PURCHASED_ITEMS";
    const WISHLISTED_ITEMS = "WISHLISTED_ITEMS";
}
