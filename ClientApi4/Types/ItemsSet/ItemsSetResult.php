<?php

namespace Prediggo\ClientApi4\Types\ItemsSet;

use Prediggo\ClientApi4\Types\Debug\DebugResult;
use Prediggo\ClientApi4\Types\Item\ItemInfo;

/**
 * ItemsSet result class
 */
class ItemsSetResult {

    private $status;
    private $sessionId;
    private $region;
    private $timeInMs;

    /**
     * @var DebugResult
     */
    private $debug;

    /**
     * @var ItemInfo[]
     */
    private $items = array();

    /**
     * @var AdvancedResult
     */
    private $advanced;

    public function __construct() {
        $this->advanced = new AdvancedResult();
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getSessionId() {
        return $this->sessionId;
    }

    public function setSessionId($sessionId) {
        $this->sessionId = $sessionId;
    }

    public function getTimeInMs() {
        return $this->timeInMs;
    }

    public function setTimeInMs($timeInMs) {
        $this->timeInMs = $timeInMs;
    }

    /**
     * @return mixed
     */
    public function getRegion() {
        return $this->region;
    }

    /**
     * @param mixed $region
     */
    public function setRegion($region) {
        $this->region = $region;
    }

    /**
     * @return ItemInfo[]
     */
    public function getItems() {
        return $this->items;
    }

    /**
     * @param ItemInfo[] $items
     */
    public function setItems($items) {
        $this->items = $items;
    }

    /**
     * @return AdvancedResult
     */
    public function getAdvanced() {
        return $this->advanced;
    }

    /**
     * @param AdvancedResult $advanced
     */
    public function setAdvanced($advanced) {
        $this->advanced = $advanced;
    }

    /**
     * @return DebugResult
     */
    public function getDebug()
    {
        return $this->debug;
    }

    /**
     * @param DebugResult $debug
     */
    public function setDebug($debug)
    {
        $this->debug = $debug;
    }
}
