<?php

namespace Prediggo\ClientApi4\Types\Ad;

/**
 * Ad campaign
 */
class AdInfo {

    private $label;
    private $mediaUrl;
    private $clickUrl;
    private $trackingCode;
    private $openMethod;
    private $mediaType;

    /**
     * @return string
     */
    public function getLabel() {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label) {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getMediaUrl() {
        return $this->mediaUrl;
    }

    /**
     * @param string $mediaUrl
     */
    public function setMediaUrl($mediaUrl) {
        $this->mediaUrl = $mediaUrl;
    }

    /**
     * @return mixed
     */
    public function getClickUrl() {
        return $this->clickUrl;
    }

    /**
     * @param mixed $clickUrl
     */
    public function setClickUrl($clickUrl) {
        $this->clickUrl = $clickUrl;
    }

    /**
     * @return string
     */
    public function getTrackingCode() {
        return $this->trackingCode;
    }

    /**
     * @param string $trackingCode
     */
    public function setTrackingCode($trackingCode) {
        $this->trackingCode = $trackingCode;
    }

    /**
     * @return string
     */
    public function getOpenMethod() {
        return $this->openMethod;
    }

    /**
     * @param string $openMethod
     */
    public function setOpenMethod($openMethod) {
        $this->openMethod = $openMethod;
    }

    /**
     * @return string
     */
    public function getMediaType() {
        return $this->mediaType;
    }

    /**
     * @param string $mediaType
     */
    public function setMediaType($mediaType) {
        $this->mediaType = $mediaType;
    }

}