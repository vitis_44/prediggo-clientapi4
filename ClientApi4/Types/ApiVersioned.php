<?php

namespace Prediggo\ClientApi4\Types;


interface ApiVersioned {

    /**
     * @return string returns the module version eg: PHP_3.x.x
     */
    function getModuleVersion();
}