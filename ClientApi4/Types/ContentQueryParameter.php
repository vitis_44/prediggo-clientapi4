<?php

namespace Prediggo\ClientApi4\Types;


interface ContentQueryParameter extends SessionSensitive {


    /**
     * @return string
     * @deprecated Languages and zones will be be removed.
     */
    function getLanguageCode();

    /**
     * @param string $languageCode
     * @deprecated Languages and zones will be be removed.
     */
    function setLanguageCode($languageCode);

    /**
     * @return string
     * @deprecated Languages and zones will be be removed.
     */
    function getZone();

    /**
     * @param string $zone
     * @deprecated Languages and zones will be be removed.
     */
    function setZone($zone);


}