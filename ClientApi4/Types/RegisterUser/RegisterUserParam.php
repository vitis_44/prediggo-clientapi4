<?php

namespace Prediggo\ClientApi4\Types\RegisterUser;


use Prediggo\ClientApi4\ModuleInfo;
use Prediggo\ClientApi4\Types\ApiVersioned;
use Prediggo\ClientApi4\Types\SessionSensitive;

class RegisterUserParam implements ApiVersioned, SessionSensitive {

    private $sessionId;
    private $customerId;

    /**
     * @var string
     */
    private $moduleVersion = ModuleInfo::MODULE_VERSION;

    public function getSessionId() {
        return $this->sessionId;
    }

    public function setSessionId($sessionId) {
        $this->sessionId = $sessionId;
    }

    public function getCustomerId() {
        return $this->customerId;
    }

    public function setCustomerId($customerId) {
        $this->customerId = $customerId;
    }

    /**
     * @return string the version of the module
     */
    function getModuleVersion() {
        return $this->moduleVersion;
    }
}