<?php

namespace Prediggo\ClientApi4\Types\RegisterUser;

use Prediggo\ClientApi4\Types\Debug\DebugResult;

class RegisterUserResult
{

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $sessionId;

    /**
     * @var string
     */
    private $customerId;

    /**
     * @var int
     */
    private $timeInMs;

    /**
     * @var DebugResult
     */
    private $debug;

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
    }

    /**
     * @return string
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param string $customerId
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    /**
     * @return int
     */
    public function getTimeInMs()
    {
        return $this->timeInMs;
    }

    /**
     * @param int $timeInMs
     */
    public function setTimeInMs($timeInMs)
    {
        $this->timeInMs = $timeInMs;
    }

    /**
     * @return DebugResult
     */
    public function getDebug()
    {
        return $this->debug;
    }

    /**
     * @param DebugResult $debug
     */
    public function setDebug($debug)
    {
        $this->debug = $debug;
    }
}