<?php

namespace Prediggo\ClientApi4\Types\Track;

use Prediggo\ClientApi4\ModuleInfo;
use Prediggo\ClientApi4\Types\ApiVersioned;
use Prediggo\ClientApi4\Types\SessionSensitive;

/**
 * Track parameter class
 */
class TrackParam implements ApiVersioned, SessionSensitive {

    private $sessionId;
    private $trackingCode;

    /**
     * @var string
     */
    private $moduleVersion = ModuleInfo::MODULE_VERSION;

    /**
     * @return mixed
     */
    public function getSessionId() {
        return $this->sessionId;
    }

    /**
     * @param mixed $sessionId
     */
    public function setSessionId($sessionId) {
        $this->sessionId = $sessionId;
    }

    /**
     * @return mixed
     */
    public function getTrackingCode() {
        return $this->trackingCode;
    }

    /**
     * @param mixed $trackingCode
     */
    public function setTrackingCode($trackingCode) {
        $this->trackingCode = $trackingCode;
    }

    /**
     * @return string the version of the module
     */
    function getModuleVersion() {
        return $this->moduleVersion;
    }
}
