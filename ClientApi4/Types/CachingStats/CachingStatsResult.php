<?php

namespace Prediggo\ClientApi4\Types\CachingStats;

use Prediggo\ClientApi4\Types\Debug\DebugResult;

/**
 * CachingStats result class
 */
class CachingStatsResult {

    private $status;
    private $sessionId;
    private $cachingStatsKey;
    private $timeInMs;
    private $serverNodeId;

    /**
     * @var DebugResult
     */
    private $debug;

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getSessionId() {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     */
    public function setSessionId($sessionId) {
        $this->sessionId = $sessionId;
    }

    /**
     * @return string
     */
    public function getCachingStatsKey() {
        return $this->cachingStatsKey;
    }

    /**
     * @param string $cachingStatsKey
     */
    public function setCachingStatsKey($cachingStatsKey) {
        $this->cachingStatsKey = $cachingStatsKey;
    }

    /**
     * @return float
     */
    public function getTimeInMs() {
        return $this->timeInMs;
    }

    /**
     * @param float $timeInMs
     */
    public function setTimeInMs($timeInMs) {
        $this->timeInMs = $timeInMs;
    }

    /**
     * @return string
     */
    public function getServerNodeId() {
        return $this->serverNodeId;
    }

    /**
     * @param string $serverNodeId
     */
    public function setServerNodeId($serverNodeId) {
        $this->serverNodeId = $serverNodeId;
    }

    /**
     * @return DebugResult
     */
    public function getDebug()
    {
        return $this->debug;
    }

    /**
     * @param DebugResult $debug
     */
    public function setDebug($debug)
    {
        $this->debug = $debug;
    }
}
