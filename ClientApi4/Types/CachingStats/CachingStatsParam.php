<?php

namespace Prediggo\ClientApi4\Types\CachingStats;

use Prediggo\ClientApi4\ModuleInfo;
use Prediggo\ClientApi4\Types\ApiVersioned;
use Prediggo\ClientApi4\Types\SessionSensitive;

/**
 * CachingStats parameter class
 */
class CachingStatsParam implements ApiVersioned, SessionSensitive {

    private $sessionId;
    private $cachingStatsKey;

    /**
     * @var string
     */
    private $moduleVersion = ModuleInfo::MODULE_VERSION;

    /**
     * @return string
     */
    public function getSessionId() {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     */
    public function setSessionId($sessionId) {
        $this->sessionId = $sessionId;
    }

    /**
     * @return string
     */
    public function getCachingStatsKey() {
        return $this->cachingStatsKey;
    }

    /**
     * @param string $cachingStatsKey
     */
    public function setCachingStatsKey($cachingStatsKey) {
        $this->cachingStatsKey = $cachingStatsKey;
    }

    /**
     * @return string the version of the module
     */
    function getModuleVersion() {
        return $this->moduleVersion;
    }
}
