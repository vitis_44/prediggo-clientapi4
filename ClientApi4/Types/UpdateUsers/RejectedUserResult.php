<?php

namespace Prediggo\ClientApi4\Types\UpdateUsers;

class RejectedUserResult extends UpdatedUserResult {

    private $errorMessage;

    /**
     * @return string
     */
    public function getErrorMessage() {
        return $this->errorMessage;
    }

    /**
     * @param string $errorMessage
     */
    public function setErrorMessage($errorMessage) {
        $this->errorMessage = $errorMessage;
    }
}