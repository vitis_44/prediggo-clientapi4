<?php

namespace Prediggo\ClientApi4\Types\UpdateUsers;

use Prediggo\ClientApi4\Types\Debug\DebugResult;

/**
 * Result object for UpdatedUser queries
 */
class UpdateUserResult {

    /**
     * @var string
     */
    private $status;

    /**
     * @var float
     */
    private $timeInMs;

    /**
     * @var UpdatedUserResult[]
     */
    private $updatedItems = array();

    /**
     * @var RejectedUserResult[]
     */
    private $rejectedItems = array();

    /**
     * @var DebugResult
     */
    private $debug;

    public function __construct() {
    }

    /**
     * @return string
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status) {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getTimeInMs() {
        return $this->timeInMs;
    }

    /**
     * @param int $timeInMs
     */
    public function setTimeInMs($timeInMs) {
        $this->timeInMs = $timeInMs;
    }

    /**
     * @return UpdatedUserResult[]
     */
    public function getUpdatedItems()
    {
        return $this->updatedItems;
    }

    /**
     * @param UpdatedUserResult[] $updatedItems
     */
    public function setUpdatedItems($updatedItems)
    {
        $this->updatedItems = $updatedItems;
    }

    /**
     * @return RejectedUserResult[]
     */
    public function getRejectedItems()
    {
        return $this->rejectedItems;
    }

    /**
     * @param RejectedUserResult[] $rejectedItems
     */
    public function setRejectedItems($rejectedItems)
    {
        $this->rejectedItems = $rejectedItems;
    }

    /**
     * @return DebugResult
     */
    public function getDebug()
    {
        return $this->debug;
    }

    /**
     * @param DebugResult $debug
     */
    public function setDebug($debug)
    {
        $this->debug = $debug;
    }
}