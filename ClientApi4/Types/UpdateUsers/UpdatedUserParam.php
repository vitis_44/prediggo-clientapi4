<?php

namespace Prediggo\ClientApi4\Types\UpdateUsers;

class UpdatedUserParam
{
    /**
     * @var string
     */
    private $sessionId = "";

    /**
     * @var string
     */
    private $customerId = "";

    /**
     * @var array
     */
    private $attributes;

    /**
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
    }

    /**
     * @return string
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param string $customerId
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param array $attributes
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * @param string $attribute
     * @param array|string $attributeValues
     */
    public function addAttributes($attribute, $attributeValues) {
        if (!is_array($this->attributes)) {
            $this->attributes = array();
        }

        if (!is_array($attributeValues)) {
            if (!array_key_exists($attribute, $this->attributes)) {
                $this->attributes[$attribute] = array();
            }

            array_push($this->attributes[$attribute], $attributeValues);
        } else {
            foreach ($attributeValues as $value) {
                if (!array_key_exists($attribute, $this->attributes)) {
                    $this->attributes[$attribute] = array();
                }

                array_push($this->attributes[$attribute], $value);
            }
        }
    }
}