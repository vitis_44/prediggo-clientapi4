<?php

namespace Prediggo\ClientApi4\Types\UpdateUsers;

class UpdatedUserResult {

    /**
     * @var string
     */
    private $sessionsId;

    /**
     * @var string
     */
    private $customerId;

    /**
     * @return string
     */
    public function getSessionsId()
    {
        return $this->sessionsId;
    }

    /**
     * @param string $sessionsId
     */
    public function setSessionsId($sessionsId)
    {
        $this->sessionsId = $sessionsId;
    }

    /**
     * @return string
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param string $customerId
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }
}