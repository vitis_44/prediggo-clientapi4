<?php

namespace Prediggo\ClientApi4\Types\UpdateUsers;

use Prediggo\ClientApi4\ModuleInfo;
use Prediggo\ClientApi4\Types\ApiVersioned;

/**
 * Param object for UpdateUser queries
 */
class UpdateUserParam implements ApiVersioned {

    /**
     * @var string
     */
    private $action = "";

    /**
     * @var array
     */
    private $users = array();

    /**
     * @var string
     */
    private $moduleVersion = ModuleInfo::MODULE_VERSION;

    /**
     * @return string
     */
    public function getAction() {
        return $this->action;
    }

    /**
     * @param string $action
     */
    public function setAction($action) {
        $this->action = $action;
    }

    /**
     * @return UpdatedUserParam[]
     */
    public function getUsers() {
        return $this->users;
    }

    /**
     * @param UpdatedUserParam[]|UpdatedUserParam $users
     */
    public function addUser($users) {
        if (is_array($users)) {
            foreach ($users as $user) {
                array_push($this->users, $user);
            }
        } else {
            array_push($this->users, $users);
        }
    }

    /**
     * @return string the version of the module
     */
    function getModuleVersion() {
        return $this->moduleVersion;
    }
}