<?php

namespace Prediggo\ClientApi4\Types\ItemsInfo;

use Prediggo\ClientApi4\Types\Item\ItemInfo;

/**
 * ItemsInteraction result class
 */
class ItemsInfoResult {

    private $status;
    private $sessionId;
    private $timeInMs;

    /**
     * @var string region
     */
    private $region;

    /**
     * @var AdvancedResult
     */
    private $advanced;

    private $debugText;

    /**
     * @var ItemInfo[]
     */
    private $items = array();

    public function __construct() {
        $this->advanced = new AdvancedResult();
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getSessionId() {
        return $this->sessionId;
    }

    public function setSessionId($sessionId) {
        $this->sessionId = $sessionId;
    }

    public function getTimeInMs() {
        return $this->timeInMs;
    }

    public function setTimeInMs($timeInMs) {
        $this->timeInMs = $timeInMs;
    }

    /**
     * @return ItemInfo[]
     */
    public function getItems() {
        return $this->items;
    }

    /**
     * @param ItemInfo[] $items
     */
    public function setItems($items) {
        $this->items = $items;
    }

    /**
     * @param string $sku
     * @param ItemInfo $itemInfo
     */
    public function addItem($sku, $itemInfo) {
        $this->items[$sku] = $itemInfo;
    }

    /**
     * @return string
     */
    public function getRegion() {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion($region) {
        $this->region = $region;
    }

    /**
     * @return AdvancedResult
     */
    public function getAdvanced() {
        return $this->advanced;
    }

    /**
     * @param AdvancedResult $advanced
     */
    public function setAdvanced($advanced) {
        $this->advanced = $advanced;
    }

    /**
     * @return mixed
     */
    public function getDebugText()
    {
        return $this->debugText;
    }

    /**
     * @param mixed $debugText
     */
    public function setDebugText($debugText)
    {
        $this->debugText = $debugText;
    }
}