<?php

namespace Prediggo\ClientApi4\Types\ItemsInfo;

use Prediggo\ClientApi4\ModuleInfo;
use Prediggo\ClientApi4\Types\ApiVersioned;
use Prediggo\ClientApi4\Types\SessionSensitive;

/**
 * ItemsInteraction parameter class
 */
class ItemsInfoParam implements ApiVersioned, SessionSensitive {

    private $sessionId;
    private $eventType;
    private $advanced;

    /**
     * @var string region
     */
    private $region = "";

    /**
     * @var string
     */
    private $moduleVersion = ModuleInfo::MODULE_VERSION;

    /**
     * @var string[]
     */
    private $skus = array();

    public function __construct() {
        $this->advanced = new AdvancedParam();
    }

    public function addSku($sku) {
        $this->skus[] = $sku;
    }

    public function addSkus($skus) {

        foreach ($skus as $sku) {
            $this->addSku($sku);
        }
    }

    public function getSessionId() {
        return $this->sessionId;
    }

    public function setSessionId($sessionId) {
        $this->sessionId = $sessionId;
    }

    public function setEventType($eventType) {
        $this->eventType = $eventType;
    }

    public function getEventType() {
        return $this->eventType;
    }

    /**
     * @return string[]
     */
    public function getSkus() {
        return $this->skus;
    }

    /**
     * @param string[] $skus
     */
    public function setSkus($skus) {
        $this->skus = $skus;
    }

    /**
     * @return string
     */
    public function getRegion() {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion($region) {
        $this->region = $region;
    }

    /**
     * @return AdvancedParam
     */
    public function getAdvanced() {
        return $this->advanced;
    }

    /**
     * @return string the version of the module
     */
    function getModuleVersion() {
        return $this->moduleVersion;
    }
}
