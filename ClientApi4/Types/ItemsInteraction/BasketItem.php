<?php

namespace Prediggo\ClientApi4\Types\ItemsInteraction;

class BasketItem {

    private $sku;
    private $quantity;

    /**
     * @return string
     */
    public function getSku() {
        return $this->sku;
    }

    /**
     * @param string $sku
     */
    public function setSku($sku) {
        $this->sku = $sku;
    }

    /**
     * @return int
     */
    public function getQuantity() {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity) {
        $this->quantity = $quantity;
    }
}