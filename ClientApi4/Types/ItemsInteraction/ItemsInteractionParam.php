<?php

namespace Prediggo\ClientApi4\Types\ItemsInteraction;

use Prediggo\ClientApi4\ModuleInfo;
use Prediggo\ClientApi4\Types\ApiVersioned;
use Prediggo\ClientApi4\Types\SessionSensitive;

/**
 * ItemsInteraction parameter class
 */
class ItemsInteractionParam implements ApiVersioned, SessionSensitive {

    private $sessionId;
    private $eventType;
    private $advanced;

    /**
     * @var string region
     */
    private $region = "";

    /**
     * @var string
     */
    private $moduleVersion = ModuleInfo::MODULE_VERSION;

    private $basket = array();

    public function __construct() {
        $this->advanced = new AdvancedParam();
    }

    public function addSku($sku, $quantity = 1) {

        $item = new BasketItem();
        $item->setSku($sku);
        $item->setQuantity($quantity);
        $this->basket[] = $item;
    }

    public function addSkus($skus, $quantity = 1) {

        foreach ($skus as $sku) {
            $this->addSku($sku, $quantity);
        }
    }

    public function getSessionId() {
        return $this->sessionId;
    }

    public function setSessionId($sessionId) {
        $this->sessionId = $sessionId;
    }

    public function setEventType($eventType) {
        $this->eventType = $eventType;
    }

    public function getEventType() {
        return $this->eventType;
    }

    /**
     * @return BasketItem[] the list of items with their quantity
     */
    public function getBasket() {
        return $this->basket;
    }

    /**
     * @return array
     * @deprecated use getBasket() instead
     */
    public function getSkus() {

        $skus = array();
        foreach ($this->getBasket() as $item) {
            $skus[] = $item->getSku();
        }

        return $skus;
    }

    /**
     * @return string
     */
    public function getRegion() {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion($region) {
        $this->region = $region;
    }

    /**
     * @return AdvancedParam
     */
    public function getAdvanced() {
        return $this->advanced;
    }

    /**
     * @return string the version of the module
     */
    function getModuleVersion() {
        return $this->moduleVersion;
    }
}
