<?php

namespace Prediggo\ClientApi4\Types\ItemsInteraction;

/**
 * ItemsInteraction result class
 */
class ItemsInteractionResult {

    private $sessionId;
    private $timeInMs;

    /**
     * @var AdvancedResult
     */
    private $advanced;

    public function __construct() {
        $this->advanced = new AdvancedResult();
    }

    public function getSessionId() {
        return $this->sessionId;
    }

    public function setSessionId($sessionId) {
        $this->sessionId = $sessionId;
    }

    public function getTimeInMs() {
        return $this->timeInMs;
    }

    public function setTimeInMs($timeInMs) {
        $this->timeInMs = $timeInMs;
    }

    /**
     * @return AdvancedResult
     */
    public function getAdvanced() {
        return $this->advanced;
    }

    /**
     * @param AdvancedResult $advanced
     */
    public function setAdvanced($advanced) {
        $this->advanced = $advanced;
    }
}
