<?php

namespace Prediggo\ClientApi4\Types\UpdateItems;

use Prediggo\ClientApi4\ModuleInfo;
use Prediggo\ClientApi4\Types\ApiVersioned;

/**
 * Param object for UpdatedItems queries
 */
class UpdateItemsParam implements ApiVersioned {

    private $action = "";

    private $items = array();

    /**
     * @var string
     */
    private $moduleVersion = ModuleInfo::MODULE_VERSION;

    /**
     * @return string
     */
    public function getAction() {
        return $this->action;
    }

    /**
     * @param string $action
     */
    public function setAction($action) {
        $this->action = $action;
    }

    /**
     * @return ItemParam[]
     */
    public function getItems() {
        return $this->items;
    }

    /**
     * @param ItemParam[]|ItemParam $items
     */
    public function addItem($items) {
        if (is_array($items)) {
            foreach ($items as $item) {
                array_push($this->items, $item);
            }
        } else {
            array_push($this->items, $items);
        }
    }

    /**
     * @return string the version of the module
     */
    function getModuleVersion() {
        return $this->moduleVersion;
    }
}