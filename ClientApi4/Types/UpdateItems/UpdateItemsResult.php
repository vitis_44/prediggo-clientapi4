<?php

namespace Prediggo\ClientApi4\Types\UpdateItems;

use Prediggo\ClientApi4\Types\Debug\DebugResult;

/**
 * Result object for UpdatedItems queries
 */
class UpdateItemsResult {

    /**
     * @var string
     */
    private $status;

    /**
     * @var float
     */
    private $timeInMs;

    /**
     * @var string
     */
    private $sessionId;

    /**
     * @var string
     */
    private $region;

    /**
     * @var UpdatedItemsResult[]
     */
    private $updatedItems = array();

    /**
     * @var RejectedItemsResult[]
     */
    private $rejectedItems = array();

    /**
     * @var DebugResult
     */
    private $debug;

    /**
     * @var string
     */
    private $debugText;

    public function __construct() {
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return float
     */
    public function getTimeInMs()
    {
        return $this->timeInMs;
    }

    /**
     * @param float $timeInMs
     */
    public function setTimeInMs($timeInMs)
    {
        $this->timeInMs = $timeInMs;
    }

    /**
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return UpdatedItemsResult[]
     */
    public function getUpdatedItems()
    {
        return $this->updatedItems;
    }

    /**
     * @param UpdatedItemsResult[] $updatedItems
     */
    public function setUpdatedItems($updatedItems)
    {
        $this->updatedItems = $updatedItems;
    }

    /**
     * @return RejectedItemsResult[]
     */
    public function getRejectedItems()
    {
        return $this->rejectedItems;
    }

    /**
     * @param RejectedItemsResult[] $rejectedItems
     */
    public function setRejectedItems($rejectedItems)
    {
        $this->rejectedItems = $rejectedItems;
    }

    /**
     * @return DebugResult
     */
    public function getDebug()
    {
        return $this->debug;
    }

    /**
     * @param DebugResult $debug
     */
    public function setDebug($debug)
    {
        $this->debug = $debug;
    }

    /**
     * @return string
     */
    public function getDebugText()
    {
        return $this->debugText;
    }

    /**
     * @param string $debugText
     */
    public function setDebugText($debugText)
    {
        $this->debugText = $debugText;
    }
}