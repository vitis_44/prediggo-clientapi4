<?php

namespace Prediggo\ClientApi4\Types\UpdateItems;

class ItemParam
{

    /**
     * @var string
     */
    private $sku = "";

    /**
     * @var string
     */
    private $zoneName;

    /**
     * @var float
     */
    private $price;

    /**
     * @var float
     */
    private $standardPrice;

    /**
     * @var string
     */
    private $stock;

    /**
     * @var bool
     */
    private $recommendable;

    /**
     * @var bool
     */
    private $searchable;

    /**
     * @var string
     */
    private $availability;

    /**
     * @var string
     */
    private $region;

    /**
     * @var string
     */
    private $offer;

    /**
     * @var string
     */
    private $store;

    /**
     * @var array
     */
    private $attributes;

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return string
     */
    public function getZoneName()
    {
        return $this->zoneName;
    }

    /**
     * @param string $zoneName
     */
    public function setZoneName($zoneName)
    {
        $this->zoneName = $zoneName;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return float
     */
    public function getStandardPrice()
    {
        return $this->standardPrice;
    }

    /**
     * @param float $standardPrice
     */
    public function setStandardPrice($standardPrice)
    {
        $this->standardPrice = $standardPrice;
    }

    /**
     * @return string
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param string $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return bool
     */
    public function isRecommendable()
    {
        return $this->recommendable;
    }

    /**
     * @param bool $recommendable
     */
    public function setRecommendable($recommendable)
    {
        $this->recommendable = $recommendable;
    }

    /**
     * @return bool
     */
    public function isSearchable()
    {
        return $this->searchable;
    }

    /**
     * @param bool $searchable
     */
    public function setSearchable($searchable)
    {
        $this->searchable = $searchable;
    }

    /**
     * @return string
     */
    public function getAvailability()
    {
        return $this->availability;
    }

    /**
     * @param string $availability
     */
    public function setAvailability($availability)
    {
        $this->availability = $availability;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return string
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * @param string $offer
     */
    public function setOffer($offer)
    {
        $this->offer = $offer;
    }

    /**
     * @return string
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * @param string $store
     */
    public function setStore($store)
    {
        $this->store = $store;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param array $attributes
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * @param string $attribute
     * @param array|string $attributeValues
     */
    public function addAttributes($attribute, $attributeValues)
    {
        if (!is_array($this->attributes)) {
            $this->attributes = array();
        }

        if (!is_array($attributeValues)) {
            if (!array_key_exists($attribute, $this->attributes)) {
                $this->attributes[$attribute] = array();
            }

            array_push($this->attributes[$attribute], $attributeValues);
        } else {
            foreach ($attributeValues as $value) {
                if (!array_key_exists($attribute, $this->attributes)) {
                    $this->attributes[$attribute] = array();
                }

                array_push($this->attributes[$attribute], $value);
            }
        }
    }
}