<?php

namespace Prediggo\ClientApi4\Types;


interface RegionContentQueryParameter extends SessionSensitive {
    /**
     * @return string
     */
    function getRegion();

    /**
     * @param string $region
     */
    function setRegion($region);
}