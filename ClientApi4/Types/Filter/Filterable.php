<?php

namespace Prediggo\ClientApi4\Types\Filter;

/**
 * Base interface for parameter objects that accept filters among their parameters
 */
interface Filterable {

    function getFilters();

    function setFilters($filter);

}
