<?php

namespace Prediggo\ClientApi4\Types\Filter;

/**
 * Base contract for key => values[] based filters
 */
interface FilterSupplier {

    public function filters();
}
