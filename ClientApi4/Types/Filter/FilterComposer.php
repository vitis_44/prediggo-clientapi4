<?php

namespace Prediggo\ClientApi4\Types\Filter;

/**
 * Builder-like class for constructing filters
 */
class FilterComposer implements FilterSupplier {

    private $filters = array();

    public static function newInstance() {
        return new FilterComposer();
    }

    public function addFilter($attributeName, $attributeValue) {

        $values = null;
        if (array_key_exists($attributeName, $this->filters)) {
            $values = $this->filters[$attributeName];
        }

        if ($values === null) {
            $values = [$attributeValue];
        } else {
            $values[] = $attributeValue;
        }

        $this->filters[$attributeName] = $values;
        return $this;
    }

    public function addFilterMinMax($attributeName, $min, $max, $minInclusive, $maxInclusive) {

        $attValue = ($minInclusive ? "[" : "(")
            . $min
            . ","
            . $max
            . ($maxInclusive ? "]" : ")");

        return $this->addFilter($attributeName, $attValue);
    }


    public function filters() {
        return $this->filters;
    }
}
