<?php

namespace Prediggo\ClientApi4\Types\Filter;

/**
 * Filter support class
 */
class Filters {

    /**
     * A default FilterSupplier without any condition
     *
     * @return FilterComposer
     */
    public static function blank() {
        return new FilterComposer();
    }


    /**
     * @param $array a key => value associative array where
     * key  = attribute name,
     * value = an array of values
     * @return FilterComposer
     */
    public static function fromAssociativeArray($array) {
        $filter = new FilterComposer();

        foreach ($array as $key => $values) {

            if (!is_array($values)) {
                $filter->addFilter($key, $values);
            } else {

                foreach ($values as $val) {
                    $filter->addFilter($key, $val);
                }
            }
        }

        return $filter;
    }
}
