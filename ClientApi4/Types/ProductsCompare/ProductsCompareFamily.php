<?php

namespace Prediggo\ClientApi4\Types\ProductsCompare;

use Prediggo\ClientApi4\Types\PageContent\FrontResult;

class ProductsCompareFamily
{
    /**
     * @var string
     */
    private $familyName;

    /**
     * @var FrontResult
     */
    private $front;

    /**
     * @var array
     */
    private $items = array();

    /**
     * @var ProductsCompareGroup[]
     */
    private $groups = array();

    /**
     * @return string
     */
    public function getFamilyName()
    {
        return $this->familyName;
    }

    /**
     * @param string $familyName
     */
    public function setFamilyName($familyName)
    {
        $this->familyName = $familyName;
    }

    /**
     * @return FrontResult
     */
    public function getFront()
    {
        return $this->front;
    }

    /**
     * @param FrontResult $front
     */
    public function setFront($front)
    {
        $this->front = $front;
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param array $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }

    /**
     * @return ProductsCompareGroup[]
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @param ProductsCompareGroup[] $groups
     */
    public function setGroups($groups)
    {
        $this->groups = $groups;
    }
}