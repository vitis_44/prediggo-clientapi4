<?php

namespace Prediggo\ClientApi4\Types\ProductsCompare;

use Prediggo\ClientApi4\ModuleInfo;
use Prediggo\ClientApi4\Types\ApiVersioned;
use Prediggo\ClientApi4\Types\SessionSensitive;

/**
 * ProductsCompare parameter class
 */
class ProductsCompareParam implements ApiVersioned, SessionSensitive {

    /**
     * @var string
     */
    private $moduleVersion = ModuleInfo::MODULE_VERSION;

    /**
     * @var string
     */
    private $sessionId;

    /**
     * @var string
     */
    private $region = "";

    /**
     * @var string
     */
    private $languageCode = "";

    /**
     * @var string[]
     */
    private $skus = array();

    /**
     * @var AdvancedParam
     */
    private $advanced;

    public function __construct() {
        $this->advanced = new AdvancedParam();
    }

    /**
     * @return string
     */
    public function getModuleVersion()
    {
        return $this->moduleVersion;
    }

    /**
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return string
     */
    public function getLanguageCode()
    {
        return $this->languageCode;
    }

    /**
     * @param string $languageCode
     */
    public function setLanguageCode($languageCode)
    {
        $this->languageCode = $languageCode;
    }

    /**
     * @return string[]
     */
    public function getSkus()
    {
        return $this->skus;
    }

    /**
     * @param string[] $skus
     */
    public function setSkus($skus)
    {
        $this->skus = $skus;
    }

    /**
     * @return AdvancedParam
     */
    public function getAdvanced()
    {
        return $this->advanced;
    }

    /**
     * @param AdvancedParam $advanced
     */
    public function setAdvanced($advanced)
    {
        $this->advanced = $advanced;
    }
}
