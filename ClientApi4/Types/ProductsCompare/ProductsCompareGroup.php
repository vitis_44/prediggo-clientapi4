<?php

namespace Prediggo\ClientApi4\Types\ProductsCompare;

use Prediggo\ClientApi4\Types\PageContent\FrontResult;

class ProductsCompareGroup
{
    /**
     * @var string
     */
    private $groupName;

    /**
     * @var int
     */
    private $position;

    /**
     * @var FrontResult
     */
    private $front;

    /**
     * @var ProductsCompareAttribute[]
     */
    private $attributes = array();

    /**
     * @return string
     */
    public function getGroupName()
    {
        return $this->groupName;
    }

    /**
     * @param string $groupName
     */
    public function setGroupName($groupName)
    {
        $this->groupName = $groupName;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return FrontResult
     */
    public function getFront()
    {
        return $this->front;
    }

    /**
     * @param FrontResult $front
     */
    public function setFront($front)
    {
        $this->front = $front;
    }

    /**
     * @return ProductsCompareAttribute[]
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param ProductsCompareAttribute[] $attributes
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
    }
}