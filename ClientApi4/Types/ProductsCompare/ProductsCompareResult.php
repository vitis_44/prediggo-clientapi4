<?php

namespace Prediggo\ClientApi4\Types\ProductsCompare;

use Prediggo\ClientApi4\Types\Debug\DebugResult;

/**
 * ProductsCompare result class
 */
class ProductsCompareResult {

    /**
     * @var string
     */
    private $status;

    /**
     * @var float
     */
    private $timeInMs;

    /**
     * @var DebugResult
     */
    private $debug;

    /**
     * @var string
     */
    private $sessionId;

    /**
     * @var AdvancedResult
     */
    private $advanced;

    /**
     * @var string[]
     */
    private $skusWithoutFamily = array();

    /**
     * @var string[]
     */
    private $unknownSkus = array();

    /**
     * @var ProductsCompareFamily[]
     */
    private $families = array();

    public function __construct()
    {
        $this->advanced = new AdvancedResult();
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return float
     */
    public function getTimeInMs()
    {
        return $this->timeInMs;
    }

    /**
     * @param float $timeInMs
     */
    public function setTimeInMs($timeInMs)
    {
        $this->timeInMs = $timeInMs;
    }

    /**
     * @return DebugResult
     */
    public function getDebug()
    {
        return $this->debug;
    }

    /**
     * @param DebugResult $debug
     */
    public function setDebug($debug)
    {
        $this->debug = $debug;
    }

    /**
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
    }

    /**
     * @return AdvancedResult
     */
    public function getAdvanced()
    {
        return $this->advanced;
    }

    /**
     * @param AdvancedResult $advanced
     */
    public function setAdvanced($advanced)
    {
        $this->advanced = $advanced;
    }

    /**
     * @return string[]
     */
    public function getSkusWithoutFamily()
    {
        return $this->skusWithoutFamily;
    }

    /**
     * @param string[] $skusWithoutFamily
     */
    public function setSkusWithoutFamily($skusWithoutFamily)
    {
        $this->skusWithoutFamily = $skusWithoutFamily;
    }

    /**
     * @return string[]
     */
    public function getUnknownSkus()
    {
        return $this->unknownSkus;
    }

    /**
     * @param string[] $unknownSkus
     */
    public function setUnknownSkus($unknownSkus)
    {
        $this->unknownSkus = $unknownSkus;
    }

    /**
     * @return ProductsCompareFamily[]
     */
    public function getFamilies()
    {
        return $this->families;
    }

    /**
     * @param ProductsCompareFamily[] $families
     */
    public function setFamilies($families)
    {
        $this->families = $families;
    }
}
