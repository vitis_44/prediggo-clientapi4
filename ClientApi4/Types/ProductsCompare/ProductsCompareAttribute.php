<?php

namespace Prediggo\ClientApi4\Types\ProductsCompare;

class ProductsCompareAttribute
{
    /**
     * @var string
     */
    private $attributeName;

    /**
     * @var string
     */
    private $attributeLabel;

    /**
     * @var string
     */
    private $attributeType;

    /**
     * @var string
     */
    private $attributeUnit;

    /**
     * @var int
     */
    private $position;

    /**
     * @var array
     */
    private $items = array();

    /**
     * @return string
     */
    public function getAttributeName()
    {
        return $this->attributeName;
    }

    /**
     * @param string $attributeName
     */
    public function setAttributeName($attributeName)
    {
        $this->attributeName = $attributeName;
    }

    /**
     * @return string
     */
    public function getAttributeLabel()
    {
        return $this->attributeLabel;
    }

    /**
     * @param string $attributeLabel
     */
    public function setAttributeLabel($attributeLabel)
    {
        $this->attributeLabel = $attributeLabel;
    }

    /**
     * @return string
     */
    public function getAttributeType()
    {
        return $this->attributeType;
    }

    /**
     * @param string $attributeType
     */
    public function setAttributeType($attributeType)
    {
        $this->attributeType = $attributeType;
    }

    /**
     * @return string
     */
    public function getAttributeUnit()
    {
        return $this->attributeUnit;
    }

    /**
     * @param string $attributeUnit
     */
    public function setAttributeUnit($attributeUnit)
    {
        $this->attributeUnit = $attributeUnit;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param array $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }
}