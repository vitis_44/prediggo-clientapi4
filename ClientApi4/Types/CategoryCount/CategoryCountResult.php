<?php

namespace Prediggo\ClientApi4\Types\CategoryCount;

use Prediggo\ClientApi4\Types\Debug\DebugResult;

/**
 * CategoryCount result class
 */
class CategoryCountResult {

    /**
     * @var string
     */
    private $status;

    /**
     * @var float
     */
    private $timeInMs;

    /**
     * @var DebugResult
     */
    private $debug;

    /**
     * @var string
     */
    private $sessionId;

    /**
     * @var string
     */
    private $region = "";

    /**
     * @var AdvancedResult
     */
    private $advanced;

    /**
     * @var string
     */
    private $attributeName;

    /**
     * @var CategoryCountSubNode[]
     */
    private $nodes = array();

    public function __construct()
    {
        $this->advanced = new AdvancedResult();
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return float
     */
    public function getTimeInMs()
    {
        return $this->timeInMs;
    }

    /**
     * @param float $timeInMs
     */
    public function setTimeInMs($timeInMs)
    {
        $this->timeInMs = $timeInMs;
    }

    /**
     * @return DebugResult
     */
    public function getDebug()
    {
        return $this->debug;
    }

    /**
     * @param DebugResult $debug
     */
    public function setDebug($debug)
    {
        $this->debug = $debug;
    }

    /**
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return AdvancedResult
     */
    public function getAdvanced()
    {
        return $this->advanced;
    }

    /**
     * @param AdvancedResult $advanced
     */
    public function setAdvanced($advanced)
    {
        $this->advanced = $advanced;
    }

    /**
     * @return string
     */
    public function getAttributeName()
    {
        return $this->attributeName;
    }

    /**
     * @param string $attributeName
     */
    public function setAttributeName($attributeName)
    {
        $this->attributeName = $attributeName;
    }

    /**
     * @return CategoryCountSubNode[]
     */
    public function getNodes()
    {
        return $this->nodes;
    }

    /**
     * @param CategoryCountSubNode[] $nodes
     */
    public function setNodes($nodes)
    {
        $this->nodes = $nodes;
    }
}
