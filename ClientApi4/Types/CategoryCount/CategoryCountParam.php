<?php

namespace Prediggo\ClientApi4\Types\CategoryCount;

use Prediggo\ClientApi4\ModuleInfo;
use Prediggo\ClientApi4\Types\ApiVersioned;
use Prediggo\ClientApi4\Types\SessionSensitive;

/**
 * CategoryCount parameter class
 */
class CategoryCountParam implements ApiVersioned, SessionSensitive {

    /**
     * @var string
     */
    private $attributeName;

    /**
     * @var string
     */
    private $languageCode = "";

    /**
     * @var string
     */
    private $sessionId;

    /**
     * @var AdvancedParam
     */
    private $advanced;

    /**
     * @var string
     */
    private $moduleVersion = ModuleInfo::MODULE_VERSION;

    /**
     * @var string
     */
    private $region = "";

    public function __construct() {
        $this->advanced = new AdvancedParam();
    }

    /**
     * @return string
     */
    public function getAttributeName()
    {
        return $this->attributeName;
    }

    /**
     * @param string $attributeName
     */
    public function setAttributeName($attributeName)
    {
        $this->attributeName = $attributeName;
    }

    /**
     * @return string
     * @deprecated Languages and zones will be be removed.
     */
    function getLanguageCode()
    {
        return $this->languageCode;
    }

    /**
     * @param string $languageCode
     * @deprecated Languages and zones will be be removed.
     */
    function setLanguageCode($languageCode)
    {
        $this->languageCode = $languageCode;
    }

    /**
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
    }

    /**
     * @return AdvancedParam
     */
    public function getAdvanced()
    {
        return $this->advanced;
    }

    /**
     * @param AdvancedParam $advanced
     */
    public function setAdvanced($advanced)
    {
        $this->advanced = $advanced;
    }

    /**
     * @return string
     */
    public function getModuleVersion()
    {
        return $this->moduleVersion;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }
}
