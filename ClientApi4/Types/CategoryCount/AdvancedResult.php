<?php

namespace Prediggo\ClientApi4\Types\CategoryCount;


class AdvancedResult {

    private $customerId;
    private $device;
    private $referenceDate;
    private $referrer;

    /**
     * @var string
     */
    private $offer;

    /**
     * @var string
     */
    private $store;

    /**
     * @var string
     */
    private $url;

    /**
     * @return string
     */
    public function getCustomerId() {
        return $this->customerId;
    }

    /**
     * @param string $customerId
     */
    public function setCustomerId($customerId) {
        $this->customerId = $customerId;
    }

    /**
     * @return string
     */
    public function getDevice() {
        return $this->device;
    }

    /**
     * @param string $device
     */
    public function setDevice($device) {
        $this->device = $device;
    }

    /**
     * @return string
     */
    public function getReferenceDate() {
        return $this->referenceDate;
    }

    /**
     * @param string $referenceDate
     */
    public function setReferenceDate($referenceDate) {
        $this->referenceDate = $referenceDate;
    }

    /**
     * @return string
     */
    public function getReferrer() {
        return $this->referrer;
    }

    /**
     * @param string $referrer
     */
    public function setReferrer($referrer) {
        $this->referrer = $referrer;
    }

    /**
     * @return string
     */
    public function getOffer() {
        return $this->offer;
    }

    /**
     * @param string $offer
     */
    public function setOffer($offer) {
        $this->offer = $offer;
    }

    /**
     * @return string
     */
    public function getStore() {
        return $this->store;
    }

    /**
     * @param string $store
     */
    public function setStore($store) {
        $this->store = $store;
    }

    /**
     * @return AdvancedResult
     */
    public function getAdvanced() {
        return $this->advanced;
    }

    /**
     * @param AdvancedResult $advanced
     */
    public function setAdvanced($advanced) {
        $this->advanced = $advanced;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }
}