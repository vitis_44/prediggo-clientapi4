<?php

namespace Prediggo\ClientApi4\Types\ProductAvailability;

use Prediggo\ClientApi4\ModuleInfo;
use Prediggo\ClientApi4\Types\ApiVersioned;
use Prediggo\ClientApi4\Types\SessionSensitive;

/**
 * ProductAvailability parameter class
 */
class ProductAvailabilityParam implements ApiVersioned, SessionSensitive {

    /**
     * @var string
     */
    private $sku;

    /**
     * @var array
     */
    private $attributesToReturn;

    /**
     * @var string
     */
    private $sessionId;

    /**
     * @var string
     */
    private $itemsSetType;

    /**
     * @var AdvancedResult
     */
    private $advanced;

    /**
     * @var string
     */
    private $region = "";

    /**
     * @var string
     */
    private $moduleVersion = ModuleInfo::MODULE_VERSION;

    public function __construct() {
        $this->advanced = new AdvancedParam();
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return array
     */
    public function getAttributesToReturn()
    {
        return $this->attributesToReturn;
    }

    /**
     * @param array $attributesToReturn
     */
    public function setAttributesToReturn($attributesToReturn)
    {
        $this->attributesToReturn = $attributesToReturn;
    }

    /**
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
    }

    /**
     * @return string
     */
    public function getItemsSetType()
    {
        return $this->itemsSetType;
    }

    /**
     * @param string $itemsSetType
     */
    public function setItemsSetType($itemsSetType)
    {
        $this->itemsSetType = $itemsSetType;
    }

    /**
     * @return AdvancedResult
     */
    public function getAdvanced()
    {
        return $this->advanced;
    }

    /**
     * @param AdvancedResult $advanced
     */
    public function setAdvanced($advanced)
    {
        $this->advanced = $advanced;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return string
     */
    public function getModuleVersion()
    {
        return $this->moduleVersion;
    }
}
