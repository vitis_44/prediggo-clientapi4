<?php

namespace Prediggo\ClientApi4\Types\ProductAvailability;

class ProductInfoAttribute
{
    /**
     * @var string
     */
    private $attributeName;

    /**
     * @var string
     */
    private $attributeType;

    /**
     * @var string
     */
    private $attributeLabel;

    /**
     * @var ProductInfoValueLabelPair[]
     */
    private $vals;

    /**
     * @return string
     */
    public function getAttributeName()
    {
        return $this->attributeName;
    }

    /**
     * @param string $attributeName
     */
    public function setAttributeName($attributeName)
    {
        $this->attributeName = $attributeName;
    }

    /**
     * @return string
     */
    public function getAttributeType()
    {
        return $this->attributeType;
    }

    /**
     * @param string $attributeType
     */
    public function setAttributeType($attributeType)
    {
        $this->attributeType = $attributeType;
    }

    /**
     * @return string
     */
    public function getAttributeLabel()
    {
        return $this->attributeLabel;
    }

    /**
     * @param string $attributeLabel
     */
    public function setAttributeLabel($attributeLabel)
    {
        $this->attributeLabel = $attributeLabel;
    }

    /**
     * @return ProductInfoValueLabelPair[]
     */
    public function getVals()
    {
        return $this->vals;
    }

    /**
     * @param ProductInfoValueLabelPair[] $vals
     */
    public function setVals($vals)
    {
        $this->vals = $vals;
    }
}