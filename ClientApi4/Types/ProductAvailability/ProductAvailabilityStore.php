<?php

namespace Prediggo\ClientApi4\Types\ProductAvailability;

class ProductAvailabilityStore
{
    /**
     * @var string
     */
    private $storeCode;

    /**
     * @var string
     */
    private $storeLabel;

    /**
     * @var ProductInfoAttribute[]
     */
    private $attributeInfo;

    /**
     * @return string
     */
    public function getStoreCode()
    {
        return $this->storeCode;
    }

    /**
     * @param string $storeCode
     */
    public function setStoreCode($storeCode)
    {
        $this->storeCode = $storeCode;
    }

    /**
     * @return string
     */
    public function getStoreLabel()
    {
        return $this->storeLabel;
    }

    /**
     * @param string $storeLabel
     */
    public function setStoreLabel($storeLabel)
    {
        $this->storeLabel = $storeLabel;
    }

    /**
     * @return ProductInfoAttribute[]
     */
    public function getAttributeInfo()
    {
        return $this->attributeInfo;
    }

    /**
     * @param ProductInfoAttribute[] $attributeInfo
     */
    public function setAttributeInfo($attributeInfo)
    {
        $this->attributeInfo = $attributeInfo;
    }
}