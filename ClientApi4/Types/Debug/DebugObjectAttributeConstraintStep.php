<?php

namespace Prediggo\ClientApi4\Types\Debug;

class DebugObjectAttributeConstraintStep
{
    /**
     * @var string
     */
    private $stepName;

    /**
     * @var int
     */
    private $nbCandidatesBefore;

    /**
     * @var int
     */
    private $nbCandidatesAfter;

    /**
     * @var double
     */
    private $timeInMs;

    /**
     * @var DebugObjectAttributeConstraint[]
     */
    private $attributeConstraintList = array();

    /**
     * @return string
     */
    public function getStepName()
    {
        return $this->stepName;
    }

    /**
     * @param string $stepName
     */
    public function setStepName($stepName)
    {
        $this->stepName = $stepName;
    }

    /**
     * @return int
     */
    public function getNbCandidatesBefore()
    {
        return $this->nbCandidatesBefore;
    }

    /**
     * @param int $nbCandidatesBefore
     */
    public function setNbCandidatesBefore($nbCandidatesBefore)
    {
        $this->nbCandidatesBefore = $nbCandidatesBefore;
    }

    /**
     * @return int
     */
    public function getNbCandidatesAfter()
    {
        return $this->nbCandidatesAfter;
    }

    /**
     * @param int $nbCandidatesAfter
     */
    public function setNbCandidatesAfter($nbCandidatesAfter)
    {
        $this->nbCandidatesAfter = $nbCandidatesAfter;
    }

    /**
     * @return float
     */
    public function getTimeInMs()
    {
        return $this->timeInMs;
    }

    /**
     * @param float $timeInMs
     */
    public function setTimeInMs($timeInMs)
    {
        $this->timeInMs = $timeInMs;
    }

    /**
     * @return DebugObjectAttributeConstraint[]
     */
    public function getAttributeConstraintList()
    {
        return $this->attributeConstraintList;
    }

    /**
     * @param DebugObjectAttributeConstraint[] $attributeConstraintList
     */
    public function setAttributeConstraintList($attributeConstraintList)
    {
        $this->attributeConstraintList = $attributeConstraintList;
    }
}