<?php

namespace Prediggo\ClientApi4\Types\Debug;

class DebugObjectPage
{
    /**
     * @var string
     */
    private $pageLabel;

    /**
     * @var string
     */
    private $pageCode;

    /**
     * @var DebugObjectBlock[]
     */
    private $debugObjectBlockList = array();

    /**
     * @return string
     */
    public function getPageLabel()
    {
        return $this->pageLabel;
    }

    /**
     * @param string $pageLabel
     */
    public function setPageLabel($pageLabel)
    {
        $this->pageLabel = $pageLabel;
    }

    /**
     * @return string
     */
    public function getPageCode()
    {
        return $this->pageCode;
    }

    /**
     * @param string $pageCode
     */
    public function setPageCode($pageCode)
    {
        $this->pageCode = $pageCode;
    }

    /**
     * @return DebugObjectBlock[]
     */
    public function getDebugObjectBlockList()
    {
        return $this->debugObjectBlockList;
    }

    /**
     * @param DebugObjectBlock[] $debugObjectBlockList
     */
    public function setDebugObjectBlockList($debugObjectBlockList)
    {
        $this->debugObjectBlockList = $debugObjectBlockList;
    }
}