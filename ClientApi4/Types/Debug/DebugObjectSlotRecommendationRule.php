<?php

namespace Prediggo\ClientApi4\Types\Debug;

class DebugObjectSlotRecommendationRule
{
    /**
     * @var string
     */
    private $ruleTemplateCode;

    /**
     * @var string
     */
    private $ruleTemplateLabel;

    /**
     * @var double
     */
    private $computationTimeInMs;

    /**
     * @var string
     */
    private $computationStatus;

    /**
     * @var string
     */
    private $candidatesRetrievalStatus;

    /**
     * @var int
     */
    private $nbValidCandidatesBeforeConstraints;

    /**
     * @var int
     */
    private $nbValidCandidatesMatchingConstraints;

    /**
     * @var int
     */
    private $nbValidCandidatesBeforeStrategy;

    /**
     * @var bool
     */
    private $strategyFoundResult;

    /**
     * @var string
     */
    private $strategyCode;

    /**
     * @var string
     */
    private $strategyLabel;

    /**
     * @var string
     */
    private $resultItemId;

    /**
     * @var DebugObjectAttributeConstraintStep[]
     */
    private $attributeConstraintsComputationSteps = array();

    /**
     * @return string
     */
    public function getRuleTemplateCode()
    {
        return $this->ruleTemplateCode;
    }

    /**
     * @param string $ruleTemplateCode
     */
    public function setRuleTemplateCode($ruleTemplateCode)
    {
        $this->ruleTemplateCode = $ruleTemplateCode;
    }

    /**
     * @return string
     */
    public function getRuleTemplateLabel()
    {
        return $this->ruleTemplateLabel;
    }

    /**
     * @param string $ruleTemplateLabel
     */
    public function setRuleTemplateLabel($ruleTemplateLabel)
    {
        $this->ruleTemplateLabel = $ruleTemplateLabel;
    }

    /**
     * @return float
     */
    public function getComputationTimeInMs()
    {
        return $this->computationTimeInMs;
    }

    /**
     * @param float $computationTimeInMs
     */
    public function setComputationTimeInMs($computationTimeInMs)
    {
        $this->computationTimeInMs = $computationTimeInMs;
    }

    /**
     * @return string
     */
    public function getComputationStatus()
    {
        return $this->computationStatus;
    }

    /**
     * @param string $computationStatus
     */
    public function setComputationStatus($computationStatus)
    {
        $this->computationStatus = $computationStatus;
    }

    /**
     * @return string
     */
    public function getCandidatesRetrievalStatus()
    {
        return $this->candidatesRetrievalStatus;
    }

    /**
     * @param string $candidatesRetrievalStatus
     */
    public function setCandidatesRetrievalStatus($candidatesRetrievalStatus)
    {
        $this->candidatesRetrievalStatus = $candidatesRetrievalStatus;
    }

    /**
     * @return int
     */
    public function getNbValidCandidatesBeforeConstraints()
    {
        return $this->nbValidCandidatesBeforeConstraints;
    }

    /**
     * @param int $nbValidCandidatesBeforeConstraints
     */
    public function setNbValidCandidatesBeforeConstraints($nbValidCandidatesBeforeConstraints)
    {
        $this->nbValidCandidatesBeforeConstraints = $nbValidCandidatesBeforeConstraints;
    }

    /**
     * @return int
     */
    public function getNbValidCandidatesMatchingConstraints()
    {
        return $this->nbValidCandidatesMatchingConstraints;
    }

    /**
     * @param int $nbValidCandidatesMatchingConstraints
     */
    public function setNbValidCandidatesMatchingConstraints($nbValidCandidatesMatchingConstraints)
    {
        $this->nbValidCandidatesMatchingConstraints = $nbValidCandidatesMatchingConstraints;
    }

    /**
     * @return int
     */
    public function getNbValidCandidatesBeforeStrategy()
    {
        return $this->nbValidCandidatesBeforeStrategy;
    }

    /**
     * @param int $nbValidCandidatesBeforeStrategy
     */
    public function setNbValidCandidatesBeforeStrategy($nbValidCandidatesBeforeStrategy)
    {
        $this->nbValidCandidatesBeforeStrategy = $nbValidCandidatesBeforeStrategy;
    }

    /**
     * @return bool
     */
    public function isStrategyFoundResult()
    {
        return $this->strategyFoundResult;
    }

    /**
     * @param bool $strategyFoundResult
     */
    public function setStrategyFoundResult($strategyFoundResult)
    {
        $this->strategyFoundResult = $strategyFoundResult;
    }

    /**
     * @return string
     */
    public function getStrategyCode()
    {
        return $this->strategyCode;
    }

    /**
     * @param string $strategyCode
     */
    public function setStrategyCode($strategyCode)
    {
        $this->strategyCode = $strategyCode;
    }

    /**
     * @return string
     */
    public function getStrategyLabel()
    {
        return $this->strategyLabel;
    }

    /**
     * @param string $strategyLabel
     */
    public function setStrategyLabel($strategyLabel)
    {
        $this->strategyLabel = $strategyLabel;
    }

    /**
     * @return string
     */
    public function getResultItemId()
    {
        return $this->resultItemId;
    }

    /**
     * @param string $resultItemId
     */
    public function setResultItemId($resultItemId)
    {
        $this->resultItemId = $resultItemId;
    }

    /**
     * @return DebugObjectAttributeConstraintStep[]
     */
    public function getAttributeConstraintsComputationSteps()
    {
        return $this->attributeConstraintsComputationSteps;
    }

    /**
     * @param DebugObjectAttributeConstraintStep[] $attributeConstraintsComputationSteps
     */
    public function setAttributeConstraintsComputationSteps($attributeConstraintsComputationSteps)
    {
        $this->attributeConstraintsComputationSteps = $attributeConstraintsComputationSteps;
    }
}