<?php

namespace Prediggo\ClientApi4\Types\Debug;

class DebugResult
{
    /**
     * @var string
     */
    private $moduleVersion;

    /**
     * @var string
     */
    private $serverNodeId;

    /**
     * @var string
     */
    private $debugText;

    /**
     * @var string
     */
    private $sessionId;

    /**
     * @var DebugObjectPage
     */
    private $debugObjectPage;

    /**
     * @return string
     */
    public function getModuleVersion()
    {
        return $this->moduleVersion;
    }

    /**
     * @param string $moduleVersion
     */
    public function setModuleVersion($moduleVersion)
    {
        $this->moduleVersion = $moduleVersion;
    }

    /**
     * @return string
     */
    public function getServerNodeId()
    {
        return $this->serverNodeId;
    }

    /**
     * @param string $serverNodeId
     */
    public function setServerNodeId($serverNodeId)
    {
        $this->serverNodeId = $serverNodeId;
    }

    /**
     * @return string
     */
    public function getDebugText()
    {
        return $this->debugText;
    }

    /**
     * @param string $debugText
     */
    public function setDebugText($debugText)
    {
        $this->debugText = $debugText;
    }

    /**
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
    }

    /**
     * @return DebugObjectPage
     */
    public function getDebugObjectPage()
    {
        return $this->debugObjectPage;
    }

    /**
     * @param DebugObjectPage $debugObjectPage
     */
    public function setDebugObjectPage($debugObjectPage)
    {
        $this->debugObjectPage = $debugObjectPage;
    }
}