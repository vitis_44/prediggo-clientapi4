<?php

namespace Prediggo\ClientApi4\Types\Debug;

class DebugObjectBlock
{
    /**
     * @var string
     */
    private $blockCode;

    /**
     * @var string
     */
    private $blockLabel;

    /**
     * @var string
     */
    private $scenarioCode;

    /**
     * @var string
     */
    private $scenarioLabel;

    /**
     * @var integer
     */
    private $maxNbResultsExpected;

    /**
     * @var DebugObjectSlot[]
     */
    private $debugObjectSlots = array();

    /**
     * @return string
     */
    public function getBlockCode()
    {
        return $this->blockCode;
    }

    /**
     * @param string $blockCode
     */
    public function setBlockCode($blockCode)
    {
        $this->blockCode = $blockCode;
    }

    /**
     * @return string
     */
    public function getBlockLabel()
    {
        return $this->blockLabel;
    }

    /**
     * @param string $blockLabel
     */
    public function setBlockLabel($blockLabel)
    {
        $this->blockLabel = $blockLabel;
    }

    /**
     * @return string
     */
    public function getScenarioCode()
    {
        return $this->scenarioCode;
    }

    /**
     * @param string $scenarioCode
     */
    public function setScenarioCode($scenarioCode)
    {
        $this->scenarioCode = $scenarioCode;
    }

    /**
     * @return string
     */
    public function getScenarioLabel()
    {
        return $this->scenarioLabel;
    }

    /**
     * @param string $scenarioLabel
     */
    public function setScenarioLabel($scenarioLabel)
    {
        $this->scenarioLabel = $scenarioLabel;
    }

    /**
     * @return int
     */
    public function getMaxNbResultsExpected()
    {
        return $this->maxNbResultsExpected;
    }

    /**
     * @param int $maxNbResultsExpected
     */
    public function setMaxNbResultsExpected($maxNbResultsExpected)
    {
        $this->maxNbResultsExpected = $maxNbResultsExpected;
    }

    /**
     * @return DebugObjectSlot[]
     */
    public function getDebugObjectSlots()
    {
        return $this->debugObjectSlots;
    }

    /**
     * @param DebugObjectSlot[] $debugObjectSlots
     */
    public function setDebugObjectSlots($debugObjectSlots)
    {
        $this->debugObjectSlots = $debugObjectSlots;
    }
}