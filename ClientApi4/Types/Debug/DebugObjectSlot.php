<?php

namespace Prediggo\ClientApi4\Types\Debug;

class DebugObjectSlot
{
    /**
     * @var int
     */
    private $slotPosition;

    /**
     * @var double
     */
    private $computationTimeInMs;

    /**
     * @var string
     */
    private $resultItemId;

    /**
     * @var string
     */
    private $computationMethod;

    /**
     * @var string
     */
    private $computationStatus;

    /**
     * @var integer
     */
    private $ruleType;

    /**
     * @var DebugObjectSlotRecommendationRule[]
     */
    private $debugObjectSlotRecommendationRules = array();

    /**
     * @return int
     */
    public function getSlotPosition()
    {
        return $this->slotPosition;
    }

    /**
     * @param int $slotPosition
     */
    public function setSlotPosition($slotPosition)
    {
        $this->slotPosition = $slotPosition;
    }

    /**
     * @return float
     */
    public function getComputationTimeInMs()
    {
        return $this->computationTimeInMs;
    }

    /**
     * @param float $computationTimeInMs
     */
    public function setComputationTimeInMs($computationTimeInMs)
    {
        $this->computationTimeInMs = $computationTimeInMs;
    }

    /**
     * @return string
     */
    public function getResultItemId()
    {
        return $this->resultItemId;
    }

    /**
     * @param string $resultItemId
     */
    public function setResultItemId($resultItemId)
    {
        $this->resultItemId = $resultItemId;
    }

    /**
     * @return string
     */
    public function getComputationMethod()
    {
        return $this->computationMethod;
    }

    /**
     * @param string $computationMethod
     */
    public function setComputationMethod($computationMethod)
    {
        $this->computationMethod = $computationMethod;
    }

    /**
     * @return string
     */
    public function getComputationStatus()
    {
        return $this->computationStatus;
    }

    /**
     * @param string $computationStatus
     */
    public function setComputationStatus($computationStatus)
    {
        $this->computationStatus = $computationStatus;
    }

    /**
     * @return int
     */
    public function getRuleType()
    {
        return $this->ruleType;
    }

    /**
     * @param int $ruleType
     */
    public function setRuleType($ruleType)
    {
        $this->ruleType = $ruleType;
    }

    /**
     * @return DebugObjectSlotRecommendationRule[]
     */
    public function getDebugObjectSlotRecommendationRules()
    {
        return $this->debugObjectSlotRecommendationRules;
    }

    /**
     * @param DebugObjectSlotRecommendationRule[] $debugObjectSlotRecommendationRules
     */
    public function setDebugObjectSlotRecommendationRules($debugObjectSlotRecommendationRules)
    {
        $this->debugObjectSlotRecommendationRules = $debugObjectSlotRecommendationRules;
    }
}