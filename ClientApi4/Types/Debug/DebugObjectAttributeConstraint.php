<?php

namespace Prediggo\ClientApi4\Types\Debug;

class DebugObjectAttributeConstraint
{
    /**
     * @var string
     */
    private $attributeName;

    /**
     * @var int
     */
    private $nbCandidatesBefore;

    /**
     * @var int
     */
    private $nbCandidatesByThisAttributeConstraint;

    /**
     * @var int
     */
    private $nbCandidatesAfter;

    /**
     * @var string
     */
    private $status;

    /**
     * @return string
     */
    public function getAttributeName()
    {
        return $this->attributeName;
    }

    /**
     * @param string $attributeName
     */
    public function setAttributeName($attributeName)
    {
        $this->attributeName = $attributeName;
    }

    /**
     * @return int
     */
    public function getNbCandidatesBefore()
    {
        return $this->nbCandidatesBefore;
    }

    /**
     * @param int $nbCandidatesBefore
     */
    public function setNbCandidatesBefore($nbCandidatesBefore)
    {
        $this->nbCandidatesBefore = $nbCandidatesBefore;
    }

    /**
     * @return int
     */
    public function getNbCandidatesByThisAttributeConstraint()
    {
        return $this->nbCandidatesByThisAttributeConstraint;
    }

    /**
     * @param int $nbCandidatesByThisAttributeConstraint
     */
    public function setNbCandidatesByThisAttributeConstraint($nbCandidatesByThisAttributeConstraint)
    {
        $this->nbCandidatesByThisAttributeConstraint = $nbCandidatesByThisAttributeConstraint;
    }

    /**
     * @return int
     */
    public function getNbCandidatesAfter()
    {
        return $this->nbCandidatesAfter;
    }

    /**
     * @param int $nbCandidatesAfter
     */
    public function setNbCandidatesAfter($nbCandidatesAfter)
    {
        $this->nbCandidatesAfter = $nbCandidatesAfter;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
}