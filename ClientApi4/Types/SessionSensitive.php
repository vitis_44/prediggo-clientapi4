<?php

namespace Prediggo\ClientApi4\Types;

interface SessionSensitive {

    function getSessionId();

    function setSessionId($sessionId);
}