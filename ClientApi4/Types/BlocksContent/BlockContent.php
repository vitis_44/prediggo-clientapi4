<?php

namespace Prediggo\ClientApi4\Types\BlocksContent;

use Prediggo\ClientApi4\Types\PageContent\AdBlockResult;
use Prediggo\ClientApi4\Types\PageContent\RecoBlockResult;
use Prediggo\ClientApi4\Types\PageContent\SearchBlockResult;

class BlockContent
{

    /**
     * @var BlockParameters
     */
    private $parameters;

    /**
     * @var AdBlockResult | RecoBlockResult | SearchBlockResult
     */
    private $block;

    /**
     * @return BlockParameters
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param BlockParameters $parameters
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * @return AdBlockResult|RecoBlockResult|SearchBlockResult
     */
    public function getBlock()
    {
        return $this->block;
    }

    /**
     * @param AdBlockResult|RecoBlockResult|SearchBlockResult $block
     */
    public function setBlock($block)
    {
        $this->block = $block;
    }
}
