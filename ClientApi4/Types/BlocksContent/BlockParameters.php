<?php

namespace Prediggo\ClientApi4\Types\BlocksContent;

use Prediggo\ClientApi4\Types\Filter\Filterable;
use Prediggo\ClientApi4\Types\Filter\FilterSupplier;

/**
 * The parameters necessary to generate the content of the block.
 * Different blocks require different parameters.
 */
class BlockParameters implements Filterable
{

    /**
     * A different key for each block. It should be unique across all the blocks in the request.
     * It will be used as key for the blocks' map in the response.
     *
     * @var string
     */
    private $key;

    /**
     * The unique identifier for the requested block.
     *
     * @var string
     */
    private $blockIdentifier;

    /**
     * An optional identifier of a campaign configured for this block.
     * If provided, the merchandising will be done using this campaign's configuration.
     *
     * @var string
     */
    private $campaignIdentifier;

    /**
     * The search query to use to generate the suggestions.
     *
     * @var string
     */
    private $query;

    /**
     * A map of filters to apply to the suggestions.
     * The key is the name of the attribute and the values are the attribute values.
     *
     * @var array [string]
     */
    private $filters = array();

    /**
     * A list of skus.
     * The content of the list depends on the block.
     * If you are requesting an item block or an add to basket pop-in block, then this field should contain just the current item id.
     * However, if you are requesting a basket block, then the skus field should contain the item ids of all the items currently in the basket.
     * More generally, the skus field should contain the item ids of the items that are relevant for the generation of the content of the block.
     * This field is only used for recommendation blocks.
     *
     * @var array [string]
     */
    private $skus = array();

    /**
     * Contains the refiningId code indicating the action of the user on the search/navigation results (such as selecting a facet, or changing the sorting).
     * This refiningId should be sent along the original query to generate a variation of the results.
     * Refining ids are generated by Prediggo and are provided in the response.
     * For example, they can be found in the pagination, the facets, and the sorting.
     *
     * @var string
     */
    private $refiningId;

    /**
     * The number of the page to retrieve.
     * This field is only used for search and navigation blocks.
     *
     * @var int
     */
    private $page = null;

    /**
     * The number of desired results per page.
     * This field is only used for search and navigation blocks.
     *
     * @var int
     */
    private $resultsPerPage = null;

    /**
     * The sorting order requested by the end-user.
     * This field is only used for search and navigation blocks.
     *
     * @var string
     */
    private $sortingCode = null;

    /**
     * The mode of the block.
     * This field is only used for search and navigation blocks.
     *
     * @var string
     */
    private $mode = 'ALL';

    /**
     * An optional parameter used to give more or less importance to the semantic search.
     * The value should be between 0 and 1.
     * The closer the value is to 0, the less important the semantic search is.
     * The closer the value is to 1, the more important the semantic search is.
     * Not setting this value results in using the system default value.
     *
     * @var float
     */
    private $semanticSearchWeight = null;

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getBlockIdentifier()
    {
        return $this->blockIdentifier;
    }

    /**
     * @param string $blockIdentifier
     */
    public function setBlockIdentifier($blockIdentifier)
    {
        $this->blockIdentifier = $blockIdentifier;
    }

    /**
     * @return string
     */
    public function getCampaignIdentifier()
    {
        return $this->campaignIdentifier;
    }

    /**
     * @param string $campaignIdentifier
     */
    public function setCampaignIdentifier($campaignIdentifier)
    {
        $this->campaignIdentifier = $campaignIdentifier;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function setPage($page)
    {
        $this->page = $page;
    }

    public function getResultsPerPage()
    {
        return $this->resultsPerPage;
    }

    public function setResultsPerPage($resultsPerPage)
    {
        $this->resultsPerPage = $resultsPerPage;
    }

    public function getSortingCode()
    {
        return $this->sortingCode;
    }

    public function setSortingCode($sortingCode)
    {
        $this->sortingCode = $sortingCode;
    }

    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * @param $filterSupplier FilterSupplier
     */
    public function setFilters($filterSupplier)
    {
        $this->filters = $filterSupplier->filters();
    }

    /**
     * @param $filters
     */
    public function setFiltersArray($filters)
    {
        $this->filters = $filters;
    }

    /**
     * @return array[string]
     */
    public function getSkus()
    {
        return $this->skus;
    }

    /**
     * @param array[string] $skus
     */
    public function setSkus($skus)
    {
        $this->skus = $skus;
    }

    /**
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }

    public function setQuery($query)
    {
        $this->query = $query;
    }

    /**
     * @return string
     */
    public function getRefiningId()
    {
        return $this->refiningId;
    }

    public function setRefiningId($refiningId)
    {
        $this->refiningId = $refiningId;
    }

    /**
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param string $mode Can be one of 'ALL' or 'FACET_ONLY'
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    /**
     * @return float
     */
    public function getSemanticSearchWeight()
    {
        return $this->semanticSearchWeight;
    }

    public function setSemanticSearchWeight($semanticSearchWeight)
    {
        $this->semanticSearchWeight = $semanticSearchWeight;
    }
}