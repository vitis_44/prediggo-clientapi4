<?php

namespace Prediggo\ClientApi4\Types\BlocksContent;

use Prediggo\ClientApi4\ModuleInfo;
use Prediggo\ClientApi4\Types\ApiVersioned;
use Prediggo\ClientApi4\Types\PageContent\AdvancedParam;
use Prediggo\ClientApi4\Types\RegionContentQueryParameter;

/**
 * Parameter object for blocksContent queries
 */
class BlocksContentParam implements ApiVersioned, RegionContentQueryParameter
{

    /**
     * @var string
     */
    private $moduleVersion = ModuleInfo::MODULE_VERSION;

    /**
     * @var string
     */
    private $sessionId = "";

    /**
     * @var string region
     */
    private $region = "";

    /**
     * @var AdvancedParam
     */
    private $advanced;

    /**
     * @var array [BlockParameters]
     */
    private $blocks = array();

    public function __construct()
    {
        $this->advanced = new AdvancedParam();
    }

    public function addBlock(BlockParameters $blockParameters)
    {
        $this->blocks[] = $blockParameters;
    }

    public function getSessionId()
    {
        return $this->sessionId;
    }

    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return AdvancedParam
     */
    public function getAdvanced()
    {
        return $this->advanced;
    }

    /**
     * @return string the version of the module
     */
    function getModuleVersion()
    {
        return $this->moduleVersion;
    }

    /**
     * @return array
     */
    public function getBlocks()
    {
        return $this->blocks;
    }
}
