<?php

namespace Prediggo\ClientApi4\Types\AutoComplete;


use Prediggo\ClientApi4\Types\Debug\DebugResult;

/**
 * Result object for AutoComplete queries
 */
class AutoCompleteResult {

    private $status;
    private $sessionId;
    private $query;

    /**
     * @var string region
     */
    private $region;

    /**
     * @var string
     * @deprecated Languages and zones will be be removed.
     */
    private $languageCode;

    /**
     * @var string
     * @deprecated Languages and zones will be be removed.
     */
    private $zone;
    private $timeInMs;

    /**
     * @var AdvancedResult
     */
    private $advanced;

    private $keywords = array();
    private $attributes = array();
    private $items = array();

    /**
     * @var RedirectionComponent
     */
    private $redirection;

    /**
     * @var DebugResult
     */
    private $debug;

    public function __construct() {
        $this->advanced = new AdvancedResult();
    }

    /**
     * Returns an ItemCollection of the desired type, null if not found
     *
     * @param  string $type the desired type eg : 'products', 'cms'
     * @return ItemCollection
     */
    public function itemCollectionByType($type) {

        foreach ($this->items as $collection) {
            if ($collection->getItemType() == $type) {
                return $collection;
            }
        }

        return null;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getSessionId() {
        return $this->sessionId;
    }

    public function setSessionId($sessionId) {
        $this->sessionId = $sessionId;
    }

    public function getQuery() {
        return $this->query;
    }

    public function setQuery($query) {
        $this->query = $query;
    }

    /**
     * @return string
     * @deprecated Languages and zones will be be removed.
     */
    public function getLanguageCode() {
        return $this->languageCode;
    }

    /**
     * @param string $languageCode
     * @deprecated Languages and zones will be be removed.
     */
    public function setLanguageCode($languageCode) {
        $this->languageCode = $languageCode;
    }


    /**
     * @return string
     * @deprecated Languages and zones will be be removed.
     */
    public function getZone() {
        return $this->zone;
    }

    /**
     * @param string $zone
     * @deprecated Languages and zones will be be removed.
     */
    public function setZone($zone) {
        $this->zone = $zone;
    }

    /**
     * @return string
     */
    public function getRegion() {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion($region) {
        $this->region = $region;
    }

    public function getTimeInMs() {
        return $this->timeInMs;
    }

    public function setTimeInMs($timeInMs) {
        $this->timeInMs = $timeInMs;
    }

    /**
     * @return Keyword[]
     */
    public function getKeywords() {
        return $this->keywords;
    }

    /**
     * @param Keyword[] $keywords
     */
    public function setKeywords($keywords) {
        $this->keywords = $keywords;
    }

    /**
     * @return Attribute[]
     */
    public function getAttributes() {
        return $this->attributes;
    }

    /**
     * @param Attribute[] $attributes
     */
    public function setAttributes($attributes) {
        $this->attributes = $attributes;
    }

    /**
     * @return ItemCollection[]
     */
    public function getItems() {
        return $this->items;
    }

    /**
     * @param ItemCollection[] $items
     */
    public function setItems($items) {
        $this->items = $items;
    }

    /**
     * @return AdvancedResult
     */
    public function getAdvanced() {
        return $this->advanced;
    }

    /**
     * @param AdvancedResult $advanced
     */
    public function setAdvanced($advanced) {
        $this->advanced = $advanced;
    }

    /**
     * @return RedirectionComponent
     */
    public function getRedirection() {
        return $this->redirection;
    }

    /**
     * @param RedirectionComponent $redirection
     */
    public function setRedirection($redirection) {
        $this->redirection = $redirection;
    }

    /**
     * @return DebugResult
     */
    public function getDebug()
    {
        return $this->debug;
    }

    /**
     * @param DebugResult $debug
     */
    public function setDebug($debug)
    {
        $this->debug = $debug;
    }
}
