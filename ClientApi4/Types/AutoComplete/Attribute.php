<?php

namespace Prediggo\ClientApi4\Types\AutoComplete;

class Attribute
{
    /**
     * @var string
     */
    private $attributeName = "";

    /**
     * @var string
     */
    private $attributeLabel = "";

    /**
     * @var string
     */
    private $attributeType = "";

    /**
     * @var string
     */
    private $value = "";

    /**
     * @var string
     */
    private $label = "";

    /**
     * @var array
     */
    private $properties = array();

    /**
     * @var int
     */
    private $nbResults = 0;

    /**
     * @return string
     */
    public function getAttributeName()
    {
        return $this->attributeName;
    }

    /**
     * @param string $attributeName
     */
    public function setAttributeName($attributeName)
    {
        $this->attributeName = $attributeName;
    }

    /**
     * @return string
     */
    public function getAttributeLabel()
    {
        return $this->attributeLabel;
    }

    /**
     * @param string $attributeLabel
     */
    public function setAttributeLabel($attributeLabel)
    {
        $this->attributeLabel = $attributeLabel;
    }

    /**
     * @return string
     */
    public function getAttributeType()
    {
        return $this->attributeType;
    }

    /**
     * @param string $attributeType
     */
    public function setAttributeType($attributeType)
    {
        $this->attributeType = $attributeType;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * @param array $properties
     */
    public function setProperties($properties)
    {
        $this->properties = $properties;
    }

    /**
     * @return int
     */
    public function getNbResults()
    {
        return $this->nbResults;
    }

    /**
     * @param int $nbResults
     */
    public function setNbResults($nbResults)
    {
        $this->nbResults = $nbResults;
    }
}