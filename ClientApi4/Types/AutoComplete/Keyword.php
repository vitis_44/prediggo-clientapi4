<?php

namespace Prediggo\ClientApi4\Types\AutoComplete;

class Keyword {

    private $entry = "";
    private $nbResults = 0;

    public function getEntry() {
        return $this->entry;
    }

    public function setEntry($entry) {
        $this->entry = $entry;
    }

    public function getNbResults() {
        return $this->nbResults;
    }

    public function setNbResults($nbResults) {
        $this->nbResults = $nbResults;
    }

}