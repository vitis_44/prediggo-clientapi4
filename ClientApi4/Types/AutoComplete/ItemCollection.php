<?php

namespace Prediggo\ClientApi4\Types\AutoComplete;

use Prediggo\ClientApi4\Types\Item\ItemInfo;

class ItemCollection {

    private $itemType;
    private $matches = array();

    public function getItemType() {
        return $this->itemType;
    }

    public function setItemType($itemType) {
        $this->itemType = $itemType;
    }

    /**
     * @return ItemInfo[]
     */
    public function getMatches() {
        return $this->matches;
    }

    /**
     * @param ItemInfo[] $matches
     */
    public function setMatches($matches) {
        $this->matches = $matches;
    }

}