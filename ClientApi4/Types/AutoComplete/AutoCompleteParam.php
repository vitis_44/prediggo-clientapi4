<?php

namespace Prediggo\ClientApi4\Types\AutoComplete;

use Prediggo\ClientApi4\ModuleInfo;
use Prediggo\ClientApi4\Types\ApiVersioned;
use Prediggo\ClientApi4\Types\ContentQueryParameter;
use Prediggo\ClientApi4\Types\Filter\Filterable;


/**
 * Parameter object for AutoComplete queries
 */
class AutoCompleteParam implements ApiVersioned, ContentQueryParameter, Filterable {

    private $sessionId = "";
    private $query = "";

    /**
     * @var string
     * @deprecated Languages and zones will be be removed.
     */
    private $languageCode = "";

    private $advanced;
    private $filters = array();

    /**
     * @var string
     */
    private $moduleVersion = ModuleInfo::MODULE_VERSION;

    /**
     * @var string region
     */
    private $region = "";

    public function __construct() {
        $this->advanced = new AdvancedParam();
    }

    public function getSessionId() {
        return $this->sessionId;
    }

    public function setSessionId($sessionId) {
        $this->sessionId = $sessionId;
    }

    public function getQuery() {
        return $this->query;
    }

    public function setQuery($query) {
        $this->query = $query;
    }

    /**
     * @return string
     * @deprecated Languages and zones will be be removed.
     */
    public function getLanguageCode() {
        return $this->languageCode;
    }

    /**
     * @deprecated Languages and zones will be be removed.
     * @param string $languageCode
     */
    public function setLanguageCode($languageCode) {
        $this->languageCode = $languageCode;
    }

    /**
     * @return string
     */
    public function getRegion() {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion($region) {
        $this->region = $region;
    }

    public function getFilters() {
        return $this->filters;
    }

    public function setFilters($filterSupplier) {
        $this->filters = $filterSupplier->filters();
    }

    /**
     * @return AdvancedParam
     */
    public function getAdvanced() {
        return $this->advanced;
    }


    /**
     * @return string
     * @deprecated Languages and zones will be be removed.
     */
    function getZone() {
        return $this->getAdvanced()->getZone();
    }

    /**
     * @param string $zone
     * @deprecated Languages and zones will be be removed.
     */
    function setZone($zone) {
        $this->getAdvanced()->setZone($zone);
    }

    /**
     * @return string the version of the module
     */
    function getModuleVersion() {
        return $this->moduleVersion;
    }

}
