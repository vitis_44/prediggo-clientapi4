<?php

namespace Prediggo\ClientApi4\Types\AutoComplete;


class RedirectionComponent {

    private $redirectUrl;

    /**
     * @return string
     */
    public function getRedirectUrl() {
        return $this->redirectUrl;
    }

    /**
     * @param string $redirectUrl
     */
    public function setRedirectUrl($redirectUrl) {
        $this->redirectUrl = $redirectUrl;
    }

}