<?php

namespace Prediggo\ClientApi4\Types\Item;

use Prediggo\ClientApi4\Types\PageContent\PictogramHtmlResult;
use Prediggo\ClientApi4\Types\PageContent\PictogramResult;
use Prediggo\ClientApi4\Types\PageContent\PictogramUrlResult;

/**
 * Generic item result class
 */
class ItemInfo {

    private $sku;
    private $price;
    private $trackingCode;
    private $itemType;
    private $groupingInfo;

    /**
     * @var AttributeGroup
     */
    private $attributeInfo = array();

    /**
     * @var PictogramResult
     */
    private $pictograms = array();

    /**
     * Returns the attribute group matching the given name, or null if not found
     *
     * @param  string $attributeName the attribute name, eg: category, color
     * @return AttributeGroup|null
     */
    public function findAttributeByName($attributeName) {
        foreach ($this->attributeInfo as $attribute) {
            if ($attribute->getAttributeName() == $attributeName) {
                return $attribute;
            }
        }

        return null;
    }

    /**
     * Returns the list of attribute values for the group matching the given name, or empty list if not found
     *
     * @param  string $attributeName the attribute name, eg: category, color
     * @return AttributeValue[]
     */
    public function findAttributeValuesOf($attributeName) {

        $group = $this->findAttributeByName($attributeName);

        if ($group != null) {
            return $group->getVals();
        }

        return array();
    }

    /**
     * Returns the first attribute value for the group matching the given name, or null if not found
     * Equivalent to FindAttributeValuesOf but easier for dealing with single value attributes
     *
     * @param  string $attributeName the attribute name, eg: category, color
     * @return AttributeValue|null
     */
    public function findSingleAttributeValueOf($attributeName) {

        $values = $this->findAttributeValuesOf($attributeName);

        if (count($values) > 0) {
            return $values[0];
        }
        return null;
    }

    public function getSku() {
        return $this->sku;
    }

    public function setSku($sku) {
        $this->sku = $sku;
    }

    public function getPrice() {
        return $this->price;
    }

    public function setPrice($price) {
        $this->price = $price;
    }

    public function getTrackingCode() {
        return $this->trackingCode;
    }

    public function setTrackingCode($trackingCode) {
        $this->trackingCode = $trackingCode;
    }

    public function getItemType() {
        return $this->itemType;
    }

    public function setItemType($itemType) {
        $this->itemType = $itemType;
    }

    /**
     * @return AttributeGroup[]
     */
    public function getAttributeInfo() {
        return $this->attributeInfo;
    }

    /**
     * @param AttributeGroup[] $attributeInfo
     */
    public function setAttributeInfo($attributeInfo) {
        $this->attributeInfo = $attributeInfo;
    }

    /**
     * @return GroupingInfo
     */
    public function getGroupingInfo() {
        return $this->groupingInfo;
    }

    /**
     * @param GroupingInfo $groupingInfo
     */
    public function setGroupingInfo($groupingInfo) {
        $this->groupingInfo = $groupingInfo;
    }

    /**
     * @return PictogramResult[]
     */
    public function getPictograms()
    {
        return $this->pictograms;
    }

    /**
     * @param PictogramResult[] $pictograms
     */
    public function setPictograms(array $pictograms)
    {
        $this->pictograms = $pictograms;
    }

    /**
     * @return array The list of pictograms of type URL. The pictograms matching other types are filtered out.
     * To get the unfiltered list, use {@link getPictograms()}.
     */
    public function getPictogramUrlResults(): array {
        return array_filter($this->pictograms, function($pictogram) {
            return $pictogram instanceof PictogramUrlResult;
        });
    }

    /**
     * @return array The list of pictograms of type HTML. The pictograms matching other types are filtered out.
     * To get the unfiltered list, use {@link getPictograms()}.
     */
    public function getPictogramHtmlResults(): array {
        return array_filter($this->pictograms, function($pictogram) {
            return $pictogram instanceof PictogramHtmlResult;
        });
    }
}
