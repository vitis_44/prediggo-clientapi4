<?php

namespace Prediggo\ClientApi4\Types\Item;

class AttributeValue {

    private $value;
    private $label;

    public function getValue() {
        return $this->value;
    }

    public function setValue($value) {
        $this->value = $value;
    }

    public function getLabel() {
        return $this->label;
    }

    public function setLabel($label) {
        $this->label = $label;
    }


}