<?php

namespace Prediggo\ClientApi4\Types\Item;

class GroupingInfo {

    private $referenceItemSku;
    private $childrenSkus = array();

    /**
     * @return string
     */
    public function getReferenceItemSku() {
        return $this->referenceItemSku;
    }

    /**
     * @param string $referenceItemSku
     */
    public function setReferenceItemSku($referenceItemSku) {
        $this->referenceItemSku = $referenceItemSku;
    }

    /**
     * @return string[]
     */
    public function getChildrenSkus() {
        return $this->childrenSkus;
    }

    /**
     * @param string[] $childrenSkus
     */
    public function setChildrenSkus($childrenSkus) {
        $this->childrenSkus = $childrenSkus;
    }

}