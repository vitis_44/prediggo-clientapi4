<?php

namespace Prediggo\ClientApi4\Types\Item;

/**
 * A list of possible sortings
 */
class AttributeTypes {

    const  ID = 'ID';
    const  VALUE = 'VALUE';
    const  RANGE = "RANGE";
}