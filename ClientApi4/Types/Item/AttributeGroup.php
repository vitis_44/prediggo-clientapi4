<?php

namespace Prediggo\ClientApi4\Types\Item;

class AttributeGroup {

    private $attributeName;
    private $attributeLabel;
    private $attributeType;
    private $attributeUnit;

    private $vals = array();

    public function getAttributeName() {
        return $this->attributeName;
    }

    public function setAttributeName($attributeName) {
        $this->attributeName = $attributeName;
    }

    public function getAttributeLabel() {
        return $this->attributeLabel;
    }

    public function setAttributeLabel($attributeLabel) {
        $this->attributeLabel = $attributeLabel;
    }

    public function getAttributeType() {
        return $this->attributeType;
    }

    public function setAttributeType($attributeType) {
        $this->attributeType = $attributeType;
    }

    public function getAttributeUnit() {
        return $this->attributeUnit;
    }

    public function setAttributeUnit($attributeUnit) {
        $this->attributeUnit = $attributeUnit;
    }

    /**
     * @return AttributeValue[]
     */
    public function getVals() {
        return $this->vals;
    }

    /**
     * @param AttributeValue[] $vals
     */
    public function setVals($vals) {
        $this->vals = $vals;
    }


}