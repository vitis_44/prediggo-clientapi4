<?php

namespace Prediggo\ClientApi4\Types\ViewUser;


class UserDataSet {

    /**
     * @var string
     */
    private $setName;

    /**
     * @var array[array]
     */
    private $entries = array();

    /**
     * @return string
     */
    public function getSetName() {
        return $this->setName;
    }

    /**
     * @param string $setName
     */
    public function setSetName($setName) {
        $this->setName = $setName;
    }

    /**
     * @return array
     */
    public function getEntries() {
        return $this->entries;
    }

    /**
     * @param array $entries
     */
    public function setEntries($entries) {
        $this->entries = $entries;
    }

}