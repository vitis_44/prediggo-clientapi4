<?php

namespace Prediggo\ClientApi4\Types\ViewUser;


use Prediggo\ClientApi4\Types\Debug\DebugResult;

class ViewUserResult {

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $sessionId;

    /**
     * @var string
     */
    private $customerId;

    /**
     * @var float
     */
    private $timeInMs;

    /**
     * @var UserDataSet[]
     */
    private $dataSets = array();

    /**
     * @var DebugResult
     */
    private $debug;

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getSessionId() {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     */
    public function setSessionId($sessionId) {
        $this->sessionId = $sessionId;
    }

    /**
     * @return string
     */
    public function getCustomerId() {
        return $this->customerId;
    }

    /**
     * @param string $customerId
     */
    public function setCustomerId($customerId) {
        $this->customerId = $customerId;
    }

    /**
     * @return float
     */
    public function getTimeInMs() {
        return $this->timeInMs;
    }

    /**
     * @param float $timeInMs
     */
    public function setTimeInMs($timeInMs) {
        $this->timeInMs = $timeInMs;
    }

    /**
     * @return UserDataSet[]
     */
    public function getDataSets() {
        return $this->dataSets;
    }

    /**
     * @param UserDataSet[] $dataSets
     */
    public function setDataSets($dataSets) {
        $this->dataSets = $dataSets;
    }

    /**
     * @return DebugResult
     */
    public function getDebug()
    {
        return $this->debug;
    }

    /**
     * @param DebugResult $debug
     */
    public function setDebug($debug)
    {
        $this->debug = $debug;
    }
}