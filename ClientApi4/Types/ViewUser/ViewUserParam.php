<?php

namespace Prediggo\ClientApi4\Types\ViewUser;


use Prediggo\ClientApi4\ModuleInfo;
use Prediggo\ClientApi4\Types\ApiVersioned;
use Prediggo\ClientApi4\Types\SessionSensitive;

class ViewUserParam implements ApiVersioned, SessionSensitive {

    /**
     * @var string
     */
    private $sessionId;

    /**
     * @var string
     */
    private $customerId;

    /**
     * @var string
     */
    private $moduleVersion = ModuleInfo::MODULE_VERSION;

    /**
     * @return string
     */
    public function getSessionId() {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     */
    public function setSessionId($sessionId) {
        $this->sessionId = $sessionId;
    }

    /**
     * @return string
     */
    public function getCustomerId() {
        return $this->customerId;
    }

    /**
     * @param string $customerId
     */
    public function setCustomerId($customerId) {
        $this->customerId = $customerId;
    }

    /**
     * @return string the version of the module
     */
    function getModuleVersion() {
        return $this->moduleVersion;
    }
}