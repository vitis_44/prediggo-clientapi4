<?php

namespace Prediggo\ClientApi4\Errors;

/**
 * Json conversion Exception
 */
class JsonConversionException extends PrediggoApi4Exception {

    protected $sourceObject;
    protected $destinationType;

    /**
     * @return mixed
     */
    public function getSourceObject() {
        return $this->sourceObject;
    }

    /**
     * @param mixed $sourceObject
     */
    public function setSourceObject($sourceObject) {
        $this->sourceObject = $sourceObject;
    }

    /**
     * @return mixed
     */
    public function getDestinationType() {
        return $this->destinationType;
    }

    /**
     * @param mixed $destinationType
     */
    public function setDestinationType($destinationType) {
        $this->destinationType = $destinationType;
    }


}
