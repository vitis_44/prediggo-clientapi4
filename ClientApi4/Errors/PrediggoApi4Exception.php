<?php

namespace Prediggo\ClientApi4\Errors;

use Exception;

/**
 * General Prediggo service exception
 */
class PrediggoApi4Exception extends Exception {


}
