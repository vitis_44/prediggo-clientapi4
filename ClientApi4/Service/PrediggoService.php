<?php

namespace Prediggo\ClientApi4\Service;

use Prediggo\ClientApi4\Service\Http\RequestExecutor;
use Prediggo\ClientApi4\Service\Json\JsonConverter;
use Prediggo\ClientApi4\Service\Listener\AfterQueryArgs;
use Prediggo\ClientApi4\Service\Listener\BeforeQueryArgs;
use Prediggo\ClientApi4\Service\Listener\QueryListener;
use Prediggo\ClientApi4\Types\AutoComplete\AutoCompleteParam;
use Prediggo\ClientApi4\Types\AutoComplete\AutoCompleteResult;
use Prediggo\ClientApi4\Types\BlocksContent\BlocksContentParam;
use Prediggo\ClientApi4\Types\BlocksContent\BlocksContentResult;
use Prediggo\ClientApi4\Types\CachingStats\CachingStatsParam;
use Prediggo\ClientApi4\Types\CachingStats\CachingStatsResult;
use Prediggo\ClientApi4\Types\CategoryCount\CategoryCountParam;
use Prediggo\ClientApi4\Types\CategoryCount\CategoryCountResult;
use Prediggo\ClientApi4\Types\DeleteUser\DeleteUserParam;
use Prediggo\ClientApi4\Types\DeleteUser\DeleteUserResult;
use Prediggo\ClientApi4\Types\ItemsInfo\ItemsInfoParam;
use Prediggo\ClientApi4\Types\ItemsInfo\ItemsInfoResult;
use Prediggo\ClientApi4\Types\ItemsInteraction\ItemsInteractionParam;
use Prediggo\ClientApi4\Types\ItemsInteraction\ItemsInteractionResult;
use Prediggo\ClientApi4\Types\ItemsSet\ItemsSetParam;
use Prediggo\ClientApi4\Types\ItemsSet\ItemsSetResult;
use Prediggo\ClientApi4\Types\PageContent\PageContentParam;
use Prediggo\ClientApi4\Types\PageContent\PageContentResult;
use Prediggo\ClientApi4\Types\PageContent\SimplePageContentParam;
use Prediggo\ClientApi4\Types\ProductAvailability\ProductAvailabilityParam;
use Prediggo\ClientApi4\Types\ProductAvailability\ProductAvailabilityResult;
use Prediggo\ClientApi4\Types\ProductsCompare\ProductsCompareParam;
use Prediggo\ClientApi4\Types\ProductsCompare\ProductsCompareResult;
use Prediggo\ClientApi4\Types\RegisterUser\RegisterUserParam;
use Prediggo\ClientApi4\Types\RegisterUser\RegisterUserResult;
use Prediggo\ClientApi4\Types\SessionSensitive;
use Prediggo\ClientApi4\Types\SubCategoryCount\SubCategoryCountParam;
use Prediggo\ClientApi4\Types\SubCategoryCount\SubCategoryCountResult;
use Prediggo\ClientApi4\Types\Track\TrackParam;
use Prediggo\ClientApi4\Types\Track\TrackResult;
use Prediggo\ClientApi4\Types\UpdateItems\UpdateItemsParam;
use Prediggo\ClientApi4\Types\UpdateItems\UpdateItemsResult;
use Prediggo\ClientApi4\Types\UpdateUsers\UpdateUserParam;
use Prediggo\ClientApi4\Types\UpdateUsers\UpdateUserResult;
use Prediggo\ClientApi4\Types\ViewUser\ViewUserParam;
use Prediggo\ClientApi4\Types\ViewUser\ViewUserResult;

class PrediggoService implements PrediggoServiceApi
{
    /**
     * @var RequestExecutor
     */
    private $executor;
    /**
     * @var JsonConverter
     */
    private $jsonConverter;
    /**
     * @var string
     */
    private $serviceUrl;
    /**
     * @var string
     */
    private $apiKey;
    /**
     * The parameter used for routing the traffic.
     * This is null by default and only used
     * with customers with distributed installation.
     * @var string
     */
    private $routingId = null;
    /**
     * Whether the API key should be sent in the URL as well as in the headers.
     * For security reasons, it is not recommended to set this to true.
     * @var bool
     */
    private $sendApiKeyInUrl = false;
    /**
     * @var string
     */
    private $apiVersion = "3.0";

    /**
     * PrediggoService constructor.
     *
     * @param RequestExecutor $executor
     * @param JsonConverter $jsonConverter
     * @param $serviceUrl
     * @param $apiKey
     */
    public function __construct($serviceUrl, $apiKey, RequestExecutor $executor, JsonConverter $jsonConverter, $routingId = null)
    {
        $this->executor = $executor;
        $this->jsonConverter = $jsonConverter;
        $this->serviceUrl = rtrim($serviceUrl, '/');
        $this->apiKey = $apiKey;
        $this->routingId = $routingId;
    }

    /**
     * @param AutoCompleteParam $param
     * @param QueryListener|null $listener
     * @return AutoCompleteResult
     */
    public function autoComplete(AutoCompleteParam $param, QueryListener $listener = null)
    {
        return $this->serviceCall($this->apiVersion . '/autoComplete', $param, 'Prediggo\ClientApi4\Types\AutoComplete\AutoCompleteResult', $listener);
    }

    /**
     * @param PageContentParam $param
     * @param QueryListener|null $listener
     * @return PageContentResult
     */
    public function pageContent(PageContentParam $param, QueryListener $listener = null)
    {
        return $this->serviceCall($this->apiVersion . '/pageContent', $param, 'Prediggo\ClientApi4\Types\PageContent\PageContentResult', $listener);
    }

    /**
     * @param RegisterUserParam $param
     * @param QueryListener|null $listener
     * @return RegisterUserResult
     */
    public function registerUser(RegisterUserParam $param, QueryListener $listener = null)
    {
        return $this->serviceCall($this->apiVersion . '/registerUser', $param, 'Prediggo\ClientApi4\Types\RegisterUser\RegisterUserResult', $listener);
    }

    /**
     * @param TrackParam $param
     * @param QueryListener|null $listener
     * @return TrackResult
     */
    public function track(TrackParam $param, QueryListener $listener = null)
    {
        return $this->serviceCall($this->apiVersion . '/track', $param, 'Prediggo\ClientApi4\Types\Track\TrackResult', $listener);
    }

    /**
     * @param UpdateItemsParam $param
     * @param QueryListener|null $listener
     * @return UpdateItemsResult
     *
     */
    public function updateItems(UpdateItemsParam $param, QueryListener $listener = null)
    {
        return $this->serviceCall($this->apiVersion . '/updateItems', $param, 'Prediggo\ClientApi4\Types\UpdateItems\UpdateItemsResult', $listener);
    }

    /**
     * @param ItemsInteractionParam $param
     * @param QueryListener|null $listener
     * @return ItemsInteractionResult
     */
    public function itemsInteraction(ItemsInteractionParam $param, QueryListener $listener = null)
    {
        return $this->serviceCall($this->apiVersion . '/itemsInteraction', $param, 'Prediggo\ClientApi4\Types\ItemsInteraction\ItemsInteractionResult', $listener);
    }

    /**
     * @param CachingStatsParam $param
     * @param QueryListener|null $listener
     * @return CachingStatsResult
     */
    public function cachingStats(CachingStatsParam $param, QueryListener $listener = null)
    {
        return $this->serviceCall($this->apiVersion . '/cachingStats', $param, 'Prediggo\ClientApi4\Types\CachingStats\CachingStatsResult', $listener);
    }

    /**
     * @param SimplePageContentParam $param
     * @param QueryListener|null $listener
     * @return PageContentResult
     */
    public function simplePageContent(SimplePageContentParam $param, QueryListener $listener = null)
    {
        return $this->serviceCall($this->apiVersion . '/simplePageContent', $param, 'Prediggo\ClientApi4\Types\PageContent\PageContentResult', $listener);
    }

    /**
     * @param ViewUserParam $param
     * @param QueryListener|null $listener
     * @return ViewUserResult
     */
    public function viewUser(ViewUserParam $param, QueryListener $listener = null)
    {
        return $this->serviceCall($this->apiVersion . '/viewUser', $param, 'Prediggo\ClientApi4\Types\ViewUser\ViewUserResult', $listener);
    }

    /**
     * @param DeleteUserParam $param
     * @param QueryListener|null $listener
     * @return DeleteUserResult
     */
    public function deleteUser(DeleteUserParam $param, QueryListener $listener = null)
    {
        return $this->serviceCall($this->apiVersion . '/deleteUser', $param, 'Prediggo\ClientApi4\Types\DeleteUser\DeleteUserResult', $listener);
    }

    /**
     * @param ItemsSetParam $param
     * @param QueryListener|null $listener
     * @return ItemsSetResult
     */
    public function itemsSet(ItemsSetParam $param, QueryListener $listener = null)
    {
        return $this->serviceCall($this->apiVersion . '/itemsSet', $param, 'Prediggo\ClientApi4\Types\ItemsSet\ItemsSetResult', $listener);
    }

    /**
     * @param ItemsInfoParam $param
     * @param QueryListener|null $listener
     * @return ItemsInfoResult
     */
    public function itemsInfo(ItemsInfoParam $param, QueryListener $listener = null)
    {
        return $this->serviceCall($this->apiVersion . '/itemsInfo', $param, 'Prediggo\ClientApi4\Types\ItemsInfo\ItemsInfoResult', $listener);
    }

    /**
     * @param CategoryCountParam $param
     * @param QueryListener|null $listener
     * @return CategoryCountResult
     */
    public function categoryCount(CategoryCountParam $param, QueryListener $listener = null)
    {
        return $this->serviceCall($this->apiVersion . '/categoryCount', $param, 'Prediggo\ClientApi4\Types\CategoryCount\CategoryCountResult', $listener);
    }

    /**
     * @param ProductAvailabilityParam $param
     * @param QueryListener|null $listener
     * @return ProductAvailabilityResult
     */
    public function productAvailability(ProductAvailabilityParam $param, QueryListener $listener = null)
    {
        return $this->serviceCall($this->apiVersion . '/productAvailability', $param, 'Prediggo\ClientApi4\Types\ProductAvailability\ProductAvailabilityResult', $listener);
    }

    /**
     * @param SubCategoryCountParam $param
     * @param QueryListener|null $listener
     * @return SubCategoryCountResult
     */
    public function subCategoryCount(SubCategoryCountParam $param, QueryListener $listener = null)
    {
        return $this->serviceCall($this->apiVersion . '/subCategoryCount', $param, 'Prediggo\ClientApi4\Types\SubCategoryCount\SubCategoryCountResult', $listener);
    }

    /**
     * @param UpdateUserParam $param
     * @param QueryListener|null $listener
     * @return UpdateUserResult
     */
    public function updateUsers(UpdateUserParam $param, QueryListener $listener = null)
    {
        return $this->serviceCall($this->apiVersion . '/updateUsers', $param, 'Prediggo\ClientApi4\Types\UpdateUsers\UpdateUserResult', $listener);
    }

    /**
     * @param ProductsCompareParam $param
     * @param QueryListener|null $listener
     * @return ProductsCompareResult
     */
    public function productsCompare(ProductsCompareParam $param, QueryListener $listener = null)
    {
        return $this->serviceCall($this->apiVersion . '/productsCompare', $param, 'Prediggo\ClientApi4\Types\ProductsCompare\ProductsCompareResult', $listener);
    }

    /**
     * @param BlocksContentParam $param
     * @param QueryListener|null $listener
     * @return BlocksContentResult
     */
    public function blocksContent(BlocksContentParam $param, QueryListener $listener = null)
    {
        return $this->serviceCall($this->apiVersion . '/blocksContent', $param, 'Prediggo\ClientApi4\Types\BlocksContent\BlocksContentResult', $listener);
    }

    protected function serviceCall($queryPath, $queryParameter, $returnType, QueryListener $listener = null)
    {

        $finalUrl = $this->buildQueryUrl($queryPath);
        $jsonBody = $this->jsonConverter->serialize($queryParameter);
        $headers = $this->additionalHeaders($finalUrl, $queryParameter);

        if ($listener != null) {

            $queryArgs = new BeforeQueryArgs();
            $queryArgs->setUrl($finalUrl);
            $queryArgs->setHeaders($headers);
            $queryArgs->setParameterObject($queryParameter);
            $queryArgs->setJsonParameter($jsonBody);

            $listener->beforeQuery($queryArgs);

            //use modified values
            $finalUrl = $queryArgs->getUrl();
            $jsonBody = $queryArgs->getJsonParameter();
            $headers = $queryArgs->getHeaders();
        }

        $jsonResponse = $this->executor->postQuery($finalUrl, $jsonBody, $headers);
        $responseObject = $this->jsonConverter->deSerialize($jsonResponse, $returnType);

        if ($listener != null) {

            $afterQueryArgs = new AfterQueryArgs();
            $afterQueryArgs->setUrl($finalUrl);
            $afterQueryArgs->setJsonParameter($jsonBody);
            $afterQueryArgs->setJsonResponse($jsonResponse);
            $afterQueryArgs->setResponseObject($responseObject);

            $listener->afterQuery($afterQueryArgs);
        }

        return $responseObject;
    }

    private function buildQueryUrl($queryPath)
    {
        if ($this->sendApiKeyInUrl) {
            return sprintf('%s/%s/%s', $this->serviceUrl, urlencode($this->apiKey), $queryPath);
        } else {
            return sprintf('%s/api/%s', $this->serviceUrl, $queryPath);
        }
    }

    protected function additionalHeaders($finalUrl, $queryParameter)
    {

        $headerMap = array();

        $headerMap["X-Prediggo-APIKey"] = $this->apiKey;

        if ($queryParameter instanceof SessionSensitive) {
            $sessionId = $queryParameter->getSessionId();
            $headerMap["X-Prediggo-Session"] = $sessionId;
        }

        if (!is_null($this->routingId) && !empty($this->routingId)) {
            $headerMap["X-Prediggo-Routing"] = $this->routingId;
        }

        return $headerMap;
    }

    /**
     * This method allows to force a given API version and should only be used in very specific use cases, such as
     * compatibility testing.
     *
     * @param string $version the API version (eg: '1.0', '3.0')
     */
    public function setApiVersion($version)
    {
        $this->apiVersion = $version;
    }

    public function setSendApiKeyInUrl($apiKeyInUrl)
    {
        $this->sendApiKeyInUrl = $apiKeyInUrl;
    }
}
