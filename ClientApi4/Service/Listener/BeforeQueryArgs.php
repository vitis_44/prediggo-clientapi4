<?php

namespace Prediggo\ClientApi4\Service\Listener;

class BeforeQueryArgs {

    private $url;
    private $parameterObject;
    private $jsonParameter;
    private $headers = array();

    public function getUrl() {
        return $this->url;
    }

    public function setUrl($url) {
        $this->url = $url;
    }

    public function getParameterObject() {
        return $this->parameterObject;
    }

    public function setParameterObject($parameterObject) {
        $this->parameterObject = $parameterObject;
    }

    public function getJsonParameter() {
        return $this->jsonParameter;
    }

    public function setJsonParameter($jsonParameter) {
        $this->jsonParameter = $jsonParameter;
    }

    public function getHeaders() {
        return $this->headers;
    }

    public function setHeaders($headers) {
        $this->headers = $headers;
    }


}