<?php

namespace Prediggo\ClientApi4\Service\Listener;

class AfterQueryArgs {

    private $url;
    private $jsonParameter;
    private $jsonResponse;
    private $responseObject;

    public function getUrl() {
        return $this->url;
    }

    public function setUrl($url) {
        $this->url = $url;
    }

    public function getJsonParameter() {
        return $this->jsonParameter;
    }

    public function setJsonParameter($jsonParameter) {
        $this->jsonParameter = $jsonParameter;
    }

    public function getJsonResponse() {
        return $this->jsonResponse;
    }

    public function setJsonResponse($jsonResponse) {
        $this->jsonResponse = $jsonResponse;
    }

    public function getResponseObject() {
        return $this->responseObject;
    }

    public function setResponseObject($responseObject) {
        $this->responseObject = $responseObject;
    }

}