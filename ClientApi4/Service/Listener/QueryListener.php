<?php

namespace Prediggo\ClientApi4\Service\Listener;

interface QueryListener {

    /**
     * @param BeforeQueryArgs $beforeQueryInfo
     */
    function beforeQuery($beforeQueryInfo);

    /**
     * @param AfterQueryArgs $afterQueryInfo
     */
    function afterQuery($afterQueryInfo);

}