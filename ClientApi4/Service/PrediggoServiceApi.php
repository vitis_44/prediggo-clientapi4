<?php

namespace Prediggo\ClientApi4\Service;

use Prediggo\ClientApi4\Service\Listener\QueryListener;
use Prediggo\ClientApi4\Types\AutoComplete\AutoCompleteParam;
use Prediggo\ClientApi4\Types\AutoComplete\AutoCompleteResult;
use Prediggo\ClientApi4\Types\BlocksContent\BlocksContentParam;
use Prediggo\ClientApi4\Types\BlocksContent\BlocksContentResult;
use Prediggo\ClientApi4\Types\CachingStats\CachingStatsParam;
use Prediggo\ClientApi4\Types\CachingStats\CachingStatsResult;
use Prediggo\ClientApi4\Types\CategoryCount\CategoryCountParam;
use Prediggo\ClientApi4\Types\CategoryCount\CategoryCountResult;
use Prediggo\ClientApi4\Types\DeleteUser\DeleteUserParam;
use Prediggo\ClientApi4\Types\DeleteUser\DeleteUserResult;
use Prediggo\ClientApi4\Types\ItemsInfo\ItemsInfoParam;
use Prediggo\ClientApi4\Types\ItemsInfo\ItemsInfoResult;
use Prediggo\ClientApi4\Types\ItemsInteraction\ItemsInteractionParam;
use Prediggo\ClientApi4\Types\ItemsInteraction\ItemsInteractionResult;
use Prediggo\ClientApi4\Types\ItemsSet\ItemsSetParam;
use Prediggo\ClientApi4\Types\ItemsSet\ItemsSetResult;
use Prediggo\ClientApi4\Types\PageContent\PageContentParam;
use Prediggo\ClientApi4\Types\PageContent\PageContentResult;
use Prediggo\ClientApi4\Types\PageContent\SimplePageContentParam;
use Prediggo\ClientApi4\Types\ProductAvailability\ProductAvailabilityParam;
use Prediggo\ClientApi4\Types\ProductAvailability\ProductAvailabilityResult;
use Prediggo\ClientApi4\Types\ProductsCompare\ProductsCompareParam;
use Prediggo\ClientApi4\Types\ProductsCompare\ProductsCompareResult;
use Prediggo\ClientApi4\Types\RegisterUser\RegisterUserParam;
use Prediggo\ClientApi4\Types\RegisterUser\RegisterUserResult;
use Prediggo\ClientApi4\Types\SubCategoryCount\SubCategoryCountParam;
use Prediggo\ClientApi4\Types\SubCategoryCount\SubCategoryCountResult;
use Prediggo\ClientApi4\Types\Track\TrackParam;
use Prediggo\ClientApi4\Types\Track\TrackResult;
use Prediggo\ClientApi4\Types\UpdateItems\UpdateItemsParam;
use Prediggo\ClientApi4\Types\UpdateItems\UpdateItemsResult;
use Prediggo\ClientApi4\Types\UpdateUsers\UpdateUserParam;
use Prediggo\ClientApi4\Types\UpdateUsers\UpdateUserResult;
use Prediggo\ClientApi4\Types\ViewUser\ViewUserParam;
use Prediggo\ClientApi4\Types\ViewUser\ViewUserResult;


/**
 * Service API
 */
interface PrediggoServiceApi
{

    /**
     * @param AutoCompleteParam $param
     * @param QueryListener|null $queryListener
     * @return AutoCompleteResult
     */
    public function autoComplete(AutoCompleteParam $param, QueryListener $queryListener = null);

    /**
     * @param PageContentParam $param
     * @param QueryListener|null $queryListener
     * @return PageContentResult
     */
    public function pageContent(PageContentParam $param, QueryListener $queryListener = null);

    /**
     * @param RegisterUserParam $param
     * @param QueryListener|null $queryListener
     * @return RegisterUserResult
     */
    public function registerUser(RegisterUserParam $param, QueryListener $queryListener = null);

    /**
     * @param TrackParam $param
     * @param QueryListener|null $queryListener
     * @return TrackResult
     */
    public function track(TrackParam $param, QueryListener $queryListener = null);

    /**
     * @param UpdateItemsParam $param
     * @param QueryListener|null $listener
     * @return UpdateItemsResult
     *
     */
    public function updateItems(UpdateItemsParam $param, QueryListener $listener = null);

    /**
     * @param ItemsInteractionParam $param
     * @param QueryListener|null $queryListener
     * @return ItemsInteractionResult
     */
    public function itemsInteraction(ItemsInteractionParam $param, QueryListener $queryListener = null);

    /**
     * @param CachingStatsParam $param
     * @param QueryListener|null $listener
     * @return CachingStatsResult
     */
    public function cachingStats(CachingStatsParam $param, QueryListener $listener = null);

    /**
     * @param SimplePageContentParam $param
     * @param QueryListener|null $listener
     * @return PageContentResult
     */
    public function simplePageContent(SimplePageContentParam $param, QueryListener $listener = null);

    /**
     * @param ViewUserParam $param
     * @param QueryListener|null $queryListener
     * @return ViewUserResult
     */
    public function viewUser(ViewUserParam $param, QueryListener $queryListener = null);

    /**
     * @param DeleteUserParam $param
     * @param QueryListener|null $listener
     * @return DeleteUserResult
     */
    public function deleteUser(DeleteUserParam $param, QueryListener $listener = null);

    /**
     * @param ItemsSetParam $param
     * @param QueryListener|null $listener
     * @return ItemsSetResult
     */
    public function itemsSet(ItemsSetParam $param, QueryListener $listener = null);

    /**
     * @param ItemsInfoParam $param
     * @param QueryListener|null $listener
     * @return ItemsInfoResult
     */
    public function itemsInfo(ItemsInfoParam $param, QueryListener $listener = null);

    /**
     * @param CategoryCountParam $param
     * @param QueryListener|null $listener
     * @return CategoryCountResult
     */
    public function categoryCount(CategoryCountParam $param, QueryListener $listener = null);

    /**
     * @param ProductAvailabilityParam $param
     * @param QueryListener|null $listener
     * @return ProductAvailabilityResult
     */
    public function productAvailability(ProductAvailabilityParam $param, QueryListener $listener = null);

    /**
     * @param SubCategoryCountParam $param
     * @param QueryListener|null $listener
     * @return SubCategoryCountResult
     */
    public function subCategoryCount(SubCategoryCountParam $param, QueryListener $listener = null);

    /**
     * @param UpdateUserParam $param
     * @param QueryListener|null $listener
     * @return UpdateUserResult
     */
    public function updateUsers(UpdateUserParam $param, QueryListener $listener = null);

    /**
     * @param ProductsCompareParam $param
     * @param QueryListener|null $listener
     * @return ProductsCompareResult
     */
    public function productsCompare(ProductsCompareParam $param, QueryListener $listener = null);

    /**
     * @param BlocksContentParam $param
     * @param QueryListener|null $listener
     * @return BlocksContentResult
     */
    public function blocksContent(BlocksContentParam $param, QueryListener $listener = null);
}
