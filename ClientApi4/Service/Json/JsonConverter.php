<?php

namespace Prediggo\ClientApi4\Service\Json;

interface JsonConverter {

    public function serialize($object);

    public function deSerialize($json, $typeName);
}