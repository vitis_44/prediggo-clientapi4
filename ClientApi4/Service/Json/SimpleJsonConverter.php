<?php

namespace Prediggo\ClientApi4\Service\Json;

use Prediggo\ClientApi4\Errors\JsonConversionException;
use Prediggo\ClientApi4\Service\Json\Converter;

class SimpleJsonConverter implements JsonConverter {

    private $serializers = array();
    private $deSerializers = array();

    public function __construct() {
        $this->serializers = [
            'Prediggo\ClientApi4\Types\AutoComplete\AutoCompleteParam' => function () {
                return new Converter\AutoComplete\ParamConverter();
            },
            'Prediggo\ClientApi4\Types\Track\TrackParam' => function () {
                return new Converter\Track\ParamConverter();
            },
            'Prediggo\ClientApi4\Types\RegisterUser\RegisterUserParam' => function () {
                return new Converter\RegisterUser\ParamConverter();
            },
            'Prediggo\ClientApi4\Types\ItemsInteraction\ItemsInteractionParam' => function () {
                return new Converter\ItemsInteraction\ParamConverter();
            },
            'Prediggo\ClientApi4\Types\PageContent\PageContentParam' => function () {
                return new Converter\PageContent\ParamConverter();
            },
            'Prediggo\ClientApi4\Types\PageContent\SimplePageContentParam' => function () {
                return new Converter\PageContent\SimpleParamConverter();
            },
            'Prediggo\ClientApi4\Types\UpdateItems\UpdateItemsParam' => function () {
                return new Converter\UpdateItems\ParamConverter();
            },
            'Prediggo\ClientApi4\Types\CachingStats\CachingStatsParam' => function () {
                return new Converter\CachingStats\ParamConverter();
            },
            'Prediggo\ClientApi4\Types\ViewUser\ViewUserParam' => function () {
                return new Converter\ViewUser\ParamConverter();
            },
            'Prediggo\ClientApi4\Types\DeleteUser\DeleteUserParam' => function () {
                return new Converter\DeleteUser\ParamConverter();
            },
            'Prediggo\ClientApi4\Types\ItemsSet\ItemsSetParam' => function () {
                return new Converter\ItemsSet\ParamConverter();
            },
            'Prediggo\ClientApi4\Types\ItemsInfo\ItemsInfoParam' => function () {
                return new Converter\ItemsInfo\ParamConverter();
            },
            'Prediggo\ClientApi4\Types\CategoryCount\CategoryCountParam' => function () {
                return new Converter\CategoryCount\ParamConverter();
            },
            'Prediggo\ClientApi4\Types\ProductAvailability\ProductAvailabilityParam' => function () {
                return new Converter\ProductAvailability\ParamConverter();
            },
            'Prediggo\ClientApi4\Types\SubCategoryCount\SubCategoryCountParam' => function () {
                return new Converter\SubCategoryCount\ParamConverter();
            },
            'Prediggo\ClientApi4\Types\UpdateUsers\UpdateUserParam' => function () {
                return new Converter\UpdateUsers\ParamConverter();
            },
            'Prediggo\ClientApi4\Types\ProductsCompare\ProductsCompareParam' => function () {
                return new Converter\ProductsCompare\ParamConverter();
            },
            'Prediggo\ClientApi4\Types\BlocksContent\BlocksContentParam' => function () {
                return new Converter\BlocksContent\BlocksContentParamConverter();
            },
        ];

        $this->deSerializers = [
            'Prediggo\ClientApi4\Types\AutoComplete\AutoCompleteResult' => function () {
                return new Converter\AutoComplete\ResultConverter();
            },
            'Prediggo\ClientApi4\Types\Track\TrackResult' => function () {
                return new Converter\Track\ResultConverter();
            },
            'Prediggo\ClientApi4\Types\RegisterUser\RegisterUserResult' => function () {
                return new Converter\RegisterUser\ResultConverter();
            },
            'Prediggo\ClientApi4\Types\ItemsInteraction\ItemsInteractionResult' => function () {
                return new Converter\ItemsInteraction\ResultConverter();
            },
            'Prediggo\ClientApi4\Types\PageContent\PageContentResult' => function () {
                return new Converter\PageContent\ResultConverter();
            },
            'Prediggo\ClientApi4\Types\UpdateItems\UpdateItemsResult' => function () {
                return new Converter\UpdateItems\ResultConverter();
            },
            'Prediggo\ClientApi4\Types\CachingStats\CachingStatsResult' => function () {
                return new Converter\CachingStats\ResultConverter();
            },
            'Prediggo\ClientApi4\Types\ViewUser\ViewUserResult' => function () {
                return new Converter\ViewUser\ResultConverter();
            },
            'Prediggo\ClientApi4\Types\DeleteUser\DeleteUserResult' => function () {
                return new Converter\DeleteUser\ResultConverter();
            },
            'Prediggo\ClientApi4\Types\ItemsSet\ItemsSetResult' => function () {
                return new Converter\ItemsSet\ResultConverter();
            },
            'Prediggo\ClientApi4\Types\ItemsInfo\ItemsInfoResult' => function () {
                return new Converter\ItemsInfo\ResultConverter();
            },
            'Prediggo\ClientApi4\Types\CategoryCount\CategoryCountResult' => function () {
                return new Converter\CategoryCount\ResultConverter();
            },
            'Prediggo\ClientApi4\Types\ProductAvailability\ProductAvailabilityResult' => function () {
                return new Converter\ProductAvailability\ResultConverter();
            },
            'Prediggo\ClientApi4\Types\SubCategoryCount\SubCategoryCountResult' => function () {
                return new Converter\SubCategoryCount\ResultConverter();
            },
            'Prediggo\ClientApi4\Types\UpdateUsers\UpdateUserResult' => function () {
                return new Converter\UpdateUsers\ResultConverter();
            },
            'Prediggo\ClientApi4\Types\ProductsCompare\ProductsCompareResult' => function () {
                return new Converter\ProductsCompare\ResultConverter();
            },
            'Prediggo\ClientApi4\Types\BlocksContent\BlocksContentResult' => function () {
                return new Converter\BlocksContent\BlocksContentResultConverter();
            },
        ];

    }

    public function serialize($object) {

        $typeName = get_class($object);

        if (!array_key_exists($typeName, $this->serializers)) {
            $ex = new JsonConversionException('No serializer for type : ' . $typeName);
            $ex->setDestinationType('string');
            $ex->setSourceObject($object);
            throw $ex;
        }

        $converter = $this->serializers[$typeName]();
        $jsonMap = $converter->toMap($object);

        $json = json_encode($jsonMap);

        if ($json === false) {
            $ex = new JsonConversionException('Json conversion error, message = ' . $this->jsonErrorMessage());
            $ex->setDestinationType('string');
            $ex->setSourceObject($object);

            throw $ex;
        }

        return $json;
    }

    public function jsonErrorMessage() {

        if (function_exists('json_last_error_msg')) {
            return json_last_error_msg();
        }

        $errorsMessages = array(
            JSON_ERROR_NONE => 'No error',
            JSON_ERROR_DEPTH => 'Maximum stack depth exceeded',
            JSON_ERROR_STATE_MISMATCH => 'State mismatch (invalid or malformed JSON)',
            JSON_ERROR_CTRL_CHAR => 'Control character error, possibly incorrectly encoded',
            JSON_ERROR_SYNTAX => 'Syntax error',
            JSON_ERROR_UTF8 => 'Malformed UTF-8 characters, possibly incorrectly encoded'
        );

        $error = json_last_error();
        return isset($errorsMessages[$error]) ? $errorsMessages[$error] : 'Unknown error';
    }

    public function deSerialize($json, $typeName) {

        if (!array_key_exists($typeName, $this->deSerializers)) {
            $ex = new JsonConversionException('No deserializer for type : ' . $typeName);
            $ex->setDestinationType($typeName);
            $ex->setSourceObject($json);
            throw $ex;
        }

        $jsonMap = json_decode($json, true);

        if ($jsonMap === false || $jsonMap === null) {
            $ex = new JsonConversionException('Json conversion error, message = ' . $this->jsonErrorMessage());
            $ex->setDestinationType($typeName);
            $ex->setSourceObject($json);
            throw  $ex;
        }

        $converter = $this->deSerializers[$typeName]();
        $object = $converter->toObject($jsonMap);

        return $object;

    }
}