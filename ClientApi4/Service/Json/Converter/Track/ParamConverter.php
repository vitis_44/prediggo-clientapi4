<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\Track;

use Prediggo\ClientApi4\Types\Track\TrackParam;

class ParamConverter {

    public function toMap($obj) {

        /** @var TrackParam $obj */
        $jsonMap = [
            'moduleVersion' => $obj->getModuleVersion(),
            'sessionId' => $obj->getSessionId(),
            'trackingCode' => $obj->getTrackingCode()
        ];

        return $jsonMap;
    }
}
