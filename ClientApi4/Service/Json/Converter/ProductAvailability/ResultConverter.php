<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\ProductAvailability;

use Prediggo\ClientApi4\Service\Json\ArrayHelper;
use Prediggo\ClientApi4\Service\Json\Converter\Debug\ResultConverter as DebugResultConverter;
use Prediggo\ClientApi4\Types\Debug\DebugResult;
use Prediggo\ClientApi4\Types\ProductAvailability\AdvancedResult;
use Prediggo\ClientApi4\Types\ProductAvailability\ProductAvailabilityResult;
use Prediggo\ClientApi4\Types\ProductAvailability\ProductAvailabilityStore;
use Prediggo\ClientApi4\Types\ProductAvailability\ProductInfoAttribute;
use Prediggo\ClientApi4\Types\ProductAvailability\ProductInfoValueLabelPair;

class ResultConverter
{

    private $debugResultConverter;

    public function __construct()
    {
        $this->debugResultConverter = new DebugResultConverter();
    }

    public function toObject($jsonMap)
    {

        $obj = new ProductAvailabilityResult();

        $obj->setStatus(ArrayHelper::extractSafely($jsonMap, 'status', ''));
        $obj->setTimeInMs(ArrayHelper::extractSafely($jsonMap, 'timeInMs', ''));
        $obj->setDebug($this->convertDebugResult(ArrayHelper::extractSafely($jsonMap, 'debug', array())));
        $obj->setSessionId(ArrayHelper::extractSafely($jsonMap, 'sessionId', ''));
        $obj->setRegion(ArrayHelper::extractSafely($jsonMap, 'region', ''));
        $this->fillAdvancedStructure(ArrayHelper::extractSafely($jsonMap, 'advanced', array()), $obj->getAdvanced());
        $obj->setSku(ArrayHelper::extractSafely($jsonMap, 'sku', ''));
        $obj->setAttributesToReturn($this->convertAttributesToReturn(ArrayHelper::extractSafely($jsonMap, 'attributesToReturn', array())));
        $obj->setStores($this->convertStores(ArrayHelper::extractSafely($jsonMap, 'stores', array())));

        return $obj;
    }

    /**
     * @param array $advancedNode
     * @param AdvancedResult $advancedObj
     */
    private function fillAdvancedStructure($advancedNode, $advancedObj)
    {
        $advancedObj->setCustomerId(ArrayHelper::extractSafely($advancedNode, 'customerId', ''));
        $advancedObj->setDevice(ArrayHelper::extractSafely($advancedNode, 'device', ''));
        $advancedObj->setReferenceDate(ArrayHelper::extractSafely($advancedNode, 'referenceDate', ''));
        $advancedObj->setReferrer(ArrayHelper::extractSafely($advancedNode, 'referer', ''));
        $advancedObj->setOffer(ArrayHelper::extractSafely($advancedNode, 'offer', ''));
        $advancedObj->setStore(ArrayHelper::extractSafely($advancedNode, 'store', ''));
        $advancedObj->setUrl(ArrayHelper::extractSafely($advancedNode, 'url', ''));
    }

    /**
     * @param $debug
     * @return DebugResult
     */
    private function convertDebugResult($debug)
    {
        return $this->debugResultConverter->toObject($debug);
    }

    /**
     * @param array $attributes
     * @return array
     */
    private function convertAttributesToReturn($attributes)
    {
        $attributesToReturn = array();

        foreach ($attributes as $attribute) {
            $attributesToReturn[] = $attribute;
        }

        return $attributesToReturn;
    }

    /**
     * @param array $stores
     * @return array
     */
    private function convertStores($stores)
    {
        $convertedStores = array();

        foreach ($stores as $store) {
            $productAvailabilityStore = new ProductAvailabilityStore();

            $productAvailabilityStore->setStoreCode(ArrayHelper::extractSafely($store, 'storeCode', ''));
            $productAvailabilityStore->setStoreLabel(ArrayHelper::extractSafely($store, 'storeLabel', ''));
            $productAvailabilityStore->setAttributeInfo($this->convertProductInfoAttribute(ArrayHelper::extractSafely($store, 'attributeInfo', array())));

            $convertedStores[] = $productAvailabilityStore;
        }

        return $convertedStores;
    }

    /**
     * @param array $attributeInfos
     * @return array
     */
    private function convertProductInfoAttribute($attributeInfos)
    {
        $convertedProductInfoAttribute = array();

        foreach ($attributeInfos as $attributeInfo) {
            $productInfoAttribute = new ProductInfoAttribute();

            $productInfoAttribute->setAttributeName(ArrayHelper::extractSafely($attributeInfo, 'attributeName', ''));
            $productInfoAttribute->setAttributeType(ArrayHelper::extractSafely($attributeInfo, 'attributeType', ''));
            $productInfoAttribute->setAttributeLabel(ArrayHelper::extractSafely($attributeInfo, 'attributeLabel', ''));
            $productInfoAttribute->setVals($this->convertVals(ArrayHelper::extractSafely($attributeInfo, 'vals', array())));

            $convertedProductInfoAttribute[] = $productInfoAttribute;
        }

        return $convertedProductInfoAttribute;
    }

    /**
     * @param array $vals
     * @return array
     */
    private function convertVals($vals)
    {
        $convertedProductInfoValueLabelPair = array();

        foreach ($vals as $val) {
            $productInfoValueLabelPair = new ProductInfoValueLabelPair();

            $productInfoValueLabelPair->setValue(ArrayHelper::extractSafely($val, 'value', ''));
            $productInfoValueLabelPair->setLabel(ArrayHelper::extractSafely($val, 'label', ''));

            $convertedProductInfoValueLabelPair[] = $productInfoValueLabelPair;
        }

        return $convertedProductInfoValueLabelPair;
    }
}
