<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\UpdateItems;

use Prediggo\ClientApi4\Service\Json\ArrayHelper;
use Prediggo\ClientApi4\Service\Json\Converter\Debug\ResultConverter as DebugResultConverter;
use Prediggo\ClientApi4\Types\Debug\DebugResult;
use Prediggo\ClientApi4\Types\UpdateItems\RejectedItemsResult;
use Prediggo\ClientApi4\Types\UpdateItems\UpdatedItemsResult;
use Prediggo\ClientApi4\Types\UpdateItems\UpdateItemsResult;

class ResultConverter {

    private $debugResultConverter;

    public function __construct()
    {
        $this->debugResultConverter = new DebugResultConverter();
    }

    public function toObject($jsonMap) {

        $obj = new UpdateItemsResult();

        $obj->setStatus(ArrayHelper::extractSafely($jsonMap, 'status', ''));
        $obj->setTimeInMs(ArrayHelper::extractSafely($jsonMap, 'timeInMs', 0.0));
        $obj->setSessionId(ArrayHelper::extractSafely($jsonMap, 'sessionId', ''));
        $obj->setRegion(ArrayHelper::extractSafely($jsonMap, 'region', ''));
        $obj->setUpdatedItems($this->convertUpdatedItems(ArrayHelper::extractSafely($jsonMap, 'updatedItems', array())));
        $obj->setRejectedItems($this->convertRejectedItems(ArrayHelper::extractSafely($jsonMap, 'rejectedItems', array())));
        $obj->setDebug($this->convertDebugResult(ArrayHelper::extractSafely($jsonMap, 'debug', array())));
        $obj->setDebugText(ArrayHelper::extractSafely($jsonMap, 'debugText', ''));

        return $obj;
    }

    /**
     * @param $debug
     * @return DebugResult
     */
    private function convertDebugResult($debug)
    {
        return $this->debugResultConverter->toObject($debug);
    }

    private function convertUpdatedItems($updatedItems) {

        $updatedItemList = array();

        foreach ($updatedItems as $updatedItem) {

            $updatedItems = new UpdatedItemsResult();

            $updatedItems->setSku(ArrayHelper::extractSafely($updatedItem, 'sku', ''));
            $updatedItems->setZoneName(ArrayHelper::extractSafely($updatedItem, 'zoneName', ''));
            $updatedItems->setRegion(ArrayHelper::extractSafely($updatedItem, 'region', ''));
            $updatedItems->setOffer(ArrayHelper::extractSafely($updatedItem, 'offer', ''));
            $updatedItems->setStore(ArrayHelper::extractSafely($updatedItem, 'store', ''));

            $updatedItemList[] = $updatedItems;
        }

        return $updatedItemList;
    }

    private function convertRejectedItems($rejectedItems) {

        $rejectedItemList = array();

        foreach ($rejectedItems as $rejectedItem) {

            $rejectedItems = new RejectedItemsResult();

            $rejectedItems->setSku(ArrayHelper::extractSafely($rejectedItem, 'sku', ''));
            $rejectedItems->setZoneName(ArrayHelper::extractSafely($rejectedItem, 'zoneName', ''));
            $rejectedItems->setRegion(ArrayHelper::extractSafely($rejectedItem, 'region', ''));
            $rejectedItems->setOffer(ArrayHelper::extractSafely($rejectedItem, 'offer', ''));
            $rejectedItems->setStore(ArrayHelper::extractSafely($rejectedItem, 'store', ''));
            $rejectedItems->setErrorMessage(ArrayHelper::extractSafely($rejectedItem, 'errorMessage', ''));

            $rejectedItemList[] = $rejectedItems;
        }

        return $rejectedItemList;
    }
}