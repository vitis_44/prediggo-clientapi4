<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\UpdateItems;

use Prediggo\ClientApi4\Types\UpdateItems\ItemParam;
use Prediggo\ClientApi4\Types\UpdateItems\UpdateItemsParam;

class ParamConverter {

    public function toMap($obj) {

        /** @var UpdateItemsParam $obj */
        $jsonMap = [
            'moduleVersion' => $obj->getModuleVersion(),
            'action' => $obj->getAction()
        ];

        $itemsParam = $this->convertItems($obj->getItems());

        $jsonMap['items'] = $itemsParam;

        return $jsonMap;
    }

    /**
     * @param ItemParam[] $itemsParam
     * @return array
     */
    public function convertItems($itemsParam) {

        $items = array();

        foreach ($itemsParam as $item) {

            /** @var ItemParam $block */
            $itemMap = [
                'sku' => $item->getSku(),
                'zoneName' => $item->getZoneName(),
                'price' => $item->getPrice(),
                'standardPrice' => $item->getStandardPrice(),
                'stock' => $item->getStock(),
                'recommendable' => $item->isRecommendable(),
                'searchable' => $item->isSearchable(),
                'region' => $item->getRegion(),
                'offer' => $item->getOffer(),
                'store' => $item->getStore(),
                'attributes' => $item->getAttributes()
            ];

            $items[] = $itemMap;
        }

        return $items;
    }
}
