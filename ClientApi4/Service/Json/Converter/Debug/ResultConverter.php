<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\Debug;

use Prediggo\ClientApi4\Service\Json\ArrayHelper;
use Prediggo\ClientApi4\Types\Debug\DebugObjectAttributeConstraint;
use Prediggo\ClientApi4\Types\Debug\DebugObjectAttributeConstraintStep;
use Prediggo\ClientApi4\Types\Debug\DebugObjectBlock;
use Prediggo\ClientApi4\Types\Debug\DebugObjectPage;
use Prediggo\ClientApi4\Types\Debug\DebugObjectSlot;
use Prediggo\ClientApi4\Types\Debug\DebugObjectSlotRecommendationRule;
use Prediggo\ClientApi4\Types\Debug\DebugResult;
use Prediggo\ClientApi4\Types\Item\AttributeGroup;
use Prediggo\ClientApi4\Types\Item\AttributeValue;
use Prediggo\ClientApi4\Types\Item\GroupingInfo;
use Prediggo\ClientApi4\Types\Item\ItemInfo;

class ResultConverter
{

    public function toObject($jsonMap)
    {

        $obj = new DebugResult();

        $obj->setModuleVersion(ArrayHelper::extractSafely($jsonMap, 'moduleVersion', ''));
        $obj->setServerNodeId(ArrayHelper::extractSafely($jsonMap, 'serverNodeId', ''));
        $obj->setDebugText(ArrayHelper::extractSafely($jsonMap, 'debugText', ''));
        $obj->setSessionId(ArrayHelper::extractSafely($jsonMap, 'sessionId', ''));
        $obj->setDebugObjectPage($this->convertDebugObjectPage(ArrayHelper::extractSafely($jsonMap, 'debugObjectPage', array())));

        return $obj;
    }

    /**
     * @param DebugObjectPage $debugObjectPageObject
     */
    private function convertDebugObjectPage($debugObjectPageObject)
    {
        $debugObjectPage = new DebugObjectPage();

        $debugObjectPage->setPageLabel(ArrayHelper::extractSafely($debugObjectPageObject, 'pageLabel', ''));
        $debugObjectPage->setPageCode(ArrayHelper::extractSafely($debugObjectPageObject, 'pageCode', ''));
        $debugObjectPage->setDebugObjectBlockList($this->convertDebugObjectBlockList($debugObjectPageObject));

        return $debugObjectPage;
    }

    /**
     * @param DebugObjectSlotRecommendationRule[] $debugObjectSlotRecommendationRule
     * @return array
     */
    private function convertAttributeConstraintsComputationSteps($debugObjectSlotRecommendationRule)
    {
        $attributeConstraintsComputationSteps = array();
        foreach ($debugObjectSlotRecommendationRule as $attributeConstraintsComputationStep) {

            $attributeConstraintsComputationStepConverter = new DebugObjectAttributeConstraintStep();

            $attributeConstraintsComputationStepConverter->setStepName(ArrayHelper::extractSafely($attributeConstraintsComputationStep, 'stepName', ''));
            $attributeConstraintsComputationStepConverter->setNbCandidatesBefore(ArrayHelper::extractSafely($attributeConstraintsComputationStep, 'nbCandidatesBefore', 0));
            $attributeConstraintsComputationStepConverter->setNbCandidatesBefore(ArrayHelper::extractSafely($attributeConstraintsComputationStep, 'nbCandidatesAfter', 0));
            $attributeConstraintsComputationStepConverter->setTimeInMs(ArrayHelper::extractSafely($attributeConstraintsComputationStep, 'timeInMs', 0.0));
            $attributeConstraintsComputationStepConverter->setAttributeConstraintList(
                $this->convertAttributeConstraintList(ArrayHelper::extractSafely($attributeConstraintsComputationStep, 'attributeConstraintList', array())));

            $attributeConstraintsComputationSteps[] = $attributeConstraintsComputationStepConverter;
        }

        return $attributeConstraintsComputationSteps;
    }

    /**
     * @param DebugObjectSlot[] $debugObjectSlot
     * @return array
     */
    private function convertDebugObjectSlotRecommendationRules($debugObjectSlot)
    {
        $debugObjectSlotRecommendationRules = array();
        foreach ($debugObjectSlot as $debugObjectSlotRecommendationRule) {

            $debugObjectSlotRecommendationRuleConverter = new DebugObjectSlotRecommendationRule();

            $debugObjectSlotRecommendationRuleConverter->setRuleTemplateCode(ArrayHelper::extractSafely($debugObjectSlotRecommendationRule, 'ruleTemplateCode', ''));
            $debugObjectSlotRecommendationRuleConverter->setRuleTemplateLabel(ArrayHelper::extractSafely($debugObjectSlotRecommendationRule, 'ruleTemplateLabel', ''));
            $debugObjectSlotRecommendationRuleConverter->setComputationTimeInMs(ArrayHelper::extractSafely($debugObjectSlotRecommendationRule, 'computationTimeInMs', 0.0));
            $debugObjectSlotRecommendationRuleConverter->setComputationStatus(ArrayHelper::extractSafely($debugObjectSlotRecommendationRule, 'computationStatus', ''));
            $debugObjectSlotRecommendationRuleConverter->setCandidatesRetrievalStatus(ArrayHelper::extractSafely($debugObjectSlotRecommendationRule, 'candidatesRetrievalStatus', ''));
            $debugObjectSlotRecommendationRuleConverter->setNbValidCandidatesBeforeConstraints(ArrayHelper::extractSafely($debugObjectSlotRecommendationRule, 'nbValidCandidatesBeforeConstraints', 0));
            $debugObjectSlotRecommendationRuleConverter->setNbValidCandidatesMatchingConstraints(ArrayHelper::extractSafely($debugObjectSlotRecommendationRule, 'nbValidCandidatesMatchingConstraints', 0));
            $debugObjectSlotRecommendationRuleConverter->setNbValidCandidatesBeforeStrategy(ArrayHelper::extractSafely($debugObjectSlotRecommendationRule, 'nbValidCandidatesBeforeStrategy', 0));
            $debugObjectSlotRecommendationRuleConverter->setStrategyFoundResult(ArrayHelper::extractSafely($debugObjectSlotRecommendationRule, 'strategyFoundResult', false));
            $debugObjectSlotRecommendationRuleConverter->setStrategyCode(ArrayHelper::extractSafely($debugObjectSlotRecommendationRule, 'strategyCode', ''));
            $debugObjectSlotRecommendationRuleConverter->setStrategyLabel(ArrayHelper::extractSafely($debugObjectSlotRecommendationRule, 'strategyLabel', ''));
            $debugObjectSlotRecommendationRuleConverter->setResultItemId(ArrayHelper::extractSafely($debugObjectSlotRecommendationRule, 'resultItemId', ''));
            $debugObjectSlotRecommendationRuleConverter->setAttributeConstraintsComputationSteps(
                $this->convertAttributeConstraintsComputationSteps(
                    ArrayHelper::extractSafely($debugObjectSlotRecommendationRule, 'attributeConstraintsComputationSteps', array())
                )
            );

            $debugObjectSlotRecommendationRules[] = $debugObjectSlotRecommendationRuleConverter;
        }

        return $debugObjectSlotRecommendationRules;
    }

    /**
     * @param DebugObjectBlock[] $debugObjectBlockConverter
     * @return array
     */
    private function convertDebugObjectSlots($debugObjectBlockConverter)
    {
        $debugObjectSlots = array();
        foreach ($debugObjectBlockConverter as $debugObjectSlot) {

            $debugObjectSlotConverter = new DebugObjectSlot();

            $debugObjectSlotConverter->setSlotPosition(ArrayHelper::extractSafely($debugObjectSlot, 'slotPosition', 0));
            $debugObjectSlotConverter->setComputationTimeInMs(ArrayHelper::extractSafely($debugObjectSlot, 'computationTimeInMs', 0.0));
            $debugObjectSlotConverter->setResultItemId(ArrayHelper::extractSafely($debugObjectSlot, 'resultItemId', ''));
            $debugObjectSlotConverter->setComputationMethod(ArrayHelper::extractSafely($debugObjectSlot, 'computationMethod', ''));
            $debugObjectSlotConverter->setComputationStatus(ArrayHelper::extractSafely($debugObjectSlot, 'computationStatus', ''));
            $debugObjectSlotConverter->setRuleType(ArrayHelper::extractSafely($debugObjectSlot, 'ruleType', ''));
            $debugObjectSlotConverter->setDebugObjectSlotRecommendationRules(
                $this->convertDebugObjectSlotRecommendationRules(
                    ArrayHelper::extractSafely($debugObjectSlot, 'debugObjectSlotRecommendationRules', array())
                )
            );

            $debugObjectSlots[] = $debugObjectSlotConverter;
        }

        return $debugObjectSlots;
    }

    /**
     * @param DebugObjectAttributeConstraintStep[] $attributeConstraintsComputationStep
     * @return array
     */
    private function convertAttributeConstraintList($attributeConstraintsComputationStep)
    {
        $attributeConstraintList = array();
        foreach ($attributeConstraintsComputationStep as $attributeConstraint) {

            $attributeConstraintConverter = new DebugObjectAttributeConstraint();

            $attributeConstraintConverter->setAttributeName(ArrayHelper::extractSafely($attributeConstraint, 'attributeName', ''));
            $attributeConstraintConverter->setNbCandidatesBefore(ArrayHelper::extractSafely($attributeConstraint, 'nbCandidatesBefore', 0));
            $attributeConstraintConverter->setNbCandidatesByThisAttributeConstraint(ArrayHelper::extractSafely($attributeConstraint, 'nbCandidatesByThisAttributeConstraint', 0));
            $attributeConstraintConverter->setNbCandidatesAfter(ArrayHelper::extractSafely($attributeConstraint, 'nbCandidatesAfter', 0));
            $attributeConstraintConverter->setStatus(ArrayHelper::extractSafely($attributeConstraint, 'status', ''));

            $attributeConstraintList[] = $attributeConstraintConverter;
        }

        return $attributeConstraintList;
    }

    /**
     * @param array $debugObjectPageObject
     * @return array
     */
    private function convertDebugObjectBlockList($debugObjectPageObject)
    {
        $debugObjectBlocks = array();
        foreach (ArrayHelper::extractSafely($debugObjectPageObject, 'debugObjectBlockList', array()) as $debugObjectBlock) {
            $debugObjectBlockConverter = new DebugObjectBlock();

            $debugObjectBlockConverter->setBlockCode(ArrayHelper::extractSafely($debugObjectBlock, 'blockCode', ''));
            $debugObjectBlockConverter->setBlockLabel(ArrayHelper::extractSafely($debugObjectBlock, 'blockLabel', ''));
            $debugObjectBlockConverter->setScenarioCode(ArrayHelper::extractSafely($debugObjectBlock, 'scenarioCode', ''));
            $debugObjectBlockConverter->setScenarioLabel(ArrayHelper::extractSafely($debugObjectBlock, 'scenarioLabel', ''));
            $debugObjectBlockConverter->setMaxNbResultsExpected(ArrayHelper::extractSafely($debugObjectBlock, 'maxNbResultsExpected', 0.0));
            $debugObjectBlockConverter->setDebugObjectSlots($this->convertDebugObjectSlots(ArrayHelper::extractSafely($debugObjectBlock, 'debugObjectSlots', array())));

            $debugObjectBlocks[] = $debugObjectBlockConverter;
        }
        return $debugObjectBlocks;
    }
}
