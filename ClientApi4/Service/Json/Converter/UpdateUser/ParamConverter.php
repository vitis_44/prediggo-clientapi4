<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\UpdateUser;


use Prediggo\ClientApi4\Types\UpdateUser\UpdatedUserParam;
use Prediggo\ClientApi4\Types\UpdateUser\UpdateUserParam;

class ParamConverter {

    public function toMap($obj) {

        /** @var UpdateUserParam $obj */
        $jsonMap = [
            'moduleVersion' => $obj->getModuleVersion(),
            'action' => $obj->getAction()
        ];

        $itemsParam = $this->convertUsers($obj->getUsers());

        $jsonMap['items'] = $itemsParam;

        return $jsonMap;
    }

    /**
     * @param UpdatedUserParam[] $usersParam
     * @return array
     */
    public function convertUsers($usersParam) {

        $users = array();

        foreach ($usersParam as $user) {

            /** @var UpdatedUserParam $block */
            $userMap = [
                'sessionId' => $user->getSessionId(),
                'customerId' => $user->getCustomerId(),
                'attributes' => $user->getAttributes()
            ];

            $users[] = $userMap;
        }

        return $users;
    }
}
