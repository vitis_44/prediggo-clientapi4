<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\DeleteUser;

use Prediggo\ClientApi4\Types\DeleteUser\DeleteUserParam;


class ParamConverter {

    public function toMap($obj) {

        /** @var DeleteUserParam $obj */
        $jsonMap = [
            'moduleVersion' => $obj->getModuleVersion(),
            'sessionId' => $obj->getSessionId(),
            'customerId' => $obj->getCustomerId()
        ];

        return $jsonMap;
    }
}
