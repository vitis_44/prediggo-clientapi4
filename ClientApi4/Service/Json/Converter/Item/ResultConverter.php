<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\Item;

use Prediggo\ClientApi4\Service\Json\ArrayHelper;
use Prediggo\ClientApi4\Types\Item\AttributeGroup;
use Prediggo\ClientApi4\Types\Item\AttributeValue;
use Prediggo\ClientApi4\Types\Item\GroupingInfo;
use Prediggo\ClientApi4\Types\Item\ItemInfo;
use Prediggo\ClientApi4\Types\PageContent\PictogramHtmlResult;
use Prediggo\ClientApi4\Types\PageContent\PictogramResult;
use Prediggo\ClientApi4\Types\PageContent\PictogramType;
use Prediggo\ClientApi4\Types\PageContent\PictogramUrlResult;

class ResultConverter {

    public function toObject($jsonMap) {

        $obj = new ItemInfo();

        $obj->setPrice(ArrayHelper::extractSafely($jsonMap, 'price', 0.0));
        $obj->setSku(ArrayHelper::extractSafely($jsonMap, 'sku', ''));
        $obj->setItemType(ArrayHelper::extractSafely($jsonMap, 'itemType', ''));
        $obj->setTrackingCode(ArrayHelper::extractSafely($jsonMap, 'trackingCode', ''));

        if (array_key_exists('groupingInfo', $jsonMap)) {
            $obj->setGroupingInfo($this->convertGroupingInfo($jsonMap['groupingInfo']));
        }

        $obj->setAttributeInfo($this->convertAttributeGroups(ArrayHelper::extractSafely($jsonMap, 'attributeInfo', array())));

        $obj->setPictograms($this->convertPictograms(ArrayHelper::extractSafely($jsonMap, 'pictograms', array())));

        return $obj;
    }


    private function convertAttributeGroups($groupCollectionNode) {

        $list = array();

        foreach ($groupCollectionNode as $groupNode) {

            $thisGroup = new AttributeGroup();

            $thisGroup->setAttributeName(ArrayHelper::extractSafely($groupNode, 'attributeName', ''));
            $thisGroup->setAttributeLabel(ArrayHelper::extractSafely($groupNode, 'attributeLabel', ''));
            $thisGroup->setAttributeType(ArrayHelper::extractSafely($groupNode, 'attributeType', ''));
            $thisGroup->setAttributeUnit(ArrayHelper::extractSafely($groupNode, 'attributeUnit', ''));
            $thisGroup->setVals($this->convertAttributeVals(ArrayHelper::extractSafely($groupNode, 'vals', [])));

            $list[] = $thisGroup;
        }

        return $list;
    }

    private function convertAttributeVals($valsCollectionNode) {

        $vals = array();
        foreach ($valsCollectionNode as $valNode) {

            $thisVal = new AttributeValue();

            $thisVal->setValue(ArrayHelper::extractSafely($valNode, 'value', ''));
            $thisVal->setLabel(ArrayHelper::extractSafely($valNode, 'label', ''));

            $vals[] = $thisVal;
        }

        return $vals;
    }

    private function convertGroupingInfo($groupingInfoNode) {

        $grouping = new GroupingInfo();

        $grouping->setReferenceItemSku(ArrayHelper::extractSafely($groupingInfoNode, 'referenceItemSku', ''));
        $grouping->setChildrenSkus(ArrayHelper::extractSafely($groupingInfoNode, 'childrenSkus', array()));

        return $grouping;
    }

    private function convertPictograms($pictogramsNode)
    {
        $list = array();

        foreach ($pictogramsNode as $pictogramNode) {

            $pictogram = null;
            switch ($pictogramNode['type']) {
                case PictogramType::HTML:
                    $pictogram = new PictogramHtmlResult();
                    $pictogram->setHtml(ArrayHelper::extractSafely($pictogramNode, 'html', ''));
                    break;

                case PictogramType::URL:
                    $pictogram = new PictogramUrlResult();
                    $pictogram->setUrl(ArrayHelper::extractSafely($pictogramNode, 'url', ''));
                    break;

                default:
                    // Handle unsupported types or fallback logic
                    break;
            }

            // Set common properties for both types
            if ($pictogram !== null) {
                $pictogram->setPictogramId(ArrayHelper::extractSafely($pictogramNode, 'pictogramId'));
                $pictogram->setType(ArrayHelper::extractSafely($pictogramNode, 'type', ''));
                $pictogram->setPosition(ArrayHelper::extractSafely($pictogramNode, 'position', ''));
                $pictogram->setRedirectionUrl(ArrayHelper::extractSafely($pictogramNode, 'redirectionUrl', ''));
                $pictogram->setAltValue(ArrayHelper::extractSafely($pictogramNode, 'altValue', ''));
            }

            if ($pictogram != null) {
                $list[] = $pictogram;
            }
        }

        return $list;
    }
}
