<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\CachingStats;

use Prediggo\ClientApi4\Service\Json\ArrayHelper;
use Prediggo\ClientApi4\Service\Json\Converter\Debug\ResultConverter as DebugResultConverter;
use Prediggo\ClientApi4\Types\CachingStats\CachingStatsResult;

class ResultConverter {

    private $debugResultConverter;

    public function __construct() {
        $this->debugResultConverter = new DebugResultConverter();
    }

    public function toObject($jsonMap) {

        $obj = new CachingStatsResult();

        $obj->setStatus(ArrayHelper::extractSafely($jsonMap, 'status', ''));
        $obj->setTimeInMs(ArrayHelper::extractSafely($jsonMap, 'timeInMs', 0.0));
        $obj->setSessionId(ArrayHelper::extractSafely($jsonMap, 'sessionId', ''));
        $obj->setServerNodeId(ArrayHelper::extractSafely($jsonMap, 'serverNodeId', ''));
        $obj->setCachingStatsKey(ArrayHelper::extractSafely($jsonMap, 'cachingStatsKey', ''));
        $obj->setDebug($this->convertDebugResult(ArrayHelper::extractSafely($jsonMap, 'debug', array())));

        return $obj;
    }

    private function convertDebugResult($debug)
    {
        return $this->debugResultConverter->toObject($debug);
    }
}
