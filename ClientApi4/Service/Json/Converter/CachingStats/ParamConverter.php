<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\CachingStats;


use Prediggo\ClientApi4\Types\CachingStats\CachingStatsParam;

class ParamConverter {

    public function toMap($obj) {

        /** @var CachingStatsParam $obj */
        $jsonMap = [
            'moduleVersion' => $obj->getModuleVersion(),
            'sessionId' => $obj->getSessionId(),
            'cachingStatsKey' => $obj->getCachingStatsKey()
        ];

        return $jsonMap;
    }
}
