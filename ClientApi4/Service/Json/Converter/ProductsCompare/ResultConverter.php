<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\ProductsCompare;

use Prediggo\ClientApi4\Service\Json\ArrayHelper;
use Prediggo\ClientApi4\Service\Json\Converter\Debug\ResultConverter as DebugResultConverter;
use Prediggo\ClientApi4\Service\Json\Converter\Item\ResultConverter as ItemResultConverter;
use Prediggo\ClientApi4\Types\Debug\DebugResult;
use Prediggo\ClientApi4\Types\PageContent\FrontResult;
use Prediggo\ClientApi4\Types\ProductsCompare\AdvancedResult;
use Prediggo\ClientApi4\Types\ProductsCompare\ProductsCompareAttribute;
use Prediggo\ClientApi4\Types\ProductsCompare\ProductsCompareAttributeValueAndLabel;
use Prediggo\ClientApi4\Types\ProductsCompare\ProductsCompareFamily;
use Prediggo\ClientApi4\Types\ProductsCompare\ProductsCompareGroup;
use Prediggo\ClientApi4\Types\ProductsCompare\ProductsCompareResult;

class ResultConverter
{

    private $debugResultConverter;
    private $itemConverter;

    public function __construct()
    {
        $this->debugResultConverter = new DebugResultConverter();
        $this->itemConverter = new ItemResultConverter();
    }

    public function toObject($jsonMap)
    {

        $obj = new ProductsCompareResult();

        $obj->setStatus(ArrayHelper::extractSafely($jsonMap, 'status', ''));
        $obj->setTimeInMs(ArrayHelper::extractSafely($jsonMap, 'timeInMs', ''));
        $obj->setDebug($this->convertDebugResult(ArrayHelper::extractSafely($jsonMap, 'debug', array())));
        $obj->setSessionId(ArrayHelper::extractSafely($jsonMap, 'sessionId', ''));
        $this->fillAdvancedStructure(ArrayHelper::extractSafely($jsonMap, 'advanced', array()), $obj->getAdvanced());
        $obj->setSkusWithoutFamily(ArrayHelper::extractSafely($jsonMap, 'skusWithoutFamily', array()));
        $obj->setUnknownSkus(ArrayHelper::extractSafely($jsonMap, 'unknownSkus', array()));
        $obj->setFamilies($this->convertProductsCompareFamily(ArrayHelper::extractSafely($jsonMap, 'families', array())));

        return $obj;
    }

    /**
     * @param array $advancedNode
     * @param AdvancedResult $advancedObj
     */
    private function fillAdvancedStructure($advancedNode, $advancedObj)
    {
        $advancedObj->setCustomerId(ArrayHelper::extractSafely($advancedNode, 'customerId', ''));
        $advancedObj->setDevice(ArrayHelper::extractSafely($advancedNode, 'device', ''));
        $advancedObj->setReferenceDate(ArrayHelper::extractSafely($advancedNode, 'referenceDate', ''));
        $advancedObj->setReferrer(ArrayHelper::extractSafely($advancedNode, 'referer', ''));
        $advancedObj->setOffer(ArrayHelper::extractSafely($advancedNode, 'offer', ''));
        $advancedObj->setStore(ArrayHelper::extractSafely($advancedNode, 'store', ''));
        $advancedObj->setUrl(ArrayHelper::extractSafely($advancedNode, 'url', ''));
    }

    /**
     * @param $debug
     * @return DebugResult
     */
    private function convertDebugResult($debug)
    {
        return $this->debugResultConverter->toObject($debug);
    }

    /**
     * @param ProductsCompareFamily[] $families
     * @return array
     */
    private function convertProductsCompareFamily($families)
    {
        $convertedProductsCompareFamily = array();

        foreach ($families as $family) {
            $productsCompareFamily = new ProductsCompareFamily();

            $productsCompareFamily->setFamilyName(ArrayHelper::extractSafely($family, 'familyName', ''));
            $productsCompareFamily->setFront($this->convertFront(ArrayHelper::extractSafely($family, 'front', '')));
            $productsCompareFamily->setItems($this->convertItemsMap(ArrayHelper::extractSafely($family, 'items', array())));
            $productsCompareFamily->setGroups($this->convertGroups(ArrayHelper::extractSafely($family, 'groups', array())));

            $convertedProductsCompareFamily[] = $productsCompareFamily;
        }

        return $convertedProductsCompareFamily;
    }

    /**
     * @param FrontResult $front
     * @return FrontResult
     */
    private function convertFront($front)
    {

        $obj = new FrontResult();
        $obj->setLabel(ArrayHelper::extractSafely($front, 'label', ''));
        $obj->setExtraLabels(ArrayHelper::extractSafely($front, 'extraLabels', []));

        return $obj;
    }

    private function convertItemsMap($itemsNode)
    {

        $itemMap = array();

        foreach ($itemsNode as $sku => $itemNode) {
            $itemMap[$sku] = $this->itemConverter->toObject($itemNode);
        }

        return $itemMap;
    }

    /**
     * @param ProductsCompareGroup[] $groups
     * @return ProductsCompareGroup[]
     */
    private function convertGroups($groups)
    {
        $productsCompareGroups = array();
        foreach ($groups as $group) {

            $productsCompareGroup = new ProductsCompareGroup();

            $productsCompareGroup->setGroupName(ArrayHelper::extractSafely($group, 'groupName', ''));
            $productsCompareGroup->setPosition(ArrayHelper::extractSafely($group, 'position', 0));
            $productsCompareGroup->setFront($this->convertFront(ArrayHelper::extractSafely($group, 'front', '')));
            $productsCompareGroup->setAttributes($this->convertAttributes(ArrayHelper::extractSafely($group, 'attributes', array())));

            $productsCompareGroups[] = $productsCompareGroup;
        }

        return $productsCompareGroups;
    }

    /**
     * @param ProductsCompareAttribute[] $attributes
     * @return ProductsCompareAttribute[]
     */
    private function convertAttributes($attributes)
    {
        $productsCompareAttributes = array();
        foreach ($attributes as $attribute) {

            $productsCompareAttribute = new ProductsCompareAttribute();

            $productsCompareAttribute->setAttributeName(ArrayHelper::extractSafely($attribute, 'attributeName', ''));
            $productsCompareAttribute->setAttributeLabel(ArrayHelper::extractSafely($attribute, 'attributeLabel', ''));
            $productsCompareAttribute->setAttributeType(ArrayHelper::extractSafely($attribute, 'attributeType', ''));
            $productsCompareAttribute->setAttributeUnit(ArrayHelper::extractSafely($attribute, 'attributeUnit', ''));
            $productsCompareAttribute->setPosition(ArrayHelper::extractSafely($attribute, 'position', 0));
            $productsCompareAttribute->setItems($this->convertItems(ArrayHelper::extractSafely($attribute, 'items', array())));

            $productsCompareAttributes[] = $productsCompareAttribute;
        }

        return $productsCompareAttributes;
    }

    /**
     * @param ProductsCompareAttributeValueAndLabel[] $items
     * @return ProductsCompareAttributeValueAndLabel[]
     */
    private function convertItems($items)
    {
        $productsCompareAttributes = array();
        foreach ($items as $key => $values) {
            if (!array_key_exists($key, $productsCompareAttributes)) {
                $itemsList = array();
            } else {
                $itemsList = $productsCompareAttributes[$key];
            }

            foreach ($values as $item) {
                $productsCompareAttribute = new ProductsCompareAttributeValueAndLabel();

                $productsCompareAttribute->setValue(ArrayHelper::extractSafely($item, 'value', ''));
                $productsCompareAttribute->setLabel(ArrayHelper::extractSafely($item, 'label', ''));

                array_push($itemsList, $productsCompareAttribute);
            }

            $productsCompareAttributes[$key] = $itemsList;
        }

        return $productsCompareAttributes;
    }
}
