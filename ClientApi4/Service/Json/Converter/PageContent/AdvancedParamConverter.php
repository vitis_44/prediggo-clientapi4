<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\PageContent;

use Prediggo\ClientApi4\Service\Json\ArrayHelper;
use Prediggo\ClientApi4\Types\PageContent\AdvancedParam;

class AdvancedParamConverter {
    /** @var AdvancedParam $obj */
    public function toMap($obj) {
        $advancedMap = [];
        ArrayHelper::pushNonNull($advancedMap, 'zone', $obj->getZone());
        ArrayHelper::pushNonNull($advancedMap, 'offer', $obj->getOffer());
        ArrayHelper::pushNonNull($advancedMap, 'store', $obj->getStore());
        ArrayHelper::pushNonNull($advancedMap, 'customerId', $obj->getCustomerId());
        ArrayHelper::pushNonNull($advancedMap, 'device', $obj->getDevice());
        ArrayHelper::pushNonNull($advancedMap, 'referenceDate', $obj->getReferenceDate());
        ArrayHelper::pushNonNull($advancedMap, 'referer', $obj->getReferrer());
        ArrayHelper::pushNonNull($advancedMap, 'url', $obj->getUrl());

        return $advancedMap;
    }
}
