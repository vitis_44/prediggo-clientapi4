<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\PageContent;

use Prediggo\ClientApi4\Service\Json\ArrayHelper;
use Prediggo\ClientApi4\Types\PageContent\AdBlockParam;
use Prediggo\ClientApi4\Types\PageContent\RecoBlockParam;
use Prediggo\ClientApi4\Types\PageContent\SearchBlock;
use Prediggo\ClientApi4\Types\PageContent\SimplePageContentParam;

class SimpleParamConverter {

    public function toMap($obj) {

        /** @var SimplePageContentParam $obj */
        $advancedMap = [];
        ArrayHelper::pushNonNull($advancedMap, 'zone', $obj->getAdvanced()->getZone());
        ArrayHelper::pushNonNull($advancedMap, 'offer', $obj->getAdvanced()->getOffer());
        ArrayHelper::pushNonNull($advancedMap, 'store', $obj->getAdvanced()->getStore());
        ArrayHelper::pushNonNull($advancedMap, 'customerId', $obj->getAdvanced()->getCustomerId());
        ArrayHelper::pushNonNull($advancedMap, 'device', $obj->getAdvanced()->getDevice());
        ArrayHelper::pushNonNull($advancedMap, 'referenceDate', $obj->getAdvanced()->getReferenceDate());
        ArrayHelper::pushNonNull($advancedMap, 'referer', $obj->getAdvanced()->getReferrer());
        ArrayHelper::pushNonNull($advancedMap, 'url', $obj->getAdvanced()->getUrl());

        $parameterMap = [];
        ArrayHelper::pushNonNull($parameterMap, 'page', $obj->getParameters()->getPage());
        ArrayHelper::pushNonNull($parameterMap, 'query', $obj->getParameters()->getQuery());
        ArrayHelper::pushNonNull($parameterMap, 'refiningId', $obj->getParameters()->getRefiningId());
        ArrayHelper::pushNonNull($parameterMap, 'resultsPerPage', $obj->getParameters()->getResultsPerPage());
        ArrayHelper::pushNonNull($parameterMap, 'sortingCode', $obj->getParameters()->getSortingCode());
        ArrayHelper::pushNonNull($parameterMap, 'skus', $obj->getParameters()->getSkus());
        ArrayHelper::pushNonNull($parameterMap, 'mode', $obj->getParameters()->getMode());
        ArrayHelper::pushNonNull($parameterMap, 'semanticSearchWeight', $obj->getParameters()->getSemanticSearchWeight());
        ArrayHelper::pushNonEmpty($parameterMap, 'filters', $obj->getParameters()->getFilters());

        $jsonMap = [
            'moduleVersion' => $obj->getModuleVersion(),
            'pageId' => $obj->getPageId(),
            'sessionId' => $obj->getSessionId(),
            'languageCode' => $obj->getLanguageCode(),
            'region' => $obj->getRegion()
        ];

        if (count($advancedMap) > 0) {
            $jsonMap['advanced'] = $advancedMap;
        }

        if (count($parameterMap) > 0) {
            $jsonMap['parameters'] = $parameterMap;
        }

        return $jsonMap;
    }

    /**
     * @param RecoBlockParam[] $blockParams
     * @return array
     */
    public function convertRecoBlocks($blockParams) {

        $recoBlocks = array();

        foreach ($blockParams as $block) {
            /** @var RecoBlockParam $block */
            $recoMap = [
                'blockId' => $block->getBlockId(),
                'skus' => $block->getSkus(),
            ];

            ArrayHelper::pushNonEmpty($recoMap, 'filters', $block->getFilters());
            $recoBlocks[] = $recoMap;
        }

        return $recoBlocks;
    }


    /**
     * @param SearchBlock[] $blockParams
     * @return array
     */
    public function convertSearchBlocks($blockParams) {

        $searchBlocks = array();

        /** @var SearchBlock $block */
        foreach ($blockParams as $block) {

            $searchMap = [
                'blockId' => $block->getBlockId()
            ];

            ArrayHelper::pushNonEmpty($searchMap, 'filters', $block->getFilters());

            ArrayHelper::pushNonNull($searchMap, 'query', $block->getQuery());
            ArrayHelper::pushNonNull($searchMap, 'refiningId', $block->getRefiningId());

            $settingsMap = array();

            ArrayHelper::pushNonNull($settingsMap, 'page', $block->getSettings()->getPage());
            ArrayHelper::pushNonNull($settingsMap, 'resultsPerPage', $block->getSettings()->getResultsPerPage());
            ArrayHelper::pushNonNull($settingsMap, 'sortingCode', $block->getSettings()->getSortingCode());
            ArrayHelper::pushNonNull($settingsMap, 'semanticSearchWeight', $block->getSettings()->getSemanticSearchWeight());

            ArrayHelper::pushNonEmpty($searchMap, 'settings', $settingsMap);
            $searchBlocks[] = $searchMap;
        }

        return $searchBlocks;
    }

    /**
     * @param AdBlockParam[] $blockParams
     * @return array
     */
    public function convertAdBlocks($blockParams) {

        $adBlocks = array();

        foreach ($blockParams as $block) {

            /** @var AdBlockParam $block */
            $adMap = [
                'blockId' => $block->getBlockId(),
            ];

            ArrayHelper::pushNonEmpty($adMap, 'filters', $block->getFilters());
            $adBlocks[] = $adMap;
        }
        return $adBlocks;
    }
}
