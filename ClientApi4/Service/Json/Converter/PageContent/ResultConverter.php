<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\PageContent;

use Prediggo\ClientApi4\Service\Json\ArrayHelper;
use Prediggo\ClientApi4\Service\Json\Converter\Item\ResultConverter as ItemResultConverter;
use Prediggo\ClientApi4\Types\Ad\AdInfo;
use Prediggo\ClientApi4\Types\PageContent\AdBlockResult;
use Prediggo\ClientApi4\Types\PageContent\AdSlot;
use Prediggo\ClientApi4\Types\PageContent\AdStatistics;
use Prediggo\ClientApi4\Types\PageContent\AdvancedResult;
use Prediggo\ClientApi4\Types\PageContent\AtmosphereComponent;
use Prediggo\ClientApi4\Types\PageContent\BannerComponent;
use Prediggo\ClientApi4\Types\PageContent\BlockContainer;
use Prediggo\ClientApi4\Types\PageContent\FacetGroup;
use Prediggo\ClientApi4\Types\PageContent\FacetOption;
use Prediggo\ClientApi4\Types\PageContent\FrontResult;
use Prediggo\ClientApi4\Types\PageContent\ItemsDistributionEntry;
use Prediggo\ClientApi4\Types\PageContent\ItemTypesDistribution;
use Prediggo\ClientApi4\Types\PageContent\PageContentResult;
use Prediggo\ClientApi4\Types\PageContent\PageOption;
use Prediggo\ClientApi4\Types\PageContent\RecoBlockResult;
use Prediggo\ClientApi4\Types\PageContent\RecoSlot;
use Prediggo\ClientApi4\Types\PageContent\RecoStatistics;
use Prediggo\ClientApi4\Types\PageContent\RedirectionComponent;
use Prediggo\ClientApi4\Types\PageContent\RuleInfo;
use Prediggo\ClientApi4\Types\PageContent\SearchBlockResult;
use Prediggo\ClientApi4\Types\PageContent\SearchRuleInfo;
use Prediggo\ClientApi4\Types\PageContent\SearchSlot;
use Prediggo\ClientApi4\Types\PageContent\SearchStatistics;
use Prediggo\ClientApi4\Types\PageContent\SortingOption;
use Prediggo\ClientApi4\Types\PageContent\Stats;

class ResultConverter {

    private $itemConverter;

    public function __construct() {
        $this->itemConverter = new ItemResultConverter();
    }

    public function toObject($jsonMap) {

        $obj = new PageContentResult();

        $obj->setTimeInMs(ArrayHelper::extractSafely($jsonMap, 'timeInMs', 0.0));
        $obj->setSessionId(ArrayHelper::extractSafely($jsonMap, 'sessionId', ''));
        $obj->setRegion(ArrayHelper::extractSafely($jsonMap, 'region', ''));
        $obj->setPageId(ArrayHelper::extractSafely($jsonMap, 'pageId', ''));
        $obj->setPageName(ArrayHelper::extractSafely($jsonMap, 'pageName', ''));
        $obj->setCachingStatsKey(ArrayHelper::extractSafely($jsonMap, 'cachingStatsKey', ''));
        $obj->setServerNodeId(ArrayHelper::extractSafely($jsonMap, 'serverNodeId', ''));
        $obj->setLanguageCode(ArrayHelper::extractSafely($jsonMap, 'languageCode', ''));
        $obj->setZone(ArrayHelper::extractSafely($jsonMap, 'zone', ''));
        $obj->setBlocks($this->convertBlocks(ArrayHelper::extractSafely($jsonMap, 'blocks', array())));
        $obj->setItems($this->convertItemsMap(ArrayHelper::extractSafely($jsonMap, 'items', array())));

        $this->fillAdvancedStructure(ArrayHelper::extractSafely($jsonMap, 'advanced', array()), $obj->getAdvanced());

        return $obj;
    }

    private function convertBlocks($blockCollectionNode) {

        $container = new BlockContainer();
        $recoBlocks = array();

        foreach (ArrayHelper::extractSafely($blockCollectionNode, 'recommendations', []) as $recoBlockNode) {
            $recoBlocks[] = $this->convertRecoBlockNode($recoBlockNode);
        }

        $container->setRecommendations($recoBlocks);


        $searchBlocks = array();

        foreach (ArrayHelper::extractSafely($blockCollectionNode, 'searches', []) as $searchBlockNode) {
            $searchBlocks[] = $this->convertSearchBlockNode($searchBlockNode);
        }

        $container->setSearches($searchBlocks);


        $adBlocks = array();

        foreach (ArrayHelper::extractSafely($blockCollectionNode, 'ads', []) as $adBlockNode) {
            $adBlocks[] = $this->convertAdBlockNode($adBlockNode);
        }

        $container->setAds($adBlocks);


        return $container;
    }

    public function convertRecoBlockNode($recoBlockNode) {

        $recoBlockResult = new RecoBlockResult();

        $recoBlockResult->setBlockId(ArrayHelper::extractSafely($recoBlockNode, 'blockId', ''));
        $recoBlockResult->setBlockName(ArrayHelper::extractSafely($recoBlockNode, 'blockName', ''));
        $recoBlockResult->setBlockType(ArrayHelper::extractSafely($recoBlockNode, 'blockType', ''));
        $recoBlockResult->setBlockFilters(ArrayHelper::extractSafely($recoBlockNode, 'blockFilters', ''));
        $recoBlockResult->setSlots($this->convertRecoSlots(ArrayHelper::extractSafely($recoBlockNode, 'slots', array())));
        $recoBlockResult->setStats($this->convertRecoStats(ArrayHelper::extractSafely($recoBlockNode, 'stats', array())));
        if (array_key_exists('front', $recoBlockNode)) {
            $recoBlockResult->setFront($this->convertFront(ArrayHelper::extractSafely($recoBlockNode, 'front', '')));
        }

        return $recoBlockResult;
    }

    public function convertSearchBlockNode($searchBlockNode) {

        $searchBlockResult = new SearchBlockResult();

        $searchBlockResult->setBlockId(ArrayHelper::extractSafely($searchBlockNode, 'blockId', ''));
        $searchBlockResult->setBlockName(ArrayHelper::extractSafely($searchBlockNode, 'blockName', ''));
        $searchBlockResult->setBlockType(ArrayHelper::extractSafely($searchBlockNode, 'blockType', ''));
        $searchBlockResult->setBlockFilters(ArrayHelper::extractSafely($searchBlockNode, 'blockFilters', ''));
        $searchBlockResult->setDidYouMean(ArrayHelper::extractSafely($searchBlockNode, 'didYouMean', array()));

        $searchBlockResult->setBacktracks($this->convertFacetGroups(ArrayHelper::extractSafely($searchBlockNode, 'backtracks', array())));
        $searchBlockResult->setFacets($this->convertFacetGroups(ArrayHelper::extractSafely($searchBlockNode, 'facets', array())));
        $searchBlockResult->setPages($this->convertSearchPages(ArrayHelper::extractSafely($searchBlockNode, 'pages', array())));
        $searchBlockResult->setSlots($this->convertSearchSlots(ArrayHelper::extractSafely($searchBlockNode, 'slots', array())));
        $searchBlockResult->setSortings($this->convertSortings(ArrayHelper::extractSafely($searchBlockNode, 'sortings', array())));

        if (array_key_exists('stats', $searchBlockNode)) {
            $searchBlockResult->setStats($this->convertSearchStats($searchBlockNode['stats']));
        }

        if (array_key_exists('banner', $searchBlockNode)) {
            $searchBlockResult->setBanner($this->convertSearchBanner($searchBlockNode['banner']));
        }

        if (array_key_exists('atmosphere', $searchBlockNode)) {
            $searchBlockResult->setAtmosphere($this->convertSearchAtmosphere($searchBlockNode['atmosphere']));
        }

        if (array_key_exists('redirection', $searchBlockNode)) {
            $searchBlockResult->setRedirection($this->convertSearchRedirection($searchBlockNode['redirection']));
        }

        if (array_key_exists('front', $searchBlockNode)) {
            $searchBlockResult->setFront($this->convertFront($searchBlockNode['front']));
        }

        return $searchBlockResult;
    }

    public function convertAdBlockNode($adBlockNode) {

        $adBlockResult = new AdBlockResult();

        $adBlockResult->setBlockId(ArrayHelper::extractSafely($adBlockNode, 'blockId', ''));
        $adBlockResult->setBlockName(ArrayHelper::extractSafely($adBlockNode, 'blockName', ''));
        $adBlockResult->setBlockType(ArrayHelper::extractSafely($adBlockNode, 'blockType', ''));
        $adBlockResult->setBlockFilters(ArrayHelper::extractSafely($adBlockNode, 'blockFilters', ''));

        if (array_key_exists('stats', $adBlockNode)) {
            $adBlockResult->setStats($this->convertAdStats($adBlockNode['stats']));
        }

        $adBlockResult->setSlots($this->convertAdSlots(ArrayHelper::extractSafely($adBlockNode, 'slots', array())));

        return $adBlockResult;
    }

    private function convertRecoSlots($slotCollectionNode) {

        $list = array();

        foreach ($slotCollectionNode as $slotNode) {

            $obj = new RecoSlot();
            $obj->setPosition(ArrayHelper::extractSafely($slotNode, 'position', 0));

            if (array_key_exists('rule', $slotNode)) {
                $obj->setRule($this->convertRuleInfo($slotNode['rule']));
            }

            if (array_key_exists('item', $slotNode)) {
                $obj->setItem($this->itemConverter->toObject($slotNode['item']));
            }

            $list[] = $obj;
        }

        return $list;
    }

    /**
     * @param RecoStatistics $stat
     * @return RecoStatistics
     */
    private function convertRecoStats($stat) {
        $obj = new RecoStatistics();

        $obj->setTotalResults(ArrayHelper::extractSafely($stat, 'totalResults', 0));
        $obj->setComputationInMs(ArrayHelper::extractSafely($stat, 'computationInMs', 0));
        $obj->setUserLabel(ArrayHelper::extractSafely($stat, 'userLabel', ''));
        $obj->setScenarioTemplate(ArrayHelper::extractSafely($stat, 'scenarioTemplate', ''));
        /** @var array $stat */
        if (array_key_exists('itemTypesDistribution', $stat)) {
            $obj->setItemTypesDistribution($this->convertItemTypesDistribution(ArrayHelper::extractSafely($stat, 'itemTypesDistribution', '')));
        }
        $obj->setCampaignName(ArrayHelper::extractSafely($stat, 'campaignName', ''));
        $obj->setCampaignLabel(ArrayHelper::extractSafely($stat, 'campaignLabel', ''));
        $obj->setNbSlotsX(ArrayHelper::extractSafely($stat, 'nbSlotsX', ''));
        $obj->setNbSlotsY(ArrayHelper::extractSafely($stat, 'nbSlotsY', ''));

        return $obj;
    }

    private function convertRuleInfo($ruleNode) {

        $obj = new RuleInfo();

        $obj->setRuleId(ArrayHelper::extractSafely($ruleNode, 'ruleId', ''));
        $obj->setRuleName(ArrayHelper::extractSafely($ruleNode, 'ruleName', ''));
        $obj->setRuleType(ArrayHelper::extractSafely($ruleNode, 'ruleType', ''));
        $obj->setStartDate(ArrayHelper::extractSafely($ruleNode, 'startDate', ''));
        $obj->setEndDate(ArrayHelper::extractSafely($ruleNode, 'endDate', ''));
        $obj->setSourceItemId(ArrayHelper::extractSafely($ruleNode, 'sourceItemId', ''));
        $obj->setRuleTemplateId(ArrayHelper::extractSafely($ruleNode, 'ruleTemplateId', ''));
        $obj->setRuleTemplateName(ArrayHelper::extractSafely($ruleNode, 'ruleTemplateName', ''));
        $obj->setRuleTemplateLabel(ArrayHelper::extractSafely($ruleNode, 'ruleTemplateLabel', ''));
        $obj->setInitialPosition(ArrayHelper::extractSafely($ruleNode, 'initialPosition', ''));
        $obj->setItemLabel(ArrayHelper::extractSafely($ruleNode, 'itemLabel', ''));
        $obj->setUserLabel(ArrayHelper::extractSafely($ruleNode, 'userLabel', ''));
        $obj->setScenarioName(ArrayHelper::extractSafely($ruleNode, 'scenarioName', ''));
        $obj->setStylesheetUrl(ArrayHelper::extractSafely($ruleNode, 'stylesheetUrl', ''));
        $obj->setComputationTimeInMs(ArrayHelper::extractSafelyIgnoreCase($ruleNode, 'computationTimeInMS', 0.0));
        if (array_key_exists('front', $ruleNode)) {
            $obj->setFront($this->convertFront(ArrayHelper::extractSafely($ruleNode, 'front', '')));
        }

        return $obj;
    }

    private function convertFacetGroups($facetCollectionNode) {

        $list = array();

        foreach ($facetCollectionNode as $facetNode) {

            $obj = new FacetGroup();
            $obj->setAttributeName(ArrayHelper::extractSafely($facetNode, 'attributeName', ''));
            $obj->setAttributeLabel(ArrayHelper::extractSafely($facetNode, 'attributeLabel', ''));
            $obj->setAttributeType(ArrayHelper::extractSafely($facetNode, 'attributeType', ''));
            $obj->setAttributeUnit(ArrayHelper::extractSafely($facetNode, 'attributeUnit', ''));
            $obj->setMultiSelect(ArrayHelper::extractSafely($facetNode, 'multiSelect', false));
            $obj->setPosition(ArrayHelper::extractSafely($facetNode, 'position', 0));
            $obj->setWidgetType(ArrayHelper::extractSafely($facetNode, 'widgetType'));
            $obj->setWidgetPosition(ArrayHelper::extractSafely($facetNode, 'widgetPosition'));
            $obj->setWidgetVisibility(ArrayHelper::extractSafely($facetNode, 'widgetVisibility'));
            $obj->setRobotMetadata(ArrayHelper::extractSafely($facetNode, 'robotMetadata'));
            $obj->setAbsoluteMin(ArrayHelper::extractSafely($facetNode, 'absoluteMin'));
            $obj->setAbsoluteMax(ArrayHelper::extractSafely($facetNode, 'absoluteMax'));
            $obj->setItemsDistribution($this->convertItemsDistributionEntry(ArrayHelper::extractSafely($facetNode, 'itemsDistribution', [])));
            $obj->setOptions($this->convertFacetOptions(ArrayHelper::extractSafely($facetNode, 'options', [])));

            $list[] = $obj;
        }

        return $list;
    }

    private function convertItemsDistributionEntry($itemsDistributionList) {

        $list = array();

        foreach ($itemsDistributionList as $itemsDistribution) {

            $obj = new ItemsDistributionEntry();
            $obj->setMin(ArrayHelper::extractSafely($itemsDistribution, 'min', ''));
            $obj->setMax(ArrayHelper::extractSafely($itemsDistribution, 'max', ''));
            $obj->setCount(ArrayHelper::extractSafely($itemsDistribution, 'count', ''));

            $list[] = $obj;
        }

        return $list;
    }

    private function convertFacetOptions($optionCollectionNode) {

        $list = array();

        foreach ($optionCollectionNode as $facetValueNode) {

            $obj = new FacetOption();
            $obj->setValue(ArrayHelper::extractSafely($facetValueNode, 'value', ''));
            $obj->setLabel(ArrayHelper::extractSafely($facetValueNode, 'label', ''));
            $obj->setMin(ArrayHelper::extractSafely($facetValueNode, 'min', ''));
            $obj->setMax(ArrayHelper::extractSafely($facetValueNode, 'max', ''));
            $obj->setNbResults(ArrayHelper::extractSafely($facetValueNode, 'nbResults', 0));
            $obj->setSelected(ArrayHelper::extractSafely($facetValueNode, 'selected', false));
            $obj->setNoFollow(ArrayHelper::extractSafely($facetValueNode, 'noFollow', false));
            $obj->setRefiningId(ArrayHelper::extractSafely($facetValueNode, 'refiningId', ''));
            $obj->setDebugText(ArrayHelper::extractSafely($facetValueNode, 'debugText', ''));
            $obj->setProperties(ArrayHelper::extractSafely($facetValueNode, 'properties', ''));

            $list[] = $obj;
        }

        return $list;
    }

    private function convertSearchPages($pageCollectionNode) {

        $list = array();

        foreach ($pageCollectionNode as $pageNode) {

            $obj = new PageOption();
            $obj->setPageNumber(ArrayHelper::extractSafely($pageNode, 'pageNumber', 0));
            $obj->setLabel(ArrayHelper::extractSafely($pageNode, 'label', ''));
            $obj->setRefiningId(ArrayHelper::extractSafely($pageNode, 'refiningId', ''));
            $obj->setNoFollow(ArrayHelper::extractSafely($pageNode, 'noFollow', false));

            $list[] = $obj;
        }

        return $list;
    }

    private function convertSearchSlots($slotCollectionNode) {
        $list = array();

        foreach ($slotCollectionNode as $slotNode) {

            $obj = new SearchSlot();
            $obj->setPosition(ArrayHelper::extractSafely($slotNode, 'position', 0));

            if (array_key_exists('item', $slotNode)) {
                $obj->setItem($this->itemConverter->toObject($slotNode['item']));
            }

            if (array_key_exists('rule', $slotNode)) {
                $obj->setRule($this->convertSearchRuleInfo($slotNode['rule']));
            }

            $list[] = $obj;
        }

        return $list;
    }

    private function convertSearchRuleInfo($ruleNode) {

        $obj = new SearchRuleInfo();

        $obj->setRuleTemplateName(ArrayHelper::extractSafely($ruleNode, 'ruleTemplateName', ''));
        $obj->setRuleTemplateLabel(ArrayHelper::extractSafely($ruleNode, 'ruleTemplateLabel', ''));
        $obj->setRuleType(ArrayHelper::extractSafely($ruleNode, 'ruleType', ''));
        $obj->setStartDate(ArrayHelper::extractSafely($ruleNode, 'startDate', ''));
        $obj->setEndDate(ArrayHelper::extractSafely($ruleNode, 'endDate', ''));
        $obj->setInitialPosition(ArrayHelper::extractSafely($ruleNode, 'initialPosition', ''));
        $obj->setItemLabel(ArrayHelper::extractSafely($ruleNode, 'itemLabel', ''));
        $obj->setUserLabel(ArrayHelper::extractSafely($ruleNode, 'userLabel', ''));
        $obj->setStylesheetUrl(ArrayHelper::extractSafely($ruleNode, 'stylesheetUrl', ''));
        $obj->setComputationTimeInMs(ArrayHelper::extractSafelyIgnoreCase($ruleNode, 'computationTimeInMs', 0.0));
        if (array_key_exists('front', $ruleNode)) {
            $obj->setFront($this->convertFront(ArrayHelper::extractSafely($ruleNode, 'front', '')));
        }

        return $obj;
    }

    private function convertSortings($sortingCollectionNode) {

        $list = array();

        foreach ($sortingCollectionNode as $sortingNode) {

            $obj = new SortingOption();
            $obj->setSortingCode(ArrayHelper::extractSafely($sortingNode, 'sortingCode', ''));
            $obj->setRefiningId(ArrayHelper::extractSafely($sortingNode, 'refiningId', ''));
            $obj->setNoFollow(ArrayHelper::extractSafely($sortingNode, 'noFollow', false));
            $obj->setSelected(ArrayHelper::extractSafely($sortingNode, 'selected', false));
            $obj->setLabel(ArrayHelper::extractSafely($sortingNode, 'label', ''));

            $list[] = $obj;
        }

        return $list;
    }

    private function convertSearchStats($statsNode) {

        $obj = new SearchStatistics();

        $obj->setComputationInMs(ArrayHelper::extractSafely($statsNode, 'computationInMs', 0.0));
        $obj->setCurrentPage(ArrayHelper::extractSafely($statsNode, 'currentPage', 0));
        $obj->setResultsInPage(ArrayHelper::extractSafely($statsNode, 'resultsInPage', 0));
        $obj->setResultsPerPage(ArrayHelper::extractSafely($statsNode, 'resultsPerPage', 0));
        $obj->setSortingCode(ArrayHelper::extractSafely($statsNode, 'sortingCode', ''));
        $obj->setTotalPages(ArrayHelper::extractSafely($statsNode, 'totalPages', 0));
        $obj->setTotalResults(ArrayHelper::extractSafely($statsNode, 'totalResults', 0));
        $obj->setGalaxyId(ArrayHelper::extractSafely($statsNode, 'galaxyId', ''));
        $obj->setUserLabel(ArrayHelper::extractSafely($statsNode, 'userLabel', ''));

        $obj->setQuery(ArrayHelper::extractSafely($statsNode, 'query', ''));
        $obj->setUpdatedQuery(ArrayHelper::extractSafely($statsNode, 'updatedQuery', ''));
        $obj->setRuleTemplate(ArrayHelper::extractSafely($statsNode, 'ruleTemplate', ''));
        $obj->setSearchId(ArrayHelper::extractSafely($statsNode, 'searchId', 0));
        if (array_key_exists('itemTypesDistribution', $statsNode)) {
            $obj->setItemTypesDistribution($this->convertItemTypesDistribution(ArrayHelper::extractSafely($statsNode, 'itemTypesDistribution', '')));
        }
        $obj->setCampaignName(ArrayHelper::extractSafely($statsNode, 'campaignName', ''));
        $obj->setCampaignLabel(ArrayHelper::extractSafely($statsNode, 'campaignLabel', ''));
        $obj->setNbSlotsX(ArrayHelper::extractSafely($statsNode, 'nbSlotsX', ''));
        $obj->setNbSlotsY(ArrayHelper::extractSafely($statsNode, 'nbSlotsY', ''));
        $obj->setIgnoredSkus(ArrayHelper::extractSafely($statsNode, 'ignoredSkus', ''));

        return $obj;
    }

    private function convertSearchBanner($bannerNode) {

        $obj = new BannerComponent();

        $obj->setPictureUrl(ArrayHelper::extractSafely($bannerNode, 'pictureUrl', ''));
        $obj->setClickUrl(ArrayHelper::extractSafely($bannerNode, 'clickUrl', ''));
        $obj->setTextToUser(ArrayHelper::extractSafely($bannerNode, 'textToUser', ''));

        return $obj;
    }

    private function convertSearchAtmosphere($atmoNode) {

        $obj = new AtmosphereComponent();

        $obj->setOptionalText(ArrayHelper::extractSafely($atmoNode, 'optionalText', ''));
        $obj->setBlockTemplate(ArrayHelper::extractSafely($atmoNode, 'blockTemplate', ''));
        $obj->setCssTemplate(ArrayHelper::extractSafely($atmoNode, 'cssTemplate', ''));

        return $obj;
    }

    private function convertSearchRedirection($redirectionNode) {

        $obj = new RedirectionComponent();

        if (isset($redirectionNode['categoryRedirection']) && $redirectionNode['categoryRedirection'] === true) {
            return null;
        }

        $obj->setRedirectUrl(ArrayHelper::extractSafely($redirectionNode, 'redirectUrl', ''));

        return $obj;
    }

    /**
     * @param FrontResult $front
     * @return FrontResult
     */
    private function convertFront($front) {

        $obj = new FrontResult();
        $obj->setLabel(ArrayHelper::extractSafely($front, 'label', ''));
        $obj->setExtraLabels(ArrayHelper::extractSafely($front, 'extraLabels', []));

        return $obj;
    }

    private function convertAdStats($statsNode) {

        $obj = new AdStatistics();

        $obj->setComputationInMs(ArrayHelper::extractSafely($statsNode, 'computationInMs', 0.0));
        $obj->setTotalResults(ArrayHelper::extractSafely($statsNode, 'totalResults', 0));
        $obj->setUserLabel(ArrayHelper::extractSafely($statsNode, 'userLabel', ''));
        if (array_key_exists('itemTypesDistribution', $statsNode)) {
            $obj->setItemTypesDistribution($this->convertItemTypesDistribution(ArrayHelper::extractSafely($statsNode, 'itemTypesDistribution', '')));
        }

        return $obj;
    }

    private function convertAdSlots($slotCollectionNode) {

        $list = array();

        foreach ($slotCollectionNode as $slotNode) {

            $obj = new AdSlot();
            $obj->setPosition(ArrayHelper::extractSafely($slotNode, 'position', 0));

            if (array_key_exists('ad', $slotNode)) {
                $obj->setBanner($this->convertAdInfo($slotNode['ad']));
            }

            $obj->setDebugInfo(ArrayHelper::extractSafely($slotNode, 'debugInfo', []));
            $list[] = $obj;
        }

        return $list;
    }

    private function convertAdInfo($adNode) {

        $ad = new AdInfo();
        $ad->setLabel(ArrayHelper::extractSafely($adNode, 'label', ''));
        $ad->setMediaUrl(ArrayHelper::extractSafely($adNode, 'mediaUrl', ''));
        $ad->setClickUrl(ArrayHelper::extractSafely($adNode, 'clickUrl', ''));
        $ad->setMediaType(ArrayHelper::extractSafely($adNode, 'mediaType', ''));
        $ad->setOpenMethod(ArrayHelper::extractSafely($adNode, 'openMethod', ''));
        $ad->setTrackingCode(ArrayHelper::extractSafely($adNode, 'trackingCode', ''));

        return $ad;
    }

    private function convertItemsMap($itemsNode) {

        $itemMap = array();

        foreach ($itemsNode as $sku => $itemNode) {
            $itemMap[$sku] = $this->itemConverter->toObject($itemNode);
        }

        return $itemMap;
    }

    private function convertItemTypesDistribution($itemTypesDistribution) {

        $obj = new ItemTypesDistribution();
        if (array_key_exists('totalResults', $itemTypesDistribution)) {
            $obj->setTotalResults(ArrayHelper::extractSafely($itemTypesDistribution, 'totalResults', []));
        }

        if (array_key_exists('resultsInPage', $itemTypesDistribution)) {
            $obj->setResultsInPage(ArrayHelper::extractSafely($itemTypesDistribution, 'resultsInPage', []));
        }

        return $obj;
    }

    /**
     * @param array $advancedNode
     * @param AdvancedResult $advancedObj
     */
    public function fillAdvancedStructure($advancedNode, $advancedObj) {

        $advancedObj->setCustomerId(ArrayHelper::extractSafely($advancedNode, 'customerId', ''));
        $advancedObj->setDevice(ArrayHelper::extractSafely($advancedNode, 'device', ''));
        $advancedObj->setReferenceDate(ArrayHelper::extractSafely($advancedNode, 'referenceDate', ''));
        $advancedObj->setReferrer(ArrayHelper::extractSafely($advancedNode, 'referer', ''));
        $advancedObj->setOffer(ArrayHelper::extractSafely($advancedNode, 'offer', ''));
        $advancedObj->setStore(ArrayHelper::extractSafely($advancedNode, 'store', ''));
        $advancedObj->setUrl(ArrayHelper::extractSafely($advancedNode, 'url', ''));
    }
}
