<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\PageContent;

use Prediggo\ClientApi4\Service\Json\ArrayHelper;
use Prediggo\ClientApi4\Types\PageContent\AdBlockParam;
use Prediggo\ClientApi4\Types\PageContent\PageContentParam;
use Prediggo\ClientApi4\Types\PageContent\RecoBlockParam;
use Prediggo\ClientApi4\Types\PageContent\SearchBlock;

class ParamConverter {

    private $advancedParamConverter;

    public function __construct() {
        $this->advancedParamConverter = new AdvancedParamConverter();
    }

    /** @var PageContentParam $obj */
    public function toMap($obj) {

        $advancedMap = $this->advancedParamConverter->toMap($obj->getAdvanced());

        $jsonMap = [
            'moduleVersion' => $obj->getModuleVersion(),
            'pageId' => $obj->getPageId(),
            'sessionId' => $obj->getSessionId(),
            'languageCode' => $obj->getLanguageCode(),
            'region' => $obj->getRegion()
        ];

        if (count($advancedMap) > 0) {
            $jsonMap['advanced'] = $advancedMap;
        }

        $recoBlocks = $this->convertRecoBlocks($obj->getBlocks()->getRecommendations());
        ArrayHelper::pushNonEmpty($blocks, 'recommendations', $recoBlocks);

        $searchBlocks = $this->convertSearchBlocks($obj->getBlocks()->getSearches());
        ArrayHelper::pushNonEmpty($blocks, 'searches', $searchBlocks);

        $adBlocks = $this->convertAdBlocks($obj->getBlocks()->getAds());
        ArrayHelper::pushNonEmpty($blocks, 'ads', $adBlocks);

        $jsonMap['blocks'] = $blocks;

        return $jsonMap;
    }

    /**
     * @param RecoBlockParam[] $blockParams
     * @return array
     */
    public function convertRecoBlocks($blockParams) {

        $recoBlocks = array();

        foreach ($blockParams as $block) {
            /** @var RecoBlockParam $block */
            $recoMap = [
                'blockId' => $block->getBlockId(),
                'skus' => $block->getSkus(),
            ];

            ArrayHelper::pushNonEmpty($recoMap, 'filters', $block->getFilters());
            $recoBlocks[] = $recoMap;
        }

        return $recoBlocks;
    }


    /**
     * @param SearchBlock[] $blockParams
     * @return array
     */
    public function convertSearchBlocks($blockParams) {

        $searchBlocks = array();

        /** @var SearchBlock $block */
        foreach ($blockParams as $block) {

            $searchMap = [
                'blockId' => $block->getBlockId()
            ];

            ArrayHelper::pushNonEmpty($searchMap, 'filters', $block->getFilters());

            ArrayHelper::pushNonNull($searchMap, 'query', $block->getQuery());
            ArrayHelper::pushNonNull($searchMap, 'refiningId', $block->getRefiningId());

            $settingsMap = array();

            ArrayHelper::pushNonNull($settingsMap, 'page', $block->getSettings()->getPage());
            ArrayHelper::pushNonNull($settingsMap, 'resultsPerPage', $block->getSettings()->getResultsPerPage());
            ArrayHelper::pushNonNull($settingsMap, 'sortingCode', $block->getSettings()->getSortingCode());
            ArrayHelper::pushNonNull($settingsMap, 'semanticSearchWeight', $block->getSettings()->getSemanticSearchWeight());

            ArrayHelper::pushNonEmpty($searchMap, 'settings', $settingsMap);
            $searchBlocks[] = $searchMap;
        }

        return $searchBlocks;
    }

    /**
     * @param AdBlockParam[] $blockParams
     * @return array
     */
    public function convertAdBlocks($blockParams) {

        $adBlocks = array();

        foreach ($blockParams as $block) {

            /** @var AdBlockParam $block */
            $adMap = [
                'blockId' => $block->getBlockId(),
            ];

            ArrayHelper::pushNonEmpty($adMap, 'filters', $block->getFilters());
            $adBlocks[] = $adMap;
        }
        return $adBlocks;
    }
}
