<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\UpdateUsers;


use Prediggo\ClientApi4\Types\UpdateUsers\UpdatedUserParam;
use Prediggo\ClientApi4\Types\UpdateUsers\UpdateUserParam;

class ParamConverter {

    public function toMap($obj) {

        /** @var UpdateUserParam $obj */
        $jsonMap = [
            'moduleVersion' => $obj->getModuleVersion(),
            'action' => $obj->getAction()
        ];

        $itemsParam = $this->convertUsers($obj->getUsers());

        $jsonMap['users'] = $itemsParam;

        return $jsonMap;
    }

    /**
     * @param UpdatedUserParam[] $usersParam
     * @return array
     */
    public function convertUsers($usersParam) {

        $users = array();

        foreach ($usersParam as $user) {

            /** @var UpdatedUserParam $block */
            $userMap = [
                'sessionId' => $user->getSessionId(),
                'customerId' => $user->getCustomerId(),
                'attributes' => $user->getAttributes()
            ];

            $users[] = $userMap;
        }

        return $users;
    }
}
