<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\UpdateUsers;

use Prediggo\ClientApi4\Service\Json\ArrayHelper;
use Prediggo\ClientApi4\Service\Json\Converter\Debug\ResultConverter as DebugResultConverter;
use Prediggo\ClientApi4\Types\Debug\DebugResult;
use Prediggo\ClientApi4\Types\UpdateUsers\RejectedUserResult;
use Prediggo\ClientApi4\Types\UpdateUsers\UpdatedUserResult;
use Prediggo\ClientApi4\Types\UpdateUsers\UpdateUserResult;

class ResultConverter {

    private $debugResultConverter;

    public function __construct()
    {
        $this->debugResultConverter = new DebugResultConverter();
    }

    public function toObject($jsonMap) {

        $obj = new UpdateUserResult();

        $obj->setStatus(ArrayHelper::extractSafely($jsonMap, 'status', ''));
        $obj->setTimeInMs(ArrayHelper::extractSafely($jsonMap, 'timeInMs', 0.0));
        $obj->setUpdatedItems($this->convertUpdatedUsers(ArrayHelper::extractSafely($jsonMap, 'updatedUsers', array())));
        $obj->setRejectedItems($this->convertRejectedUsers(ArrayHelper::extractSafely($jsonMap, 'rejectedUsers', array())));
        $obj->setDebug($this->convertDebugResult(ArrayHelper::extractSafely($jsonMap, 'debug', array())));

        return $obj;
    }

    /**
     * @param $debug
     * @return DebugResult
     */
    private function convertDebugResult($debug)
    {
        return $this->debugResultConverter->toObject($debug);
    }

    private function convertUpdatedUsers($updatedUsers) {

        $updatedItemList = array();

        foreach ($updatedUsers as $updatedUser) {

            $updatedUsers = new UpdatedUserResult();

            $updatedUsers->setSessionsId(ArrayHelper::extractSafely($updatedUser, 'sessionId', ''));
            $updatedUsers->setCustomerId(ArrayHelper::extractSafely($updatedUser, 'customerId', ''));

            $updatedItemList[] = $updatedUsers;
        }

        return $updatedItemList;
    }

    private function convertRejectedUsers($rejectedUsers) {

        $rejectedUserList = array();

        foreach ($rejectedUsers as $rejectedUser) {

            $rejectedUsers = new RejectedUserResult();

            $rejectedUsers->setSessionsId(ArrayHelper::extractSafely($rejectedUser, 'sessionId', ''));
            $rejectedUsers->setCustomerId(ArrayHelper::extractSafely($rejectedUser, 'customerId', ''));
            $rejectedUsers->setErrorMessage(ArrayHelper::extractSafely($rejectedUser, 'errorMessage', ''));

            $rejectedUserList[] = $rejectedUsers;
        }

        return $rejectedUserList;
    }
}