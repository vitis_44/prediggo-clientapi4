<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\ItemsInteraction;

use Prediggo\ClientApi4\Service\Json\ArrayHelper;
use Prediggo\ClientApi4\Types\ItemsInteraction\AdvancedResult;
use Prediggo\ClientApi4\Types\ItemsInteraction\ItemsInteractionResult;

class ResultConverter {

    public function toObject($jsonMap) {

        $obj = new ItemsInteractionResult();

        $obj->setTimeInMs(ArrayHelper::extractSafely($jsonMap, 'timeInMs', 0.0));
        $obj->setSessionId(ArrayHelper::extractSafely($jsonMap, 'sessionId', ''));
        $this->fillAdvancedStructure(ArrayHelper::extractSafely($jsonMap, 'advanced', array()), $obj->getAdvanced());

        return $obj;
    }

    /**
     * @param array $advancedNode
     * @param AdvancedResult $advancedObj
     */
    private function fillAdvancedStructure($advancedNode, $advancedObj) {

        $advancedObj->setCustomerId(ArrayHelper::extractSafely($advancedNode, 'customerId', ''));
        $advancedObj->setDevice(ArrayHelper::extractSafely($advancedNode, 'device', ''));
        $advancedObj->setReferrer(ArrayHelper::extractSafely($advancedNode, 'referer', ''));
        $advancedObj->setOffer(ArrayHelper::extractSafely($advancedNode, 'offer', ''));
        $advancedObj->setStore(ArrayHelper::extractSafely($advancedNode, 'store', ''));
        $advancedObj->setUrl(ArrayHelper::extractSafely($advancedNode, 'url', ''));
    }
}
