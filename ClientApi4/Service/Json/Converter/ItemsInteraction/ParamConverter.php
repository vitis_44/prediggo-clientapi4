<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\ItemsInteraction;

use Prediggo\ClientApi4\Service\Json\ArrayHelper;
use Prediggo\ClientApi4\Types\ItemsInteraction\BasketItem;
use Prediggo\ClientApi4\Types\ItemsInteraction\ItemsInteractionParam;

class ParamConverter {

    public function toMap($obj) {

        /** @var ItemsInteractionParam $obj */
        $advancedMap = [];
        ArrayHelper::pushNonNull($advancedMap, 'zone', $obj->getAdvanced()->getZone());
        ArrayHelper::pushNonNull($advancedMap, 'offer', $obj->getAdvanced()->getOffer());
        ArrayHelper::pushNonNull($advancedMap, 'store', $obj->getAdvanced()->getStore());
        ArrayHelper::pushNonNull($advancedMap, 'customerId', $obj->getAdvanced()->getCustomerId());
        ArrayHelper::pushNonNull($advancedMap, 'device', $obj->getAdvanced()->getDevice());
        ArrayHelper::pushNonNull($advancedMap, 'referer', $obj->getAdvanced()->getReferrer());
        ArrayHelper::pushNonNull($advancedMap, 'url', $obj->getAdvanced()->getUrl());

        $jsonMap = [
            'moduleVersion' => $obj->getModuleVersion(),
            'eventType' => $obj->getEventType(),
            'sessionId' => $obj->getSessionId(),
            'region' => $obj->getRegion(),
            'basket' => $this->itemsToMap($obj->getBasket())
        ];

        if (count($advancedMap) > 0) {
            $jsonMap['advanced'] = $advancedMap;
        }

        return $jsonMap;
    }

    public function itemsToMap($basketItems) {

        /** @var BasketItem[] $basketItems */
        $advancedMap = array();

        foreach ($basketItems as $item) {

            $itemMap = array();
            ArrayHelper::pushNonNull($itemMap, 'sku', $item->getSku());
            ArrayHelper::pushNonNull($itemMap, 'quantity', $item->getQuantity());

            $advancedMap[] = $itemMap;
        }

        return $advancedMap;
    }
}
