<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\RegisterUser;

use Prediggo\ClientApi4\Types\RegisterUser\RegisterUserParam;


class ParamConverter {

    public function toMap($obj) {

        /** @var RegisterUserParam $obj */
        $jsonMap = [
            'moduleVersion' => $obj->getModuleVersion(),
            'sessionId' => $obj->getSessionId(),
            'customerId' => $obj->getCustomerId()
        ];

        return $jsonMap;
    }
}
