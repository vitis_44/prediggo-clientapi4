<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\SubCategoryCount;

use Prediggo\ClientApi4\Service\Json\ArrayHelper;
use Prediggo\ClientApi4\Service\Json\Converter\Debug\ResultConverter as DebugResultConverter;
use Prediggo\ClientApi4\Types\SubCategoryCount\AdvancedResult;
use Prediggo\ClientApi4\Types\SubCategoryCount\CategoryNode;
use Prediggo\ClientApi4\Types\SubCategoryCount\SubCategoryCountResult;
use Prediggo\ClientApi4\Types\SubCategoryCount\SubCategoryCountSubNode;

class ResultConverter
{

    private $debugResultConverter;

    public function __construct()
    {
        $this->debugResultConverter = new DebugResultConverter();
    }

    public function toObject($jsonMap)
    {

        $obj = new SubCategoryCountResult();

        $obj->setStatus(ArrayHelper::extractSafely($jsonMap, 'status', ''));
        $obj->setTimeInMs(ArrayHelper::extractSafely($jsonMap, 'timeInMs', 0.0));
        $obj->setDebug($this->convertDebugResult(ArrayHelper::extractSafely($jsonMap, 'debug', array())));
        $obj->setSessionId(ArrayHelper::extractSafely($jsonMap, 'sessionId', ''));
        $obj->setRegion(ArrayHelper::extractSafely($jsonMap, 'region', ''));
        $this->fillAdvancedStructure(ArrayHelper::extractSafely($jsonMap, 'advanced', array()), $obj->getAdvanced());
        $obj->setAttributeName(ArrayHelper::extractSafely($jsonMap, 'attributeName', ''));
        $obj->setAttributeValue(ArrayHelper::extractSafely($jsonMap, 'attributeValue', ''));
        $obj->setNode($this->convertNode(ArrayHelper::extractSafely($jsonMap, 'node', array())));
        $obj->setIncludeParents(ArrayHelper::extractSafely($jsonMap, 'includeParents', ''));
        $obj->setSubNodes($this->convertSubCategoryCountSubNodes(ArrayHelper::extractSafely($jsonMap, 'subNodes', array())));
        $obj->setParentNodes($this->convertSubCategoryCountSubNodes(ArrayHelper::extractSafely($jsonMap, 'parentNodes', array())));

        return $obj;
    }

    private function convertDebugResult($debug)
    {
        return $this->debugResultConverter->toObject($debug);
    }

    /**
     * @param CategoryNode $node
     */
    private function convertNode($node)
    {
        $categoryNode = new CategoryNode();

        $this->convertGenericNode($categoryNode, $node);

        return $categoryNode;
    }

    /**
     * @param SubCategoryCountSubNode[] $nodes
     */
    private function convertSubCategoryCountSubNodes($nodes)
    {
        $categoryCountSubNodes = array();
        foreach ($nodes as $node) {
            $categoryCountSubNode = new SubCategoryCountSubNode();
            $this->convertGenericNode($categoryCountSubNode, $node);

            $categoryCountSubNode->setSearchableProducts(ArrayHelper::extractSafely($node, 'searchableProducts', ''));

            $categoryCountSubNodes[] = $categoryCountSubNode;
        }

        return $categoryCountSubNodes;
    }

    /**
     * @param CategoryNode|SubCategoryCountSubNode $genericNode
     * @param CategoryNode|SubCategoryCountSubNode $node
     */
    private function convertGenericNode($genericNode, $node)
    {
        $genericNode->setAttributeName(ArrayHelper::extractSafely($node, 'attributeName', ''));
        $genericNode->setAttributeType(ArrayHelper::extractSafely($node, 'attributeType', ''));
        $genericNode->setAttributeLabel(ArrayHelper::extractSafely($node, 'attributeLabel', ''));
        $genericNode->setAttributeValue(ArrayHelper::extractSafely($node, 'attributeValue', ''));
        $genericNode->setAttributeValueLabel(ArrayHelper::extractSafely($node, 'attributeValueLabel', ''));
        $genericNode->setAttributeProperties(ArrayHelper::extractSafely($node, 'attributeProperties', ''));
    }

    /**
     * @param array $advancedNode
     * @param AdvancedResult $advancedObj
     */
    private function fillAdvancedStructure($advancedNode, $advancedObj)
    {
        $advancedObj->setCustomerId(ArrayHelper::extractSafely($advancedNode, 'customerId', ''));
        $advancedObj->setDevice(ArrayHelper::extractSafely($advancedNode, 'device', ''));
        $advancedObj->setReferenceDate(ArrayHelper::extractSafely($advancedNode, 'referenceDate', ''));
        $advancedObj->setReferrer(ArrayHelper::extractSafely($advancedNode, 'referer', ''));
        $advancedObj->setOffer(ArrayHelper::extractSafely($advancedNode, 'offer', ''));
        $advancedObj->setStore(ArrayHelper::extractSafely($advancedNode, 'store', ''));
        $advancedObj->setUrl(ArrayHelper::extractSafely($advancedNode, 'url', ''));
    }
}
