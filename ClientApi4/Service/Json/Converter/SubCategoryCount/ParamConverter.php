<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\SubCategoryCount;

use Prediggo\ClientApi4\Service\Json\ArrayHelper;
use Prediggo\ClientApi4\Types\SubCategoryCount\SubCategoryCountParam;

class ParamConverter {

    public function toMap($obj) {

        /** @var SubCategoryCountParam $obj */
        $advancedMap = [];
        ArrayHelper::pushNonNull($advancedMap, 'offer', $obj->getAdvanced()->getOffer());
        ArrayHelper::pushNonNull($advancedMap, 'store', $obj->getAdvanced()->getStore());
        ArrayHelper::pushNonNull($advancedMap, 'customerId', $obj->getAdvanced()->getCustomerId());
        ArrayHelper::pushNonNull($advancedMap, 'device', $obj->getAdvanced()->getDevice());
        ArrayHelper::pushNonNull($advancedMap, 'referenceDate', $obj->getAdvanced()->getReferenceDate());
        ArrayHelper::pushNonNull($advancedMap, 'referer', $obj->getAdvanced()->getReferrer());
        ArrayHelper::pushNonNull($advancedMap, 'url', $obj->getAdvanced()->getUrl());

        $jsonMap = [
            'attributeName' => $obj->getAttributeName(),
            'attributeValue' => $obj->getAttributeValue(),
            'includeParents' => $obj->getIncludeParents(),
            'languageCode' => $obj->getLanguageCode(),
            'sessionId' => $obj->getSessionId(),
            'moduleVersion' => $obj->getModuleVersion(),
            'region' => $obj->getRegion(),
        ];

        if (count($advancedMap) > 0) {
            $jsonMap['advanced'] = $advancedMap;
        }

        return $jsonMap;
    }

}
