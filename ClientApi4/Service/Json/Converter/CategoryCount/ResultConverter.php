<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\CategoryCount;

use Prediggo\ClientApi4\Service\Json\ArrayHelper;
use Prediggo\ClientApi4\Service\Json\Converter\Debug\ResultConverter as DebugResultConverter;
use Prediggo\ClientApi4\Types\CategoryCount\CategoryCountResult;
use Prediggo\ClientApi4\Types\CategoryCount\AdvancedResult;
use Prediggo\ClientApi4\Types\CategoryCount\CategoryCountSubNode;

class ResultConverter
{

    private $debugResultConverter;

    public function __construct()
    {
        $this->debugResultConverter = new DebugResultConverter();
    }

    public function toObject($jsonMap)
    {

        $obj = new CategoryCountResult();

        $obj->setStatus(ArrayHelper::extractSafely($jsonMap, 'status', ''));
        $obj->setTimeInMs(ArrayHelper::extractSafely($jsonMap, 'timeInMs', 0.0));
        $obj->setDebug($this->convertDebugResult(ArrayHelper::extractSafely($jsonMap, 'debug', array())));
        $obj->setSessionId(ArrayHelper::extractSafely($jsonMap, 'sessionId', ''));
        $obj->setRegion(ArrayHelper::extractSafely($jsonMap, 'region', ''));
        $this->fillAdvancedStructure(ArrayHelper::extractSafely($jsonMap, 'advanced', array()), $obj->getAdvanced());
        $obj->setAttributeName(ArrayHelper::extractSafely($jsonMap, 'attributeName', ''));
        $obj->setNodes($this->convertNodes(ArrayHelper::extractSafely($jsonMap, 'nodes', array())));

        return $obj;
    }

    private function convertDebugResult($debug)
    {
        return $this->debugResultConverter->toObject($debug);
    }

    private function convertNodes($nodes)
    {
        $categoryCountSubNodes = array();

        foreach ($nodes as $node) {
            $categoryCountSubNodes[] = $this->convertCategoryCountSubNode($node);
        }

        return $categoryCountSubNodes;
    }

    /**
     * @param CategoryCountSubNode $node
     */
    private function convertCategoryCountSubNode($node)
    {
        $categoryCountSubNode = new CategoryCountSubNode();

        $categoryCountSubNode->setAttributeName(ArrayHelper::extractSafely($node, 'attributeName', ''));
        $categoryCountSubNode->setAttributeType(ArrayHelper::extractSafely($node, 'attributeType', ''));
        $categoryCountSubNode->setAttributeLabel(ArrayHelper::extractSafely($node, 'attributeLabel', ''));
        $categoryCountSubNode->setAttributeValue(ArrayHelper::extractSafely($node, 'attributeValue', ''));
        $categoryCountSubNode->setAttributeValueLabel(ArrayHelper::extractSafely($node, 'attributeValueLabel', ''));
        $categoryCountSubNode->setAttributeProperties(ArrayHelper::extractSafely($node, 'attributeProperties', array()));
        $categoryCountSubNode->setAvailableProducts(ArrayHelper::extractSafely($node, 'availableProducts', 0.0));

        $categoryCountSubNodeArray = array();
        foreach (ArrayHelper::extractSafely($node, 'nodes', []) as $subNode) {
            $categoryCountSubNodeArray[] = $this->convertCategoryCountSubNode($subNode);
        }

        $categoryCountSubNode->setNodes($categoryCountSubNodeArray);

        return $categoryCountSubNode;
    }

    /**
     * @param array $advancedNode
     * @param AdvancedResult $advancedObj
     */
    private function fillAdvancedStructure($advancedNode, $advancedObj)
    {
        $advancedObj->setCustomerId(ArrayHelper::extractSafely($advancedNode, 'customerId', ''));
        $advancedObj->setDevice(ArrayHelper::extractSafely($advancedNode, 'device', ''));
        $advancedObj->setReferenceDate(ArrayHelper::extractSafely($advancedNode, 'referenceDate', ''));
        $advancedObj->setReferrer(ArrayHelper::extractSafely($advancedNode, 'referer', ''));
        $advancedObj->setOffer(ArrayHelper::extractSafely($advancedNode, 'offer', ''));
        $advancedObj->setStore(ArrayHelper::extractSafely($advancedNode, 'store', ''));
        $advancedObj->setUrl(ArrayHelper::extractSafely($advancedNode, 'url', ''));
    }
}
