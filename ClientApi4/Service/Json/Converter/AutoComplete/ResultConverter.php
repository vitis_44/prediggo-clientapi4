<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\AutoComplete;

use Prediggo\ClientApi4\Service\Json\ArrayHelper;
use Prediggo\ClientApi4\Service\Json\Converter\Debug\ResultConverter as DebugResultConverter;
use Prediggo\ClientApi4\Service\Json\Converter\Item\ResultConverter as ItemResultConverter;
use Prediggo\ClientApi4\Types\AutoComplete\AdvancedResult;
use Prediggo\ClientApi4\Types\AutoComplete\Attribute;
use Prediggo\ClientApi4\Types\AutoComplete\AutoCompleteResult;
use Prediggo\ClientApi4\Types\AutoComplete\ItemCollection;
use Prediggo\ClientApi4\Types\AutoComplete\Keyword;
use Prediggo\ClientApi4\Types\AutoComplete\RedirectionComponent;

class ResultConverter {

    private $itemConverter;

    private $debugResultConverter;

    public function __construct() {
        $this->itemConverter = new ItemResultConverter();
        $this->debugResultConverter = new DebugResultConverter();
    }

    public function toObject($jsonMap) {

        $obj = new AutoCompleteResult();

        foreach ($jsonMap as $key => $value) {

            switch ($key) {

                case 'status':
                    $obj->setStatus($value);
                    break;

                case 'timeInMs':
                    $obj->setTimeInMs(floatval($value));
                    break;

                case 'sessionId':
                    $obj->setSessionId($value);
                    break;

                case 'languageCode':
                    $obj->setLanguageCode($value);
                    break;

                case 'zone':
                    $obj->setZone($value);
                    break;

                case 'region':
                    $obj->setRegion($value);
                    break;

                case 'query':
                    $obj->setQuery($value);
                    break;

                case 'keywords':
                    $obj->setKeywords($this->convertKeywords($value));
                    break;

                case 'attributes':
                    $obj->setAttributes($this->convertAttributes($value));
                    break;

                case 'items':
                    $obj->setItems($this->convertItemCollection($value));
                    break;

                case 'advanced':
                    $this->fillAdvancedStructure($value, $obj->getAdvanced());
                    break;

                case 'redirection':
                    $obj->setRedirection($this->convertSearchRedirection($value));
                    break;

                case 'debug':
                    $obj->setDebug($this->convertDebugResult($value));
                    break;
            }
        }

        return $obj;
    }

    private function convertKeywords($jsonMap) {

        $list = array();

        foreach ($jsonMap as $keywordNode) {

            $obj = new Keyword();

            foreach ($keywordNode as $key => $value) {

                switch ($key) {

                    case 'entry':
                        $obj->setEntry($value);
                        break;

                    case 'nbResults':
                        $obj->setNbResults(intval($value));
                        break;
                }
            }

            $list[] = $obj;
        }

        return $list;

    }

    private function convertAttributes($jsonMap) {

        $list = array();

        foreach ($jsonMap as $attributeNode) {

            $obj = new Attribute();

            foreach ($attributeNode as $key => $value) {
                switch ($key) {

                    case 'attributeName':
                        $obj->setAttributeName($value);
                        break;

                    case 'attributeLabel':
                        $obj->setAttributeLabel($value);
                        break;

                    case 'attributeType':
                        $obj->setAttributeType($value);
                        break;

                    case 'value':
                        $obj->setValue($value);
                        break;

                    case 'label':
                        $obj->setLabel($value);
                        break;

                    case 'properties':
                        $obj->setProperties($value);
                        break;

                    case 'nbResults':
                        $obj->setNbResults(intval($value));
                        break;
                }
            }

            $list[] = $obj;
        }

        return $list;

    }

    private function convertSearchRedirection($redirectionNode) {

        $obj = new RedirectionComponent();
        $obj->setRedirectUrl(ArrayHelper::extractSafely($redirectionNode, 'redirectUrl', ''));

        return $obj;
    }

    private function convertItemCollection($itemsNode) {

        $list = array();

        foreach ($itemsNode as $collectionNode) {

            $obj = new ItemCollection();

            foreach ($collectionNode as $key => $value) {

                switch ($key) {

                    case 'itemType':
                        $obj->setItemType($value);
                        break;

                    case 'matches':

                        $matches = array();
                        foreach ($value as $itemMap) {
                            $matches[] = $this->itemConverter->toObject($itemMap);
                        }

                        $obj->setMatches($matches);
                        break;
                }

            }
            $list[] = $obj;

        }

        return $list;

    }

    /**
     * @param array $advancedNode
     * @param AdvancedResult $advancedObj
     */
    private function fillAdvancedStructure($advancedNode, $advancedObj) {

        $advancedObj->setCustomerId(ArrayHelper::extractSafely($advancedNode, 'customerId', ''));
        $advancedObj->setDevice(ArrayHelper::extractSafely($advancedNode, 'device', ''));
        $advancedObj->setReferenceDate(ArrayHelper::extractSafely($advancedNode, 'referenceDate', ''));
        $advancedObj->setReferrer(ArrayHelper::extractSafely($advancedNode, 'referer', ''));
        $advancedObj->setOffer(ArrayHelper::extractSafely($advancedNode, 'offer', ''));
        $advancedObj->setStore(ArrayHelper::extractSafely($advancedNode, 'store', ''));
        $advancedObj->setUrl(ArrayHelper::extractSafely($advancedNode, 'url', ''));
    }

    private function convertDebugResult($debug)
    {
        return $this->debugResultConverter->toObject($debug);
    }
}
