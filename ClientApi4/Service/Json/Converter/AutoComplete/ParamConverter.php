<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\AutoComplete;

use Prediggo\ClientApi4\Service\Json\ArrayHelper;
use Prediggo\ClientApi4\Types\AutoComplete\AutoCompleteParam;

class ParamConverter {

    public function toMap($obj) {

        /** @var AutoCompleteParam $obj */
        $advancedMap = [];
        ArrayHelper::pushNonNull($advancedMap, 'zone', $obj->getAdvanced()->getZone());
        ArrayHelper::pushNonNull($advancedMap, 'offer', $obj->getAdvanced()->getOffer());
        ArrayHelper::pushNonNull($advancedMap, 'store', $obj->getAdvanced()->getStore());
        ArrayHelper::pushNonNull($advancedMap, 'customerId', $obj->getAdvanced()->getCustomerId());
        ArrayHelper::pushNonNull($advancedMap, 'device', $obj->getAdvanced()->getDevice());
        ArrayHelper::pushNonNull($advancedMap, 'referenceDate', $obj->getAdvanced()->getReferenceDate());
        ArrayHelper::pushNonNull($advancedMap, 'referer', $obj->getAdvanced()->getReferrer());
        ArrayHelper::pushNonNull($advancedMap, 'url', $obj->getAdvanced()->getUrl());

        $jsonMap = [
            'moduleVersion' => $obj->getModuleVersion(),
            'query' => $obj->getQuery(),
            'sessionId' => $obj->getSessionId(),
            'languageCode' => $obj->getLanguageCode(),
            'region' => $obj->getRegion()
        ];

        if (count($advancedMap) > 0) {
            $jsonMap['advanced'] = $advancedMap;
        }

        ArrayHelper::pushNonEmpty($jsonMap, 'filters', $obj->getFilters());
        return $jsonMap;
    }
}
