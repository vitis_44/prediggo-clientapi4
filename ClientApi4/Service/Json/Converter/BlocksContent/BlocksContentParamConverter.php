<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\BlocksContent;

use Prediggo\ClientApi4\Service\Json\ArrayHelper;
use Prediggo\ClientApi4\Service\Json\Converter\PageContent\AdvancedParamConverter;
use Prediggo\ClientApi4\Types\BlocksContent\BlockParameters;
use Prediggo\ClientApi4\Types\BlocksContent\BlocksContentParam;

class BlocksContentParamConverter
{

    private $advancedParamConverter;

    public function __construct() {
        $this->advancedParamConverter = new AdvancedParamConverter();
    }

    /**
     * @var BlocksContentParam $obj
     */
    public function toMap($obj)
    {
        $jsonMap = [
            'moduleVersion' => $obj->getModuleVersion(),
            'sessionId' => $obj->getSessionId(),
            'region' => $obj->getRegion()
        ];

        $advancedMap = $this->advancedParamConverter->toMap($obj->getAdvanced());
        if (count($advancedMap) > 0) {
            $jsonMap['advanced'] = $advancedMap;
        }

        $jsonMap['blocks'] = $this->convertBlockParameters($obj->getBlocks());

        return $jsonMap;
    }

    /**
     * @param BlockParameters[] $blockParameters
     */
    public function convertBlockParameters($blockParameters)
    {
        $blocks = array();

        foreach ($blockParameters as $parameters) {
            $parameterMap = [];
            ArrayHelper::pushNonNull($parameterMap, 'key', $parameters->getKey());
            ArrayHelper::pushNonNull($parameterMap, 'blockIdentifier', $parameters->getBlockIdentifier());
            ArrayHelper::pushNonNull($parameterMap, 'campaignIdentifier', $parameters->getCampaignIdentifier());
            ArrayHelper::pushNonNull($parameterMap, 'query', $parameters->getQuery());
            ArrayHelper::pushNonEmpty($parameterMap, 'filters', $parameters->getFilters());
            ArrayHelper::pushNonNull($parameterMap, 'skus', $parameters->getSkus());
            ArrayHelper::pushNonNull($parameterMap, 'refiningId', $parameters->getRefiningId());
            ArrayHelper::pushNonNull($parameterMap, 'page', $parameters->getPage());
            ArrayHelper::pushNonNull($parameterMap, 'resultsPerPage', $parameters->getResultsPerPage());
            ArrayHelper::pushNonNull($parameterMap, 'sortingCode', $parameters->getSortingCode());
            ArrayHelper::pushNonNull($parameterMap, 'mode', $parameters->getMode());
            ArrayHelper::pushNonNull($parameterMap, 'semanticSearchWeight', $parameters->getSemanticSearchWeight());
            if (count($parameterMap) > 0) {
                $blocks[] = $parameterMap;
            }
        }

        return $blocks;
    }
}
