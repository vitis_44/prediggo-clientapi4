<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\BlocksContent;

use Prediggo\ClientApi4\Service\Json\ArrayHelper;
use Prediggo\ClientApi4\Service\Json\Converter\PageContent\ResultConverter as PageContentResultConverter;
use Prediggo\ClientApi4\Service\Json\Converter\Debug\ResultConverter as DebugResultConverter;
use Prediggo\ClientApi4\Types\BlocksContent\BlockContent;
use Prediggo\ClientApi4\Types\BlocksContent\BlockParameters;
use Prediggo\ClientApi4\Types\BlocksContent\BlocksContentResult;

class BlocksContentResultConverter {

    private $pageContentConverter;
    private $debugResultConverter;

    public function __construct() {
        $this->pageContentConverter = new PageContentResultConverter();
        $this->debugResultConverter = new DebugResultConverter();
    }

    public function toObject($jsonMap) {

        $obj = new BlocksContentResult();

        $obj->setStatus(ArrayHelper::extractSafely($jsonMap, 'status', ''));
        $obj->setTimeInMs(ArrayHelper::extractSafely($jsonMap, 'timeInMs', 0.0));
        $obj->setDebug($this->convertDebugResult(ArrayHelper::extractSafely($jsonMap, 'debug', array())));
        $obj->setSessionId(ArrayHelper::extractSafely($jsonMap, 'sessionId', ''));
        $obj->setRegion(ArrayHelper::extractSafely($jsonMap, 'region', ''));
        $this->pageContentConverter->fillAdvancedStructure(ArrayHelper::extractSafely($jsonMap, 'advanced', array()), $obj->getAdvanced());

        $obj->setBlocks($this->convertBlocks(ArrayHelper::extractSafely($jsonMap, 'blocks', array())));

        return $obj;
    }

    private function convertBlocks($blockContentCollectionNode) {
        $blocks = array();

        foreach ($blockContentCollectionNode as $key => $blockContentNode) {
            $blockContent = new BlockContent();

            $blockParametersNode = ArrayHelper::extractSafely($blockContentNode, 'parameters', array());
            $blockContent->setParameters($this->convertBlockParametersNode($blockParametersNode));

            $blockNode = ArrayHelper::extractSafely($blockContentNode, 'block', array());
            $blockType = ArrayHelper::extractSafely($blockNode, 'blockType', '');
            switch ($blockType) {
                case 'AD':
                    $blockContent->setBlock($this->pageContentConverter->convertAdBlockNode($blockNode));
                    break;
                case 'SEARCH':
                case 'NAVIGATION':
                case 'ITEMS_COLLECTION':
                    $blockContent->setBlock($this->pageContentConverter->convertSearchBlockNode($blockNode));
                    break;
                case 'AISLE':
                case 'BASKET':
                case 'CATEGORY':
                case 'GENERIC':
                case 'PRODUCT':
                case 'SEARCH_AISLE':
                default:
                    $blockContent->setBlock($this->pageContentConverter->convertRecoBlockNode($blockNode));
                    break;
            }

            $blocks[$key] = $blockContent;
        }

        return $blocks;
    }

    private function convertBlockParametersNode($blockParametersNode) {
        $blockParameters = new BlockParameters();

        $blockParameters->setKey(ArrayHelper::extractSafely($blockParametersNode, 'key'));
        $blockParameters->setBlockIdentifier(ArrayHelper::extractSafely($blockParametersNode, 'blockIdentifier'));
        $blockParameters->setCampaignIdentifier(ArrayHelper::extractSafely($blockParametersNode, 'campaignIdentifier'));
        $blockParameters->setQuery(ArrayHelper::extractSafely($blockParametersNode, 'query'));
        $blockParameters->setFiltersArray($this->convertFiltersNode(ArrayHelper::extractSafely($blockParametersNode, 'filters', array())));
        $blockParameters->setSkus($this->convertArray(ArrayHelper::extractSafely($blockParametersNode, 'skus', array())));
        $blockParameters->setRefiningId(ArrayHelper::extractSafely($blockParametersNode, 'refiningId'));
        $blockParameters->setPage(ArrayHelper::extractSafely($blockParametersNode, 'page'));
        $blockParameters->setResultsPerPage(ArrayHelper::extractSafely($blockParametersNode, 'resultsPerPage'));
        $blockParameters->setSortingCode(ArrayHelper::extractSafely($blockParametersNode, 'sortingCode'));
        $blockParameters->setMode(ArrayHelper::extractSafely($blockParametersNode, 'mode'));
        $blockParameters->setSemanticSearchWeight(ArrayHelper::extractSafely($blockParametersNode, 'semanticSearchWeight'));

        return $blockParameters;
    }

    private function convertDebugResult($debug)
    {
        return $this->debugResultConverter->toObject($debug);
    }

    private function convertFiltersNode($filtersNode)
    {
        $filters = array();

        foreach ($filtersNode as $attName => $valuesNode) {
            $filters[$attName] = $this->convertArray($valuesNode);
        }

        return $filters;
    }

    private function convertArray($arrayNode) {
        $values = array();

        foreach ($arrayNode as $value) {
            $values[] = $value;
        }

        return $values;
    }
}
