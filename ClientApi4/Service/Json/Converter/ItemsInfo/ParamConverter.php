<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\ItemsInfo;

use Prediggo\ClientApi4\Service\Json\ArrayHelper;
use Prediggo\ClientApi4\Types\ItemsInfo\ItemsInfoParam;

class ParamConverter {

    public function toMap($obj) {

        /** @var ItemsInfoParam $obj */
        $advancedMap = [];
        ArrayHelper::pushNonNull($advancedMap, 'offer', $obj->getAdvanced()->getOffer());
        ArrayHelper::pushNonNull($advancedMap, 'store', $obj->getAdvanced()->getStore());
        ArrayHelper::pushNonNull($advancedMap, 'customerId', $obj->getAdvanced()->getCustomerId());
        ArrayHelper::pushNonNull($advancedMap, 'device', $obj->getAdvanced()->getDevice());
        ArrayHelper::pushNonNull($advancedMap, 'referenceDate', $obj->getAdvanced()->getReferenceDate());
        ArrayHelper::pushNonNull($advancedMap, 'referer', $obj->getAdvanced()->getReferrer());
        ArrayHelper::pushNonNull($advancedMap, 'url', $obj->getAdvanced()->getUrl());

        $jsonMap = [
            'moduleVersion' => $obj->getModuleVersion(),
            'skus' => $obj->getSkus(),
            'sessionId' => $obj->getSessionId(),
            'region' => $obj->getRegion(),
        ];

        if (count($advancedMap) > 0) {
            $jsonMap['advanced'] = $advancedMap;
        }

        return $jsonMap;
    }

}
