<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\ItemsInfo;

use Prediggo\ClientApi4\Service\Json\ArrayHelper;
use Prediggo\ClientApi4\Service\Json\Converter\Item\ResultConverter as ItemResultConverter;
use Prediggo\ClientApi4\Types\ItemsInfo\AdvancedResult;
use Prediggo\ClientApi4\Types\ItemsInfo\ItemsInfoResult;

class ResultConverter {

    private $itemConverter;

    public function __construct() {
        $this->itemConverter = new ItemResultConverter();
    }

    public function toObject($jsonMap) {

        $obj = new ItemsInfoResult();

        $obj->setStatus(ArrayHelper::extractSafely($jsonMap, 'status', ''));
        $obj->setTimeInMs(ArrayHelper::extractSafely($jsonMap, 'timeInMs', 0.0));
        $obj->setSessionId(ArrayHelper::extractSafely($jsonMap, 'sessionId', ''));
        $obj->setRegion(ArrayHelper::extractSafely($jsonMap, 'region', ''));
        $obj->setItems($this->convertItemsMap(ArrayHelper::extractSafely($jsonMap, 'items', array())));
        $obj->setDebugText(ArrayHelper::extractSafely($jsonMap, 'debugText', ''));

        $this->fillAdvancedStructure(ArrayHelper::extractSafely($jsonMap, 'advanced', array()), $obj->getAdvanced());

        return $obj;
    }

    private function convertItemsMap($itemsNode) {

        $itemMap = array();

        foreach ($itemsNode as $sku => $itemNode) {
            $itemMap[$sku] = $this->itemConverter->toObject($itemNode);
        }

        return $itemMap;
    }

    /**
     * @param array $advancedNode
     * @param AdvancedResult $advancedObj
     */
    private function fillAdvancedStructure($advancedNode, $advancedObj) {

        $advancedObj->setCustomerId(ArrayHelper::extractSafely($advancedNode, 'customerId', ''));
        $advancedObj->setDevice(ArrayHelper::extractSafely($advancedNode, 'device', ''));
        $advancedObj->setReferenceDate(ArrayHelper::extractSafely($advancedNode, 'referenceDate', ''));
        $advancedObj->setReferrer(ArrayHelper::extractSafely($advancedNode, 'referer', ''));
        $advancedObj->setOffer(ArrayHelper::extractSafely($advancedNode, 'offer', ''));
        $advancedObj->setStore(ArrayHelper::extractSafely($advancedNode, 'store', ''));
        $advancedObj->setUrl(ArrayHelper::extractSafely($advancedNode, 'url', ''));
    }
}
