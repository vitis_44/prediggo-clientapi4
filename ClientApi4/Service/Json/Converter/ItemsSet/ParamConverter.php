<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\ItemsSet;

use Prediggo\ClientApi4\Service\Json\ArrayHelper;
use Prediggo\ClientApi4\Types\ItemsSet\ItemsSetParam;

class ParamConverter {

    public function toMap($obj) {

        /** @var ItemsSetParam $obj */
        $advancedMap = [];;
        ArrayHelper::pushNonNull($advancedMap, 'offer', $obj->getAdvanced()->getOffer());
        ArrayHelper::pushNonNull($advancedMap, 'store', $obj->getAdvanced()->getStore());
        ArrayHelper::pushNonNull($advancedMap, 'customerId', $obj->getAdvanced()->getCustomerId());
        ArrayHelper::pushNonNull($advancedMap, 'device', $obj->getAdvanced()->getDevice());
        ArrayHelper::pushNonNull($advancedMap, 'referenceDate', $obj->getAdvanced()->getReferenceDate());
        ArrayHelper::pushNonNull($advancedMap, 'referer', $obj->getAdvanced()->getReferrer());
        ArrayHelper::pushNonNull($advancedMap, 'url', $obj->getAdvanced()->getUrl());

        $jsonMap = [
            'moduleVersion' => $obj->getModuleVersion(),
            'itemsSetType' => $obj->getItemsSetType(),
            'sessionId' => $obj->getSessionId(),
            'region' => $obj->getRegion(),
        ];

        if (count($advancedMap) > 0) {
            $jsonMap['advanced'] = $advancedMap;
        }

        return $jsonMap;
    }

}
