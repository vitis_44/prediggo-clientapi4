<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\ItemsSet;

use Prediggo\ClientApi4\Service\Json\ArrayHelper;
use Prediggo\ClientApi4\Service\Json\Converter\Item\ResultConverter as ItemResultConverter;
use Prediggo\ClientApi4\Service\Json\Converter\Debug\ResultConverter as DebugResultConverter;
use Prediggo\ClientApi4\Types\ItemsSet\AdvancedResult;
use Prediggo\ClientApi4\Types\ItemsSet\ItemsSetResult;

class ResultConverter {

    private $itemConverter;

    private $debugResultConverter;

    public function __construct() {
        $this->itemConverter = new ItemResultConverter();
        $this->debugResultConverter = new DebugResultConverter();
    }

    public function toObject($jsonMap) {

        $obj = new ItemsSetResult();

        $obj->setStatus(ArrayHelper::extractSafely($jsonMap, 'status', ''));
        $obj->setTimeInMs(ArrayHelper::extractSafely($jsonMap, 'timeInMs', 0.0));
        $obj->setSessionId(ArrayHelper::extractSafely($jsonMap, 'sessionId', ''));
        $obj->setRegion(ArrayHelper::extractSafely($jsonMap, 'region', ''));
        $obj->setItems($this->convertItemsArray(ArrayHelper::extractSafely($jsonMap, 'items', array())));
        $obj->setDebug($this->convertDebugResult(ArrayHelper::extractSafely($jsonMap, 'debug', array())));

        $this->fillAdvancedStructure(ArrayHelper::extractSafely($jsonMap, 'advanced', array()), $obj->getAdvanced());

        return $obj;
    }

    private function convertItemsArray($itemsNodeArray) {

        $itemArray = array();

        foreach ($itemsNodeArray as $itemNode) {
            $itemArray[] = $this->itemConverter->toObject($itemNode);
        }

        return $itemArray;
    }

    /**
     * @param array $advancedNode
     * @param AdvancedResult $advancedObj
     */
    private function fillAdvancedStructure($advancedNode, $advancedObj) {

        $advancedObj->setCustomerId(ArrayHelper::extractSafely($advancedNode, 'customerId', ''));
        $advancedObj->setDevice(ArrayHelper::extractSafely($advancedNode, 'device', ''));
        $advancedObj->setReferenceDate(ArrayHelper::extractSafely($advancedNode, 'referenceDate', ''));
        $advancedObj->setReferrer(ArrayHelper::extractSafely($advancedNode, 'referer', ''));
        $advancedObj->setOffer(ArrayHelper::extractSafely($advancedNode, 'offer', ''));
        $advancedObj->setStore(ArrayHelper::extractSafely($advancedNode, 'store', ''));
        $advancedObj->setUrl(ArrayHelper::extractSafely($advancedNode, 'url', ''));
    }

    private function convertDebugResult($debug)
    {
        return $this->debugResultConverter->toObject($debug);
    }
}
