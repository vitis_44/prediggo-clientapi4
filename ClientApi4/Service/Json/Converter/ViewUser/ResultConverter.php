<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\ViewUser;

use Prediggo\ClientApi4\Service\Json\ArrayHelper;
use Prediggo\ClientApi4\Service\Json\Converter\Debug\ResultConverter as DebugResultConverter;
use Prediggo\ClientApi4\Types\Debug\DebugResult;
use Prediggo\ClientApi4\Types\ViewUser\UserDataSet;
use Prediggo\ClientApi4\Types\ViewUser\ViewUserResult;

class ResultConverter {

    private $debugResultConverter;

    public function __construct()
    {
        $this->debugResultConverter = new DebugResultConverter();
    }

    public function toObject($jsonMap) {

        $obj = new ViewUserResult();

        $obj->setStatus(ArrayHelper::extractSafely($jsonMap, 'status', ''));
        $obj->setTimeInMs(ArrayHelper::extractSafely($jsonMap, 'timeInMs', 0.0));
        $obj->setSessionId(ArrayHelper::extractSafely($jsonMap, 'sessionId', ''));
        $obj->setCustomerId(ArrayHelper::extractSafely($jsonMap, 'customerId', ''));
        $obj->setDebug($this->convertDebugResult(ArrayHelper::extractSafely($jsonMap, 'debug', array())));

        $dataSets = array();

        foreach (ArrayHelper::extractSafely($jsonMap, 'dataSets', array()) as $dataSetNode) {
            $thisDataSet = new UserDataSet();
            $thisDataSet->setSetName(ArrayHelper::extractSafely($dataSetNode, 'setName'));
            $thisDataSet->setEntries(ArrayHelper::extractSafely($dataSetNode, 'entries'));

            $dataSets[] = $thisDataSet;
        }

        $obj->setDataSets($dataSets);
        return $obj;
    }
    /**
     * @param $debug
     * @return DebugResult
     */
    private function convertDebugResult($debug)
    {
        return $this->debugResultConverter->toObject($debug);
    }
}
