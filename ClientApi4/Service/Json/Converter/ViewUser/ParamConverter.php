<?php

namespace Prediggo\ClientApi4\Service\Json\Converter\ViewUser;

use Prediggo\ClientApi4\Types\ViewUser\ViewUserParam;


class ParamConverter {

    public function toMap($obj) {

        /** @var ViewUserParam $obj */
        $jsonMap = [
            'moduleVersion' => $obj->getModuleVersion(),
            'sessionId' => $obj->getSessionId(),
            'customerId' => $obj->getCustomerId()
        ];

        return $jsonMap;
    }
}
