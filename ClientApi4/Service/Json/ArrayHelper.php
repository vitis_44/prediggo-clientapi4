<?php

namespace Prediggo\ClientApi4\Service\Json;


class ArrayHelper {

    static function extractSafely($array, $keyName, $default = null) {

        if (!array_key_exists($keyName, $array)) {
            return $default;
        }

        return $array[$keyName];
    }

    static function extractSafelyIgnoreCase($array, $keyName, $default = null) {

        foreach ($array as $key => $value) {
            if (strtolower($keyName) == strtolower($key)) {
                return self::extractSafely($array, $key);
            }
        }

        return $default;
    }

    static function pushNonNull(&$array, $keyName, $value) {

        if (isset($value)) {
            $array[$keyName] = $value;
        }

    }

    static function pushNonEmpty(&$array, $keyName, $collection) {

        if (count($collection)) {
            $array[$keyName] = $collection;
        }

    }
}