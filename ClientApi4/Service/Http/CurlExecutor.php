<?php

namespace Prediggo\ClientApi4\Service\Http;

use Prediggo\ClientApi4\Errors\RequestException;


/**
 * Executor Implementation using php cUrl APIe
 */
class CurlExecutor implements RequestExecutor {

    const DEFAULT_CONNECT_TIMEOUT_MS = 4000;
    const DEFAULT_READ_TIMEOUT_MS = 4000;

    private $connectTimeout;
    private $readTimeout;
    private $useGzipCompression = true; //compression enabled by default

    /**
     * CurlExecutor constructor.
     *
     * @param $connectTimeout
     * @param $readTimeout
     */
    public function __construct($connectTimeout = self::DEFAULT_CONNECT_TIMEOUT_MS, $readTimeout = self::DEFAULT_READ_TIMEOUT_MS) {
        $this->connectTimeout = $connectTimeout;
        $this->readTimeout = $readTimeout;
    }

    function postQuery($url, $jsonBody, $headers) {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_USERAGENT => "phpClientLib" // who am i
        );

        //follow redirects if supported
        if (!ini_get("open_basedir")) {
            $options[CURLOPT_FOLLOWLOCATION] = true;
        }

        //timeout...
        $options[CURLOPT_CONNECTTIMEOUT_MS] = $this->connectTimeout; // timeout on connect
        $options[CURLOPT_TIMEOUT_MS] = $this->readTimeout;    // timeout on response

        $options[CURLOPT_POST] = 1;
        $options[CURLOPT_POSTFIELDS] = $jsonBody;

        $finalHeaders = array('Content-Type: application/json');

        foreach ($headers as $key => $value) {
            $finalHeaders[] = $key . ': ' . $value;
        }

        $options[CURLOPT_HTTPHEADER] = $finalHeaders;
        //echo '<br>'.$finalTimeout.'<br>';
        //echo '<br>'.$this->createRequestUrl().'<br>';

        if ($this->useGzipCompression) {
            $options[CURLOPT_ENCODING] = 'gzip';
        }

        $curlRequest = curl_init($url);
        curl_setopt_array($curlRequest, $options);

        //executes the request
        $content = curl_exec($curlRequest);

        //error retrieval
        $err = curl_errno($curlRequest);
        $errmsg = curl_error($curlRequest);
        $httpCode = curl_getinfo($curlRequest, CURLINFO_HTTP_CODE);
        curl_close($curlRequest);

        //socket error?
        if ($err != 0) {

            $ex = new RequestException('Curl error (' . $err . ')' . (!empty($errmsg) ? ' ' . $errmsg : ''));
            $ex->setQuery($jsonBody);
            $ex->setUrl($url);
            $ex->setResponse($content);
            throw $ex;
        }

        //http error?
        if ($httpCode < 200 || $httpCode >= 400) {
            $ex = new RequestException('HTTP error (' . $httpCode . ') ' . $content);
            $ex->setQuery($jsonBody);
            $ex->setUrl($url);
            $ex->setResponse($content);

            throw $ex;
        }

        return $content;
    }

    /**
     * Can turn gzip compression on/off (on by default)
     *
     * @param $enabled true = on
     */
    function useGzipCompression($enabled) {
        $this->useGzipCompression = $enabled;
    }

    /**
     * Current status of gzip compression
     *
     * @return boolean
     */
    public function isGzipCompressionEnabled() {
        return $this->useGzipCompression;
    }

}
