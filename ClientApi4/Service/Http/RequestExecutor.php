<?php

namespace Prediggo\ClientApi4\Service\Http;


/**
 * Base Http layer
 */
interface RequestExecutor {

    public function postQuery($url, $jsonBody, $headers);
}
